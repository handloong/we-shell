﻿namespace WeShell
{
    partial class FrmUIPageIndex
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            uiContextMenuStrip1 = new Sunny.UI.UIContextMenuStrip();
            设置ToolStripMenuItem = new ToolStripMenuItem();
            密码ToolStripMenuItem = new ToolStripMenuItem();
            退出ToolStripMenuItem = new ToolStripMenuItem();
            lblAccount = new Sunny.UI.UIMarkLabel();
            lblName = new Sunny.UI.UIMarkLabel();
            uiMarkLabel1 = new Sunny.UI.UIMarkLabel();
            uiContextMenuStrip1.SuspendLayout();
            SuspendLayout();
            // 
            // uiContextMenuStrip1
            // 
            uiContextMenuStrip1.BackColor = Color.FromArgb(243, 249, 255);
            uiContextMenuStrip1.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiContextMenuStrip1.ImageScalingSize = new Size(20, 20);
            uiContextMenuStrip1.Items.AddRange(new ToolStripItem[] { 设置ToolStripMenuItem, 密码ToolStripMenuItem, 退出ToolStripMenuItem });
            uiContextMenuStrip1.Name = "uiContextMenuStrip1";
            uiContextMenuStrip1.Size = new Size(119, 76);
            // 
            // 设置ToolStripMenuItem
            // 
            设置ToolStripMenuItem.Name = "设置ToolStripMenuItem";
            设置ToolStripMenuItem.Size = new Size(118, 24);
            设置ToolStripMenuItem.Text = "设置";
            // 
            // 密码ToolStripMenuItem
            // 
            密码ToolStripMenuItem.Name = "密码ToolStripMenuItem";
            密码ToolStripMenuItem.Size = new Size(118, 24);
            密码ToolStripMenuItem.Text = "密码";
            // 
            // 退出ToolStripMenuItem
            // 
            退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            退出ToolStripMenuItem.Size = new Size(118, 24);
            退出ToolStripMenuItem.Text = "退出";
            // 
            // lblAccount
            // 
            lblAccount.Cursor = Cursors.No;
            lblAccount.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            lblAccount.Location = new Point(58, 25);
            lblAccount.MarkColor = Color.FromArgb(0, 190, 172);
            lblAccount.Name = "lblAccount";
            lblAccount.Padding = new Padding(5, 0, 0, 0);
            lblAccount.Size = new Size(318, 29);
            lblAccount.Style = Sunny.UI.UIStyle.Colorful;
            lblAccount.TabIndex = 3;
            lblAccount.Text = "12727182";
            lblAccount.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // lblName
            // 
            lblName.Cursor = Cursors.No;
            lblName.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            lblName.Location = new Point(58, 91);
            lblName.MarkColor = Color.FromArgb(0, 190, 172);
            lblName.Name = "lblName";
            lblName.Padding = new Padding(5, 0, 0, 0);
            lblName.Size = new Size(318, 29);
            lblName.Style = Sunny.UI.UIStyle.Colorful;
            lblName.TabIndex = 3;
            lblName.Text = "12727182";
            lblName.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // uiMarkLabel1
            // 
            uiMarkLabel1.Cursor = Cursors.No;
            uiMarkLabel1.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiMarkLabel1.Location = new Point(58, 157);
            uiMarkLabel1.MarkColor = Color.FromArgb(0, 190, 172);
            uiMarkLabel1.Name = "uiMarkLabel1";
            uiMarkLabel1.Padding = new Padding(5, 0, 0, 0);
            uiMarkLabel1.Size = new Size(609, 29);
            uiMarkLabel1.Style = Sunny.UI.UIStyle.Colorful;
            uiMarkLabel1.TabIndex = 3;
            uiMarkLabel1.Text = "欢迎使用WeShell团队共享SSH软件";
            uiMarkLabel1.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // FrmUIPageIndex
            // 
            AutoScaleMode = AutoScaleMode.None;
            BackColor = Color.FromArgb(238, 251, 250);
            ClientSize = new Size(800, 450);
            ControlBoxFillHoverColor = Color.FromArgb(51, 203, 189);
            Controls.Add(uiMarkLabel1);
            Controls.Add(lblName);
            Controls.Add(lblAccount);
            Name = "FrmUIPageIndex";
            RectColor = Color.FromArgb(0, 190, 172);
            Style = Sunny.UI.UIStyle.Colorful;
            Text = "FPageMain";
            uiContextMenuStrip1.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion
        private Sunny.UI.UIContextMenuStrip uiContextMenuStrip1;
        private ToolStripMenuItem 设置ToolStripMenuItem;
        private ToolStripMenuItem 密码ToolStripMenuItem;
        private ToolStripMenuItem 退出ToolStripMenuItem;
        private Sunny.UI.UIMarkLabel lblAccount;
        private Sunny.UI.UIMarkLabel lblName;
        private Sunny.UI.UIMarkLabel uiMarkLabel1;
    }
}