﻿using Sunny.UI;
using System.Data;
using WeShellCore;

namespace WeShell
{
    public partial class FrmEditUser : UIForm
    {
        private IUserServer _userServer;
        private IGroupServer _groupServer;

        public FrmEditUser()
        {
            InitializeComponent();
            _userServer = new UserServer(App.WebSocketJsonRpcClient);
            _groupServer = new GroupServer(App.WebSocketJsonRpcClient);
            UIStyles.InitColorful(EasyConf.Read<Config>().GetColorFul(), Color.White);
        }

        private async void btnGetPageList_Click(object sender, EventArgs e)
        {
            await LoadUser();
        }
        List<GroupResponse> _groupResponses = new List<GroupResponse>();
        private async void FrmEditUser_Load(object sender, EventArgs e)
        {
            await LoadUser();

            var result = await _groupServer.ListAsync(new GroupQueryInput
            {
                Token = Glb.Token
            });

            if (result.Successful)
            {
                _groupResponses = result.Data.As<List<GroupResponse>>().OrderBy(x => x.Name).ToList();
                TreeUtils.PopulateTreeViewParentIdSelect(_groupResponses, cboTreeGroups.Nodes, new string[] { });
            }
            else
            {
                ShowErrorTip($"加载Group错误 {result.Message}");
                this.Close();
            }
        }

        private async void btnSave_Click(object sender, EventArgs e)
        {
            var id = lblId.Text;
            if (!string.IsNullOrWhiteSpace(id))
            {
                await SaveForm(id);
            }
            else
            {
                this.ShowWarningTip("请先在左侧双击用户进入编辑");
            }
        }

        private async Task SaveForm(string id)
        {
            var account = txtAccount.Text;
            var name = txtName.Text;
            if (string.IsNullOrWhiteSpace(account))
            {
                ShowWarningTip("账号不能为空");
                return;
            }
            if (string.IsNullOrWhiteSpace(name))
            {
                ShowWarningTip("姓名不能为空");
                return;
            }
            var superAdmin = cbSuperAdmin.Checked ? "Y" : "N";
            var status = cbStatus.Checked ? "Y" : "N";

            var groupTreeNodes = TreeUtils.GetSelectedNodes(cboTreeGroups.Nodes);

            var groupIds = groupTreeNodes.Select(x => (x.Tag as GroupResponse).Id).ToList();

            var result = await _userServer.SaveFormAsync(new UserInput
            {
                Account = account,
                Id = id,
                Name = name,
                Status = status,
                SuperAdmin = superAdmin,
                Token = Glb.Token,
                GroupIds = groupIds
            });
            if (result.Successful)
            {
                ShowSuccessTip("保存成功");

                ClearForm();
            }
            else
            {
                ShowErrorTip($"保存失败 {result.Message}");
            }
        }

        private void ClearForm()
        {
            lblId.Text = string.Empty;
            txtAccount.Text = string.Empty;
            txtName.Text = string.Empty;
        }

        private async void btnDelete_Click(object sender, EventArgs e)
        {
            if (dgvUsers.SelectedRows.Count > 0)//小于等于0 为没有选中任何行
            {

                string id = dgvUsers.SelectedRows[0].Cells["T_Id"].Value.ToString();
                string name = dgvUsers.SelectedRows[0].Cells["T_Name"].Value.ToString();

                var smd = UIMessageDialog.ShowMessageDialog($"您确定要删除 [{name}] 吗?", "温馨提示", true, Style);
                if (smd)
                {
                    var result = await _userServer.DeleteAsync(new BaseInput
                    {
                        Id = id,
                        Token = Glb.Token,
                    });
                    if (result.Successful)
                    {
                        await LoadUser();
                    }
                    else
                    {
                        ShowErrorTip(result.Message);
                    }
                }
            }
            else
            {
                ShowWarningTip($"请选择一个用户");
            }
        }

        private async void btnResetPassword_Click(object sender, EventArgs e)
        {
            if (dgvUsers.SelectedRows.Count > 0)//小于等于0 为没有选中任何行
            {

                string id = dgvUsers.SelectedRows[0].Cells["T_Id"].Value.ToString();
                string name = dgvUsers.SelectedRows[0].Cells["T_Name"].Value.ToString();

                var smd = UIMessageDialog.ShowMessageDialog($"您确定要重置[{name}]的密码设置为123456吗?", "温馨提示", true, Style);
                if (smd)
                {
                    var result = await _userServer.RestEveryOnePassswordAsync(new BaseInput
                    {
                        Id = id,
                        Token = Glb.Token,
                    });
                    if (result.Successful)
                    {
                        ShowSuccessTip($"已经将[{name}]的密码设置为123456");
                    }
                    else
                    {
                        ShowErrorTip(result.Message);
                    }
                }
            }
            else
            {
                ShowWarningTip($"请选择一个用户");
            }
        }

        private async Task LoadUser()
        {
            var result = await _userServer.GetPageListAsync(new UserQueryInput
            {
                Token = Glb.Token,
                Account = txtQueryAccount.Text,
                Name = txtQueryName.Text,
                PageIndex = uiPagination.ActivePage,
                PageSize = uiPagination.PageSize,
            });
            if (result.Successful)
            {
                var users = result.Data.As<List<UserResponse>>();
                var page = result.Page;
                uiPagination.TotalCount = page.Total;
                dgvUsers.DataSource = users;
            }
            else
            {
                ShowErrorTip(result.Message);
            }
        }

        private async void dgvUsers_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //获取用户Id
            var id = Convert.ToString(dgvUsers.CurrentRow.Cells["T_Id"].Value);
            var name = Convert.ToString(dgvUsers.CurrentRow.Cells["T_Name"].Value);
            var account = Convert.ToString(dgvUsers.CurrentRow.Cells["T_Account"].Value);
            var status = Convert.ToString(dgvUsers.CurrentRow.Cells["T_Status"].Value);
            var superAdmin = Convert.ToString(dgvUsers.CurrentRow.Cells["T_SuperAdmin"].Value);

            lblId.Text = id;
            txtName.Text = name;
            txtAccount.Text = account;
            cbStatus.Checked = status == "Y";
            cbSuperAdmin.Checked = superAdmin == "Y";

            var result = await _userServer.GroupIdsAsync(new BaseInput { Id = id, Token = Glb.Token });
            if (result.Successful)
            {
                var groupIds = result.Data.As<List<string>>();
                cboTreeGroups.Nodes.Clear();
                var selectList = TreeUtils.PopulateTreeViewParentIdSelect(_groupResponses, cboTreeGroups.Nodes, groupIds.ToArray());
                cboTreeGroups.Text = string.Join(";", selectList);
            }
            else
            {
                ShowErrorTip($"获取用户对应的组失败 {result.Message}");
            }
        }

        private async void btnAdd_Click(object sender, EventArgs e)
        {
            await SaveForm("");
        }


    }
}
