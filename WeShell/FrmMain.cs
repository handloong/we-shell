using Rebex.TerminalEmulation;
using Sunny.UI;
using System.Diagnostics;
using System.IO.Pipes;
using System.Reflection;
using System.Text;
using WeShellCore;

namespace WeShell
{
    public partial class FrmMain : Sunny.UI.UIForm
    {
        private readonly UserLoginResponse _userLoginResponse;
        private readonly IGroupServer _groupServer = null;
        private readonly IServer _server = null;
        public FrmMain(UserLoginResponse userLoginResponse)
        {
            InitializeComponent();
            _userLoginResponse = userLoginResponse;

            this.Text = $"WeShell,欢迎使用:{_userLoginResponse.Name},v:{Assembly.GetExecutingAssembly().GetName().Version}";

            _groupServer = new GroupServer(App.WebSocketJsonRpcClient);
            _server = new Server(App.WebSocketJsonRpcClient);

            SetGlobalConfig(); //全局配置
        }

        #region 全局配置
        private void SetGlobalConfig()
        {
            MainTabControl = tabControl; //自动关联
            tabControl.ShowCloseButton = true;
            tabControl.Font = new Font("宋体", 8F, FontStyle.Regular, GraphicsUnit.Point, 134);
            var mainPage = new FrmUIPageIndex(_userLoginResponse);
            tabControl.MainPage = mainPage.Text = "首页";
            AddPage(mainPage);



            var config = EasyConf.Read<Config>();
            this.Size = config.GetSize(this.Size);//位置信息
            var location = config.GetLocation(new Point(-1996));
            var x = location.X;
            var y = location.Y;
            if (x >= 0 && y >= 0 && x < Screen.PrimaryScreen.Bounds.Width && y < Screen.PrimaryScreen.Bounds.Height)
                this.Location = new Point(x, y);
            else
                this.StartPosition = FormStartPosition.CenterScreen;
            UIStyles.InitColorful(config.GetColorFul(), Color.White);//多彩主题

            //imageList
            this.imageList.ColorDepth = ColorDepth.Depth24Bit;
            this.imageList.ImageSize = new Size(24, 24);
            ResourcesHelper.SetImageList(this.imageList);
        }
        #endregion

        #region 窗口事件
        private async void FrmMain_Load(object sender, EventArgs e)
        {
            SetControlForUser();
            await LoadLeftTreeAsync();
        }

        private void FrmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }
        #endregion

        #region 权限信息

        private void SetControlForUser()
        {
            tsAddGroup.Visible = _userLoginResponse.SuperAdmin;
            tsModifyGroup.Visible = _userLoginResponse.SuperAdmin;
            tsDeleteGroup.Visible = _userLoginResponse.SuperAdmin;

            tsAddServer.Visible = _userLoginResponse.SuperAdmin;
            tsModifyServer.Visible = _userLoginResponse.SuperAdmin;
            tsDeleteServer.Visible = _userLoginResponse.SuperAdmin;

            tsUserManager.Visible = _userLoginResponse.SuperAdmin;
        }

        #endregion

        #region 加载左侧Tree
        List<GroupResponse> _groupResponses = new List<GroupResponse>();
        private async Task LoadLeftTreeAsync()
        {
            uiTreeView.ImageList = imageList;
            uiTreeView.CheckBoxes = false;
            uiTreeView.ShowLines = false;
            uiTreeView.Nodes.Clear();

            var result = await _groupServer.ListAsync(new GroupQueryInput
            {
                Token = Glb.Token
            });

            if (result.Successful)
            {
                _groupResponses = result.Data.As<List<GroupResponse>>().OrderBy(x => x.Name).ToList();
                TreeUtils.PopulateTreeView(_groupResponses, uiTreeView.Nodes);
            }
            else
            {
                ShowErrorTip($"加载Group错误 {result.Message}");
            }
        }

        #endregion

        #region 新增组

        private async void tsAddGroup_Click(object sender, EventArgs e)
        {
            new FrmEditGroup(async x =>
            {
                var result = await _groupServer.SaveFormAsync(new GroupInput
                {
                    Name = x.Name,
                    Icon = x.Icon,
                    Token = Glb.Token,
                    ParentId = x.ParentId
                });
                if (result.Successful)
                {
                    await LoadLeftTreeAsync();
                }
                else
                {
                    ShowErrorDialog(result.Message);
                }
            }, _groupResponses).ShowDialog();

            await Task.CompletedTask;
        }

        #endregion

        #region 修改组

        private void tsModifyGroup_Click(object sender, EventArgs e)
        {
            TreeNode selectedNode = uiTreeView.SelectedNode;
            if (selectedNode.IsGroup())
            {
                var response = selectedNode.Tag as GroupResponse;

                new FrmEditGroup(async x =>
                {
                    var result = await _groupServer.SaveFormAsync(new GroupInput
                    {
                        Id = response.Id,
                        Name = x.Name,
                        Icon = x.Icon,
                        Token = _userLoginResponse.Token,
                        ParentId = x.ParentId
                    });
                    if (result.Successful)
                    {
                        await LoadLeftTreeAsync();
                    }
                    else
                    {
                        ShowErrorDialog(result.Message);
                    }
                }, _groupResponses, response).ShowDialog();
            }
            else
            {
                ShowWarningTip("请先选中一个组");
            }
        }

        #endregion

        #region 删除组

        private async void tsDeleteGroup_ClickAsync(object sender, EventArgs e)
        {
            TreeNode selectedNode = uiTreeView.SelectedNode;
            if (selectedNode.IsGroup())
            {
                var response = selectedNode.Tag as GroupResponse;

                var smd = UIMessageDialog.ShowMessageDialog($"您确定要删除 [{response.Name}] 吗?", "温馨提示", true, Style);
                if (smd)
                {
                    var result = await _groupServer.DeleteAsync(new BaseInput
                    {
                        Id = response.Id,
                        Token = Glb.Token
                    });
                    if (result.Successful)
                    {
                        await LoadLeftTreeAsync();
                    }
                    else
                    {
                        ShowErrorDialog(result.Message);
                    }
                }
            }
            else
            {
                ShowWarningTip("请先选中一个组");
            }
        }

        #endregion

        #region 新增会话

        private void tsAddServer_Click(object sender, EventArgs e)
        {
            TreeNode selectedNode = uiTreeView.SelectedNode;
            string _selectGroupId = null;

            if (selectedNode.IsGroup())
                _selectGroupId = (selectedNode.Tag as GroupResponse).Id;
            if (selectedNode.IsServer())
            {
                _selectGroupId = (selectedNode.Tag as ServerResponse).GroupId;
            }

            new FrmEditServer(_selectGroupId, null, _groupResponses, async (x) =>
            {
                if (x)
                    await LoadLeftTreeAsync();
            }).ShowDialog();
        }

        #endregion

        #region 打开新窗口
        private async void tsOpenNewShell_Click(object sender, EventArgs e)
        {
            TreeNode selectedNode = uiTreeView.SelectedNode;
            if (selectedNode.IsServer())
            {
                await OpenShell(selectedNode.Tag as ServerResponse, true);
            }
            else
            {
                ShowWarningTip("只有会话才可以如此操作");
            }
        }
        #endregion

        #region 编辑会话

        private void tsEditServer_Click(object sender, EventArgs e)
        {
            TreeNode selectedNode = uiTreeView.SelectedNode;

            if (selectedNode.IsServer())
            {
                var _selectServerId = (selectedNode.Tag as ServerResponse).Id;
                new FrmEditServer(null, _selectServerId, _groupResponses, async (x) =>
                {
                    if (x)
                        await LoadLeftTreeAsync();
                }).ShowDialog();
            }
            else
            {
                ShowWarningTip("请先选中一个会话");
            }
        }

        #endregion

        #region 删除会话

        private async void tsDeleteServer_ClickAsync(object sender, EventArgs e)
        {
            TreeNode selectedNode = uiTreeView.SelectedNode;
            if (selectedNode.IsServer())
            {
                var response = selectedNode.Tag as ServerResponse;

                var smd = UIMessageDialog.ShowMessageDialog($"您确定要删除 [{response.Name}] 吗?", "温馨提示[首先提示]", true, Style);
                if (smd)
                {
                    smd = UIMessageDialog.ShowMessageDialog($"您确定要删除 [{response.Name}] 吗?", "温馨提示[最后一次提示]", true, Style);
                    if (smd)
                    {
                        var result = await _server.DeleteAsync(new BaseInput
                        {
                            Id = response.Id,
                            Token = Glb.Token
                        });
                        if (result.Successful)
                        {
                            await LoadLeftTreeAsync();
                        }
                        else
                        {
                            ShowErrorDialog(result.Message);
                        }
                    }
                }
            }
            else
            {
                ShowWarningTip("请先选中一个组");
            }
        }

        #endregion

        #region 展开组加载会话

        private async void uiTreeView_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            TreeNode selectedNode = e.Node;
            var group = selectedNode.Tag as GroupResponse;
            var result = await _server.ListAsync(new ServerQueryInput
            {
                GroupId = group.Id,
                Token = _userLoginResponse.Token
            });

            if (result.Successful)
            {
                //如果是Server先清空,然后在请求接口重新添加
                //[此处不要使用foreacn,他不报错,是个坑爹啊]
                //使用 foreach 循环遍历节点时，如果在循环中修改了集合的内容（例如删除节点）
                //可能会导致异常或意外行为。这是因为 foreach 循环在遍历集合时会创建一个迭代器，
                //而在循环过程中修改集合会导致迭代器失效。

                //当前展开的节点下面是不是有组
                var hasGroup = false;

                //移除节点,不移除组
                for (int i = selectedNode.Nodes.Count - 1; i >= 0; i--)
                {
                    TreeNode childNode = selectedNode.Nodes[i];
                    if (childNode.IsServer())
                        selectedNode.Nodes.Remove(childNode);

                    if (!hasGroup)
                        hasGroup = childNode.IsGroup();
                }

                var serverResponses = result.Data.As<List<ServerResponse>>().OrderBy(x => x.Name).ToList();

                //Group下面没有会话和组,Loading变成没有数据,否则移除Loading
                var firstNode = selectedNode.FirstNode;
                if (firstNode.IsLoading())
                {
                    if (serverResponses.Count == 0 && !hasGroup)
                    {
                        firstNode.Text = "没有数据";
                        firstNode.SetImage("na");
                    }
                    else
                        selectedNode.Nodes.Remove(firstNode);
                }

                //加入Server
                foreach (var item in serverResponses)
                {
                    var subNode = new TreeNode($"{item.Name} {item.IP}");
                    subNode.Tag = item;
                    subNode.ToolTipText = item.Name;
                    subNode.SetImage(item.Icon);
                    selectedNode.Nodes.Add(subNode);
                }
            }
            else
            {
                ShowErrorTip(result.Message);
            }
        }

        #endregion

        #region 详情

        private void tsDetail_Click(object sender, EventArgs e)
        {
            TreeNode selectedNode = uiTreeView.SelectedNode;
            if (selectedNode != null)
            {
                if (selectedNode.IsGroup())
                {
                    ShowSuccessTip($"组名称:{selectedNode.Text}");
                }
                else if (selectedNode.IsServer())
                {
                    var item = selectedNode.Tag as ServerResponse;
                    UIMessageDialog.ShowMessageDialog(this, @$"名称:{item.Name}
IP:{item.IP}
端口:{item.Port}
责任人:{item.DutyUser}
备注:{(string.IsNullOrWhiteSpace(item.Remark) ? "无" : item.Remark)}", "Server 详情", false, UIStyle.Colorful);
                }
                else
                {
                    ShowErrorTip($"当前信息不支持查看");
                }
            }
        }

        #endregion

        #region 右键节点
        private void uiTreeView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                var ClickPoint = new Point(e.X, e.Y);

                TreeNode CurrentNode = uiTreeView.GetNodeAt(ClickPoint);
                if (CurrentNode != null)
                {
                    uiTreeView.SelectedNode = CurrentNode;
                }
            }
        }

        #endregion

        #region 刷新Tree

        private async void tsLoadTree_Click(object sender, EventArgs e)
        {
            await LoadLeftTreeAsync();
        }

        #endregion

        #region 双击连接SSH/Windows
        private async void uiTreeView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var clickPoint = new Point(e.X, e.Y);
            TreeNode selectedNode = uiTreeView.GetNodeAt(clickPoint);
            if (selectedNode.IsServer())
            {
                await OpenShell(selectedNode.Tag as ServerResponse, false);
            }
        }

        Dictionary<string, List<Guid>> _shellTabDic = new Dictionary<string, List<Guid>>();
        private async Task OpenShell(ServerResponse _selectServer, bool alawaysAppend)
        {
            var serverId = _selectServer.Id;

            if (!_shellTabDic.ContainsKey(serverId))
            {
                _shellTabDic[serverId] = new List<Guid> { Guid.NewGuid() };
            }

            var _guid = _shellTabDic[serverId].Last();

            if (ExistPage(_guid) && !alawaysAppend)
            {
                SelectPage(_guid);
                return;
            }

            if (alawaysAppend)
            {
                var guids = _shellTabDic[serverId];
                guids.Add(Guid.NewGuid());

                _guid = _shellTabDic[serverId].Last();
            }

            var result = await _server.ListAuthAsync(new BaseInput
            {
                Id = serverId,
                Token = Glb.Token
            });
            if (result.Successful)
            {
                //Where(x => x.Allow) 优化超管多个账户也会弹出用户选择器
                var auths = result.Data.As<List<ServerAuth>>().Where(x => x.Allow).ToList();
                ServerAuth serverAuth = null;

                if (auths.Count == 0)
                {
                    ShowErrorTip("该服务器没有设置允许授权的密码");
                    return;
                }
                else if (auths.Count == 1)
                {
                    serverAuth = auths[0];
                }
                else
                {
                    new FrmPasswordSelect(auths, x =>
                    {
                        serverAuth = x;
                    }).ShowDialog();
                }
                if (serverAuth != null)
                {
                    var page = new FrmUIPage(new ServerInfo
                    {
                        Type = _selectServer.Type,
                        Name = _selectServer.Name,
                        IP = _selectServer.IP,
                        Port = _selectServer.Port,
                        UserName = serverAuth.UserName,
                        Password = serverAuth.Password.ToPwd()
                    });

                    ShowInfoTip($"开始连接[{_selectServer.IP}] [{_selectServer.Name}]", 600);

                    await page.Connect(OnFail: x =>
                    {
                        ShowErrorTip(x);
                    }, OnConnected: () =>
                    {
                        AddPage(page, _guid);
                        SelectPage(_guid);
                    });
                }
            }
        }
        #endregion

        #region 关闭会话
        private bool tabControl_BeforeRemoveTabPage(object sender, int index)
        {
            var @bool = UIMessageDialog.ShowMessageDialog("确定要关闭: " + tabControl.TabPages[index].Text + "吗?", UILocalize.AskTitle, showCancelButton: true, UIStyle.Colorful, false, topMost: true, UIMessageDialogButtons.Ok);
            if (@bool)
            {
                try
                {
                    var ctls = tabControl.TabPages[index].Controls;
                    foreach (var item in ctls)
                    {
                        if (item is FrmUIPage)
                        {
                            var page = item as FrmUIPage;
                            page.Disconnect();
                            page.Dispose();
                        }
                    }
                }
                catch (Exception)
                {
                    //ShowWarningTip($"Close Warning {ex.Message}");
                }
            }
            return @bool;
        }
        #endregion

        #region 用户管理
        private void tsUserManager_Click(object sender, EventArgs e)
        {
            new FrmEditUser().ShowDialog();
        }
        #endregion

        #region 扩展菜单
        private void btnColorful_Click(object sender, EventArgs e)
        {
            new FrmColorful().ShowDialog();
        }
        private void tsSystemConfigMenuItem_Click(object sender, EventArgs e)
        {
            ShowWarningTip("更多设置开发中..预计在1.3+版本中实现该功能");
        }

        private void tsChangePwdMenuItem_Click(object sender, EventArgs e)
        {
            new FrmChangePassword("", x =>
            {
                ShowSuccessDialog("密码修改成功,点击确定退出软件");
                Application.Exit();
            }).ShowDialog();
        }
        #endregion

        #region 记录位置
        private void FrmMain_Resize(object sender, EventArgs e)
        {
            var config = EasyConf.Read<Config>();
            config.SetLocation(this.Location);
            config.SetSize(this.Size);
            EasyConf.Write(config);

#if DEBUG
            Debug.WriteLine($"Resize x:{this.Location.X},y:{this.Location.Y}, Width:{Size.Width},Height:{Size.Height} ");
#endif
        }

        private void FrmMain_Move(object sender, EventArgs e)
        {
            var config = EasyConf.Read<Config>();
            config.SetLocation(this.Location);
            config.SetSize(this.Size);
            EasyConf.Write(config);

#if DEBUG
            Debug.WriteLine($"Move x:{this.Location.X},y:{this.Location.Y}, Width:{Size.Width},Height:{Size.Height} ");
#endif

        }
        #endregion


    }
}