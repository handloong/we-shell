﻿using System.Net;
using WeShellCore;

namespace WeShell
{
    public static class PwdExtension
    {
        public static string ToPwd(this string @this)
        {
            if (string.IsNullOrWhiteSpace(@this))
            {
                return @this;
            }
            return PwdUtils.AesDecryptor_Base64(@this, "ZZKK6666KKZZ8888");
        }

        /// <summary>
        /// 解密
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static string Decrypt(this string @this)
        {
            if (string.IsNullOrWhiteSpace(@this))
            {
                return @this;
            }
            return PwdUtils.AesDecryptor_Base64(@this, Dns.GetHostName());
        }

        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="this"></param>
        /// <returns></returns>
        public static string Encrypt(this string @this)
        {
            if (string.IsNullOrWhiteSpace(@this))
            {
                return @this;
            }
            return PwdUtils.AesEncryptor_Base64(@this, Dns.GetHostName());
        }
    }
}
