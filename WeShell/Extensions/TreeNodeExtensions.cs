﻿using WeShellCore;

namespace WeShell
{
    public static class TreeNodeExtensions
    {
        public static bool IsGroup(this TreeNode node)
        {
            return node != null && node.Tag is GroupResponse;
        }

        public static bool IsServer(this TreeNode node)
        {
            return node != null && node.Tag is ServerResponse;
        }

        public static bool IsLoading(this TreeNode node)
        {
            return node != null && (node.Tag is string) && Convert.ToString(node.Tag) == "Loading";
        }

        public static bool IsWindows(this TreeNode node)
        {
            return IsServer(node) && (node.Tag as ServerResponse).Type == "Windows";
        }

        public static bool IsLinux(this TreeNode node)
        {
            return IsServer(node) && (node.Tag as ServerResponse).Type == "Linux";
        }

        public static void SetImage(this TreeNode node, string icon)
        {
            node.ImageKey = icon;
            node.SelectedImageKey = node.ImageKey;
        }
    }
}