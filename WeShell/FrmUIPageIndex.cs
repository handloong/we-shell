﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WeShellCore;

namespace WeShell
{
    public partial class FrmUIPageIndex : UIPage
    {
        private readonly UserLoginResponse _userLoginResponse;

        public FrmUIPageIndex(UserLoginResponse userLoginResponse)
        {
            InitializeComponent();
            _userLoginResponse = userLoginResponse;

            lblAccount.Text = _userLoginResponse.Account;
            lblName.Text = _userLoginResponse.Name;
            UIStyles.InitColorful(EasyConf.Read<Config>().GetColorFul(), Color.White);
        }

        //系统设置
        private void uiLinkLabel1_Click(object sender, EventArgs e)
        {
        }

        //修改密码
        private void uiLinkLabel2_Click(object sender, EventArgs e)
        {
          
        }
    }
}
