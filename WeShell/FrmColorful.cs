﻿using Sunny.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WeShell
{
    public partial class FrmColorful : UIForm
    {
        public FrmColorful()
        {
            InitializeComponent();
            UIStyles.InitColorful(EasyConf.Read<Config>().GetColorFul(), Color.White);
        }

        private void uiPanel3_Click(object sender, EventArgs e)
        {
            var panel = (UIPanel)sender;
            var c = panel.FillColor;

            var config = EasyConf.Read<Config>();
            config.SetColorful(c.R, c.G, c.B);
            EasyConf.Write(config);

            UIStyles.InitColorful(c, Color.White);
        }
    }
}
