﻿namespace WeShell
{
    partial class FrmPasswordSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            uiFlowLayoutPanel = new Sunny.UI.UIFlowLayoutPanel();
            SuspendLayout();
            // 
            // uiFlowLayoutPanel
            // 
            uiFlowLayoutPanel.Dock = DockStyle.Fill;
            uiFlowLayoutPanel.FillColor = Color.FromArgb(238, 251, 250);
            uiFlowLayoutPanel.FillColor2 = Color.FromArgb(238, 251, 250);
            uiFlowLayoutPanel.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiFlowLayoutPanel.Location = new Point(0, 35);
            uiFlowLayoutPanel.Margin = new Padding(4, 5, 4, 5);
            uiFlowLayoutPanel.MinimumSize = new Size(1, 1);
            uiFlowLayoutPanel.Name = "uiFlowLayoutPanel";
            uiFlowLayoutPanel.Padding = new Padding(2);
            uiFlowLayoutPanel.RectColor = Color.FromArgb(0, 190, 172);
            uiFlowLayoutPanel.ScrollBarBackColor = Color.FromArgb(238, 251, 250);
            uiFlowLayoutPanel.ScrollBarColor = Color.FromArgb(0, 190, 172);
            uiFlowLayoutPanel.ShowText = false;
            uiFlowLayoutPanel.Size = new Size(218, 248);
            uiFlowLayoutPanel.Style = Sunny.UI.UIStyle.Colorful;
            uiFlowLayoutPanel.TabIndex = 2;
            uiFlowLayoutPanel.Text = "uiFlowLayoutPanel1";
            uiFlowLayoutPanel.TextAlignment = ContentAlignment.MiddleCenter;
            // 
            // FrmPasswordSelect
            // 
            AutoScaleMode = AutoScaleMode.None;
            ClientSize = new Size(218, 283);
            ControlBoxFillHoverColor = Color.FromArgb(51, 203, 189);
            Controls.Add(uiFlowLayoutPanel);
            EscClose = true;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FrmPasswordSelect";
            RectColor = Color.FromArgb(0, 190, 172);
            Style = Sunny.UI.UIStyle.Colorful;
            Text = "用户选择器";
            TitleColor = Color.FromArgb(0, 190, 172);
            ZoomScaleRect = new Rectangle(19, 19, 800, 450);
            ResumeLayout(false);
        }

        #endregion
        private Sunny.UI.UIFlowLayoutPanel uiFlowLayoutPanel;
    }
}