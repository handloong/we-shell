﻿using System.Reflection;
using WeShell.Properties;

namespace WeShell
{
    public class ResourcesHelper
    {
        public static void SetImageList(ImageList imageList)
        {
            Properties.Resources res = new Properties.Resources();
            PropertyInfo[] properInfo = res.GetType().GetProperties(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (PropertyInfo item in properInfo)
            {
                var @object = Resources.ResourceManager.GetObject(item.Name, Properties.Resources.Culture);

                if (@object is Bitmap)
                {
                    Bitmap img = (Bitmap)@object;
                    imageList.Images.Add(item.Name, img);
                }
            }
        }

        public static List<ResourcesDto> GetImageList()
        {
            var retval = new List<ResourcesDto>();
            Properties.Resources res = new Properties.Resources();
            PropertyInfo[] properInfo = res.GetType().GetProperties(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance);
            foreach (PropertyInfo item in properInfo)
            {
                var @object = Resources.ResourceManager.GetObject(item.Name, Properties.Resources.Culture);

                if (@object is Bitmap)
                {
                    Bitmap img = (Bitmap)@object;
                    retval.Add(new ResourcesDto { Name = item.Name, Bitmap = img });
                }
            }
            return retval;
        }
    }

    public class ResourcesDto
    {
        public string Name { get; set; }
        public Bitmap Bitmap { get; set; }
    }
}
