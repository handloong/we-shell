﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeShellCore;

namespace WeShell
{
    public class TreeUtils
    {
        public static void PopulateTreeView(List<GroupResponse> groups, TreeNodeCollection parentNode)
        {
            foreach (var group in groups)
            {
                TreeNode node = new TreeNode(group.Name);
                node.Tag = group;
                node.SetImage(group.Icon);

                var loadingNode = new TreeNode("Loading");
                loadingNode.Tag = "Loading";
                node.Nodes.Add(loadingNode);

                if (group.Childs != null && group.Childs.Count > 0)
                {
                    PopulateTreeView(group.Childs, node.Nodes); // 递归调用填充子节点
                }

                parentNode.Add(node);
            }
        }



        public static TreeNode PopulateTreeViewParentId(List<GroupResponse> groups, TreeNodeCollection parentNode, string selectedId)
        {
            TreeNode selectNode = null;

            foreach (var group in groups)
            {
                TreeNode node = new TreeNode(group.Name);
                node.Tag = group;
                node.SetImage(group.Icon);

                if (selectedId == group.Id)
                {
                    selectNode = node;
                }

                if (group.Childs != null && group.Childs.Count > 0)
                {
                    TreeNode childNode = PopulateTreeViewParentId(group.Childs, node.Nodes, selectedId); // 递归调用填充子节点
                    if (childNode != null)
                    {
                        selectNode = childNode; // 将子节点的值赋给 selectNode
                    }
                }

                parentNode.Add(node);
            }

            return selectNode;
        }

        public static List<string> PopulateTreeViewParentIdSelect(List<GroupResponse> groups, TreeNodeCollection parentNode, string[] selectedIds)
        {
            List<string> selectNode = new List<string>();

            foreach (var group in groups)
            {
                TreeNode node = new TreeNode(group.Name);
                node.Tag = group;
                node.SetImage(group.Icon);

                if (selectedIds.Contains(group.Id))
                {
                    selectNode.Add(group.Name);
                    node.Checked = true;
                }

                if (group.Childs != null && group.Childs.Count > 0)
                {
                    List<string> sns = PopulateTreeViewParentIdSelect(group.Childs, node.Nodes, selectedIds); // 递归调用填充子节点
                    selectNode.AddRange(sns);
                }
                parentNode.Add(node);
            }

            return selectNode;
        }

        public static List<TreeNode> GetSelectedNodes(TreeNodeCollection nodes)
        {
            List<TreeNode> selectedNodes = new List<TreeNode>();

            foreach (TreeNode node in nodes)
            {
                if (node.Checked)
                {
                    selectedNodes.Add(node);
                }

                if (node.Nodes.Count > 0)
                {
                    selectedNodes.AddRange(GetSelectedNodes(node.Nodes));
                }
            }

            return selectedNodes;
        }
    }
}
