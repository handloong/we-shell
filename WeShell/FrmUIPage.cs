﻿using Rebex;
using Rebex.IO;
using Rebex.Net;
using Rebex.TerminalEmulation;
using Sunny.UI;
using Sunny.UI.Win32;
using System.Collections;
using System.Security.Cryptography;
using System.Text;

namespace WeShell
{
    public partial class FrmUIPage : UIPage
    {

        private readonly ServerInfo _serverInfo;

        public FrmUIPage(ServerInfo serverInfo)
        {
            InitializeComponent();
            UIStyles.InitColorful(EasyConf.Read<Config>().GetColorFul(), Color.White);
            _serverInfo = serverInfo;
            this.Text = _serverInfo.Name;

            imageList.Images.Add("Folder", IconHelper.GetDirectoryIcon(true));
        }

        public void Disconnect()
        {
            _sftp?.Disconnect();
            _sftp?.Dispose();
            _ssh?.Disconnect();
            _ssh?.Dispose();
            _rdp?.Disconnect();
            _rdp?.Dispose();
        }

        Sftp _sftp;
        Ssh _ssh;
        AxMSTSCLib.AxMsRdpClient9NotSafeForScripting _rdp;
        public async Task Connect(Action OnConnected, Action<string> OnFail)
        {
            if (_serverInfo.Type.Equals("linux", StringComparison.OrdinalIgnoreCase))
            {
                try
                {
                    TerminalControl terminalControl = new TerminalControl();
                    terminalControl.Dock = DockStyle.Fill;
                    terminalControl.Options = new TerminalOptions
                    {
                        Encoding = Encoding.UTF8,
                    };
                    SplitContainer.Panel2.Controls.Add(terminalControl);

                    _ssh = new Ssh();
                    await _ssh.ConnectAsync(_serverInfo.IP, _serverInfo.Port);
                    await _ssh.LoginAsync(_serverInfo.UserName, _serverInfo.Password);
                    terminalControl.Bind(_ssh);

                    _sftp = new Sftp();

                    //_sftp.LogWriter = new RichTextBoxLogWriter(txtLog, txtLog.MaxLength, Rebex.LogLevel.Debug);
                    _sftp.Settings.UseLargeBuffers = true;
                    _sftp.Timeout = 10 * 1000; //10s
                    _sftp.StateChanged += StateChanged;
                    _sftp.Traversing += Traversing;
                    _sftp.TransferProgressChanged += TransferProgressChanged;
                    _sftp.DeleteProgressChanged += DeleteProgressChanged;
                    _sftp.ProblemDetected += ProblemDetected;
                    _sftp.AuthenticationRequest += AuthenticationRequest;
                    _sftp.FingerprintCheck += _sftp_FingerprintCheck;  // Key接受事件
                    await _sftp.ConnectAsync(_serverInfo.IP, _serverInfo.Port);
                    await _sftp.LoginAsync(_serverInfo.UserName, _serverInfo.Password);

                    MakeSftpList();

                    OnConnected?.Invoke();

                }
                catch (Exception ex)
                {
                    OnFail?.Invoke($"登录[{_serverInfo.IP}] [{_serverInfo.Name}] 失败:{ex.Message}");
                }
            }
            else if (_serverInfo.Type.Equals("windows", StringComparison.OrdinalIgnoreCase))
            {
                try
                {
                    SplitContainer.Dispose();
                    _rdp = new AxMSTSCLib.AxMsRdpClient9NotSafeForScripting();
                    ((System.ComponentModel.ISupportInitialize)(_rdp)).BeginInit();
                    Controls.Add(_rdp);
                    ((System.ComponentModel.ISupportInitialize)(_rdp)).EndInit();

                    _rdp.Dock = DockStyle.Fill;
                    Rectangle ScreenArea = Screen.PrimaryScreen.Bounds;
                    _rdp.ColorDepth = 32;
                    _rdp.DesktopHeight = ScreenArea.Height;
                    _rdp.DesktopWidth = ScreenArea.Width;
                    _rdp.AdvancedSettings9.SmartSizing = true;
                    _rdp.Server = _serverInfo.IP;
                    _rdp.AdvancedSettings2.RDPPort = _serverInfo.Port;
                    _rdp.UserName = _serverInfo.UserName;
                    _rdp.AdvancedSettings2.ClearTextPassword = _serverInfo.Password;
                    //var clientNonScriptable =  axMsRdpClient6NotSafeForScripting1.GetOcx();
                    //注意：一定要把EnableCredSspSupport属性置为ture 否则连接上去就是一片空白！
                    _rdp.AdvancedSettings7.EnableCredSspSupport = true;
                    _rdp.ConnectingText = $"正在连接[{_serverInfo.IP}:{_serverInfo.Port}] [{_serverInfo.UserName}] 请稍等... ";
                    _rdp.AdvancedSettings2.PinConnectionBar = true;
                    _rdp.OnDisconnected += (rdpSender, rdpE) =>
                    {
                        OnFail?.Invoke($@"服务器:[{_serverInfo.IP}]
断开原因:被人顶掉或服务器断开连接
原因代码:{rdpE.discReason}");
                    };
                    _rdp.Connect();

                    OnConnected?.Invoke();
                }
                catch (Exception ex)
                {
                    OnFail?.Invoke($"登录[{_serverInfo.IP}] [{_serverInfo.Name}] 失败:{ex.Message}");
                }
            }
            else
            {
                OnFail?.Invoke($"不支持的类型:{_serverInfo.Type}");
            }
            await Task.CompletedTask;
        }

        private void _sftp_FingerprintCheck(object sender, SshFingerprintEventArgs e)
        {
            e.Accept = true;
        }

        private void AuthenticationRequest(object sender, SshAuthenticationRequestEventArgs e)
        {
        }

        //上传文件重复,就覆盖
        private void ProblemDetected(object sender, SftpProblemDetectedEventArgs e)
        {
            e.Overwrite();
        }

        private void DeleteProgressChanged(object sender, SftpDeleteProgressChangedEventArgs e)
        {

        }

        private void TransferProgressChanged(object sender, SftpTransferProgressChangedEventArgs e)
        {
            string strBatchInfo = string.Format("{0}/{1}", e.FilesProcessed, e.FilesTotal);
            SetProgressValue(Convert.ToInt32(e.ProgressPercentage));

            switch (e.TransferState)
            {
                case TransferProgressState.DataBlockProcessed:
                    int fileSizeInKB = (int)Math.Ceiling((double)e.BytesTransferred / 1024);
                    strBatchInfo += $"{fileSizeInKB} kb";
                    break;
                case TransferProgressState.DirectoryProcessing:
                    strBatchInfo += "处理文件夹";
                    break;
                case TransferProgressState.FileTransferring:
                    strBatchInfo += "传输文件";
                    break;
                case TransferProgressState.FileTransferred:
                    strBatchInfo += "文件传输完成.";
                    break;
                case TransferProgressState.TransferCompleted:
                    strBatchInfo += "文件传输完成";
                    //ShowTransferStatus(e.BytesTransferred, e.FilesTransferred);
                    break;
            }

            lblStatus.Text = strBatchInfo;
        }

        private void SetProgressValue(int value)
        {
            // workaround for progress bar smoothing
            if (value == 100)
            {
                pbTransfer.Maximum = 101;
                pbTransfer.Value = 101;
                pbTransfer.Maximum = 100;
                System.Threading.Thread.Sleep(200);
            }
            else
            {
                pbTransfer.Value = value;
            }
        }


        private void Traversing(object sender, SftpTraversingEventArgs e)
        {
        }

        private void StateChanged(object sender, SftpStateChangedEventArgs e)
        {
            //lblStatus.Text =  e.NewState.ToString();
        }


        private const string SYMLINK = "--->";     // symlink tag

        /// <summary>
        /// make sftp list
        /// </summary>
        public async void MakeSftpList()
        {
            listServer.Items.Clear();

            // not connected?

            SftpItemCollection list = null;
            try
            {
                list = await _sftp.GetListAsync((string)null);
                list.Sort();
            }
            catch (Exception ex)
            {
                ShowErrorTip(ex.Message);
                listServer.Items.Add(new ListViewItem("..", 0));
                return;
            }

            listServer.Items.Add(new ListViewItem("..", 0));

            foreach (SftpItem ftp in list)
            {
                var name = ftp.Name;

                var imageKey = $"FILE_{Path.GetExtension(name)}";
                if (!imageList.Images.ContainsKey(imageKey))
                {
                    if (ftp.IsFile)
                        imageList.Images.Add(imageKey, IconHelper.GetIcon(ftp.Name, true));
                }

                if (ftp.IsDirectory)
                    imageKey = "Folder";

                ListViewItem item = new ListViewItem(name, imageKey);
                item.Tag = ftp;
                var size = "";
                if (ftp.IsLink)
                    size = SYMLINK;

                if (ftp.IsFile)
                {
                    int fileSizeInKB = (int)Math.Ceiling((double)ftp.Length / 1024);
                    size = fileSizeInKB.ToString();
                }

                item.SubItems.Add(size);
                item.SubItems.Add(ftp.LastWriteTime.Value.ToString("yyyy-MM-dd HH:mm:ss"));
                listServer.Items.Add(item);
            }

            listServer.ListViewItemSorter = new ListViewColumnSorter();
            listServer.Sort();
            txtServerDir.Text = _sftp.GetCurrentDirectory();
        }

        private async void ListServerClick()
        {
            if (listServer.Items.Count < 1)
                return;

            if (listServer.SelectedItems.Count < 1)
                listServer.Items[0].Selected = true;

            listServer.Focus();

            // sftp state test
            int index = listServer.SelectedIndices[0];
            ListViewItem item = listServer.SelectedItems[0];

            //返回上层
            var path = "";
            if (index == 0)
                path = "..";

            var file = false;

            if (item.Tag is SftpItem)
            {
                var sftpItem = item.Tag as SftpItem;
                path = sftpItem.Path;
                switch (sftpItem.Type)
                {
                    case SftpItemType.Unknown:
                    case SftpItemType.Fifo:
                    case SftpItemType.CharacterDevice:
                    case SftpItemType.BlockDevice:
                    case SftpItemType.Socket:
                        this.ShowWarningTip($"该类型未支持,请联系邓振振解析该类型");
                        return;
                    case SftpItemType.Directory:
                        file = false;
                        break;
                    case SftpItemType.RegularFile:
                        file = true;
                        break;
                    case SftpItemType.Symlink:
                        path = _sftp.ResolveSymlink(sftpItem.Path);
                        SftpItem info = _sftp.GetInfo(path);
                        switch (info.Type)
                        {
                            case SftpItemType.Directory:
                                file = false;
                                break;
                            case SftpItemType.RegularFile:
                                file = true;
                                break;
                            default:
                                return;
                        }
                        break;
                    default:
                        this.ShowWarningTip($"该类型未支持,请联系邓振振解析该类型");
                        return;
                }
            }

            if (file)
            {
                //下载文件
                try
                {
                    FolderBrowserDialog fbd = new FolderBrowserDialog();
                    fbd.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    fbd.InitialDirectory = fbd.SelectedPath;

                    if (fbd.ShowDialog() == DialogResult.OK)
                    {
                        var fileFullName = Path.Combine(fbd.SelectedPath, path);
                        if (File.Exists(fileFullName))
                        {
                            var smd = UIMessageDialog.ShowMessageDialog($"该文件已经存在如果继续下载,需要删除本地文件 [{fileFullName}],您确定要删除吗?", "温馨提示", true, Style);
                            if (smd)
                                File.Delete(fileFullName);
                            else
                                return;
                        }
                        await _sftp.DownloadAsync(path, fbd.SelectedPath, TraversalMode.Recursive, TransferMethod.Copy, ActionOnExistingFiles.ThrowException);
                        ShowSuccessTip($"下载完成:{fileFullName}");
                    }

                }
                catch (Exception ex)
                {
                    this.ShowWarningTip($"执行下载文件失败!{ex.Message}");
                }
            }
            else
            {
                _sftp.ChangeDirectory(path);
                MakeSftpList();
            }
        }

        public class ListViewColumnSorter : MultiComparer, IComparer
        {
            public ListViewColumnSorter()
                : base(new FileSystemItemComparer(FileSystemItemComparerType.FileType, SortingOrder.Ascending), new FileSystemItemComparer(FileSystemItemComparerType.Name))
            {
            }

            int IComparer.Compare(object x, object y)
            {
                ListViewItem listViewX = (ListViewItem)x;
                ListViewItem listViewY = (ListViewItem)y;

                SftpItem itemX = (SftpItem)listViewX.Tag;
                SftpItem itemY = (SftpItem)listViewY.Tag;

                if (itemX == null)
                {
                    if (itemY == null)
                        return 0;

                    return -1;
                }
                else if (itemY == null)
                {
                    return 1;
                }

                return base.Compare(itemX, itemY);
            }
        }

        private void listServer_DoubleClick(object sender, EventArgs e)
        {
            ListServerClick();
        }

        private void txtServerDir_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter
                )
            {
                try
                {
                    _sftp.ChangeDirectory(txtServerDir.Text);
                    MakeSftpList();
                }
                catch (Exception ex)
                {
                    this.ShowErrorTip(ex.Message);
                }

            }
        }

        private async void uiButton1_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog();
                dialog.Multiselect = true;//该值确定是否可以选择多个文件
                dialog.Title = "请选择文件";
                dialog.Filter = "所有文件(*.*)|*.*";
                if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    var files = dialog.FileNames;
                    foreach (var item in files)
                    {
                        await _sftp.UploadAsync(item, txtServerDir.Text, TraversalMode.Recursive, TransferMethod.Copy, ActionOnExistingFiles.ThrowException);
                        this.ShowSuccessTip($"上传完成:{item}");
                    }
                }
            }
            catch (Exception ex)
            {
                this.ShowErrorTip(ex.Message);
            }
            finally
            {
                MakeSftpList();
            }
        }
    }


    public class ServerInfo
    {
        public string Type { get; set; }
        public string Name { get; set; }
        public string IP { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
