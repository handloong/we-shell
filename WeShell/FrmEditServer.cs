﻿using Sunny.UI;
using System.Data;
using System.DirectoryServices.ActiveDirectory;
using WeShellCore;

namespace WeShell
{
    public partial class FrmEditServer : UIForm
    {
        private readonly IGroupServer _groupServer = null;
        private readonly IServer _server = null;
        private readonly string _selectGroupId; //选中的组
        private readonly string _selectServerId;//选中的Server,如果为空则是新增
        private readonly List<GroupResponse> _groupResponses;
        private readonly bool _add;

        private readonly Action<bool> _action;
        private List<ResourcesDto> _resources;

        public FrmEditServer(string selectGroupGroupId, string selectServerGroupId, List<GroupResponse> groupResponses, Action<bool> action)
        {
            _groupServer = new GroupServer(App.WebSocketJsonRpcClient);
            _server = new Server(App.WebSocketJsonRpcClient);
            _selectGroupId = selectGroupGroupId;
            _selectServerId = selectServerGroupId;
            _groupResponses = groupResponses;
            _action = action;
            _add = string.IsNullOrWhiteSpace(_selectServerId);

            InitializeComponent();

            UIStyles.InitColorful(EasyConf.Read<Config>().GetColorFul(), Color.White);
            _resources = ResourcesHelper.GetImageList();
            if (_add)
            {
                var addResource = _resources.First(x => x.Name.Equals("linux", StringComparison.OrdinalIgnoreCase));
                picIcon.Image = addResource.Bitmap;
                picIcon.Tag = addResource.Name;
            }
        }

        private ServerResponse _selectServerResponse;

        private async Task InitializeServer()
        {
            if (!_add)
            {
                var serverResult = await _server.DetailAsync(new ServerQueryInput { Token = Glb.Token, Id = _selectServerId });

                if (!serverResult.Successful)
                {
                    ShowErrorDialog($"获取当前会话错误 {serverResult.Message}");
                    return;
                }
                _selectServerResponse = serverResult.Data.As<ServerResponse>();

                txtIP.Text = _selectServerResponse.IP;
                txtName.Text = _selectServerResponse.Name;
                txtDutyUser.Text = _selectServerResponse.DutyUser;
                txtPort.Text = _selectServerResponse.Port.ToString();
                txtRemark.Text = _selectServerResponse.Remark;

                if (_selectServerResponse.Type.ToLower() == "linux")
                    rdoSSH.Checked = true;
                else
                    rdoWindows.Checked = true;

                //回显图标

                var img = _resources.FirstOrDefault(x => x.Name == _selectServerResponse.Icon);
                if (img != null)
                {
                    picIcon.Image = img.Bitmap;
                    picIcon.Tag = img.Name;
                }

                //加载Auth信息
                var result = await _server.ListAuthAsync(new BaseInput
                {
                    Token = Glb.Token,
                    Id = _selectServerResponse.Id,
                });
                if (result.Successful)
                {
                    var auths = result.Data.As<List<ServerAuthResponse>>();

                    foreach (var item in auths)
                    {
                        int index = this.dgvUsers.Rows.Add();
                        this.dgvUsers.Rows[index].Cells["UserName"].Value = item.UserName;
                        this.dgvUsers.Rows[index].Cells["Password"].Value = item.Password.ToPwd();
                        this.dgvUsers.Rows[index].Cells["Allow"].Value = item.Allow;
                    }
                }
            }
        }

        private void cboGroup_NodeSelected(object sender, TreeNode node)
        {
            txtIP.Focus();
        }

        private async void btnSave_Click(object sender, EventArgs e)
        {
            await SaveForm(true);
        }

        private async Task SaveForm(bool clearWhenAdd)
        {
            var ip = txtIP.Text;
            if (string.IsNullOrWhiteSpace(ip))
            {
                ShowErrorTip("请输入IP");
                return;
            }
            var port = txtPort.Text;
            if (string.IsNullOrWhiteSpace(port))
            {
                ShowErrorTip("请输入端口");
                return;
            }
            var selectGroup = cboGroup.SelectedNode;
            if (selectGroup == null)
            {
                ShowErrorTip("请选择组");
                return;
            }
            var groupId = (selectGroup.Tag as GroupResponse).Id;

            var type = "Linux";
            if (rdoWindows.Checked)
                type = "Windows";

            var auths = new List<ServerAuth>();
            foreach (DataGridViewRow row in dgvUsers.Rows)
            {
                string userName = Convert.ToString(row.Cells["UserName"].Value);
                if (string.IsNullOrWhiteSpace(userName))
                {
                    continue;
                }
                string password = Convert.ToString(row.Cells["Password"].Value);
                if (string.IsNullOrWhiteSpace(password))
                {
                    continue;
                }

                bool allow = Convert.ToBoolean(row.Cells["Allow"].Value);
                auths.Add(new ServerAuth
                {
                    Allow = allow,
                    Password = password,
                    UserName = userName,
                });
            }
            if (!auths.Any())
            {
                ShowErrorTip("请给该服务器设置用户名和密码");
                return;
            }

            var notAllow = auths.Count(x => !x.Allow) == auths.Count();
            if (notAllow)
            {
                var smd = UIMessageDialog.ShowMessageDialog($"该服务器没有为其他用户设置授权过的用户名和密码,其他用户可能无法访问,是否继续?", "温馨提示", true, Style);
                if (!smd)
                {
                    return;
                }
            }

            var serverInput = new ServerInput()
            {
                Id = _selectServerId,
                IP = ip,
                Port = port.ToInt(),
                Name = txtName.Text,
                Status = cbStatus.Checked ? "Y" : "N",
                DutyUser = txtDutyUser.Text,
                GroupId = groupId,
                Type = type,
                Token = Glb.Token,
                Remark = txtRemark.Text,
                ServerAuths = auths,
                Icon = Convert.ToString(picIcon.Tag)
            };

            var result = await _server.SaveFormAsync(serverInput);
            if (!result.Successful)
            {
                ShowErrorDialog(result.Message);
            }
            else
            {
                //新增
                if (string.IsNullOrWhiteSpace(_selectServerId) && clearWhenAdd)
                {
                    txtIP.Text = "";
                    txtName.Text = "";
                    txtRemark.Text = "";
                    txtDutyUser.Text = "";
                }
                _action?.Invoke(uiCheckBox.Checked);
            }
        }

        private async void btnSaveClose_Click(object sender, EventArgs e)
        {
            await SaveForm(false);
            btnClose_Click(sender, e);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private async void FrmEditServer_Load(object sender, EventArgs e)
        {
            await InitializeServer(); //先加载Server信息,获取组Id

            var selectedTreeNode = TreeUtils.PopulateTreeViewParentId(_groupResponses, cboGroup.Nodes, _selectServerResponse?.GroupId);
            cboGroup.SelectedNode = selectedTreeNode;
        }

        private void picIcon_Click(object sender, EventArgs e)
        {
            new FrmIcon(_resources, (n, i) =>
            {
                picIcon.Image = i;
                picIcon.Tag = n;
            }).ShowDialog();
        }

        private void rdoSSH_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoSSH.Checked && _add)
            {
                picIcon.Image = _resources.FirstOrDefault(x => x.Name.Equals("linux", StringComparison.OrdinalIgnoreCase))?.Bitmap;
            }
        }

        private void rdoWindows_CheckedChanged(object sender, EventArgs e)
        {
            if (rdoWindows.Checked && _add)
            {
                picIcon.Image = _resources.FirstOrDefault(x => x.Name.Equals("Windows", StringComparison.OrdinalIgnoreCase))?.Bitmap;
            }
        }
    }
}