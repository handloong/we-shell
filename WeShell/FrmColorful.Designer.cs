﻿namespace WeShell
{
    partial class FrmColorful
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            uiPanel1 = new Sunny.UI.UIPanel();
            uiPanel2 = new Sunny.UI.UIPanel();
            uiPanel3 = new Sunny.UI.UIPanel();
            uiPanel4 = new Sunny.UI.UIPanel();
            uiPanel5 = new Sunny.UI.UIPanel();
            uiPanel6 = new Sunny.UI.UIPanel();
            uiPanel7 = new Sunny.UI.UIPanel();
            uiPanel9 = new Sunny.UI.UIPanel();
            uiPanel10 = new Sunny.UI.UIPanel();
            uiPanel8 = new Sunny.UI.UIPanel();
            SuspendLayout();
            // 
            // uiPanel1
            // 
            uiPanel1.Cursor = Cursors.Hand;
            uiPanel1.FillColor = Color.FromArgb(0, 150, 136);
            uiPanel1.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiPanel1.Location = new Point(169, 64);
            uiPanel1.Margin = new Padding(4, 5, 4, 5);
            uiPanel1.MinimumSize = new Size(1, 1);
            uiPanel1.Name = "uiPanel1";
            uiPanel1.RectColor = Color.FromArgb(0, 150, 136);
            uiPanel1.Size = new Size(120, 80);
            uiPanel1.Style = Sunny.UI.UIStyle.Custom;
            uiPanel1.StyleCustomMode = true;
            uiPanel1.TabIndex = 0;
            uiPanel1.Text = "墨绿色";
            uiPanel1.TextAlignment = ContentAlignment.MiddleCenter;
            uiPanel1.Click += uiPanel3_Click;
            // 
            // uiPanel2
            // 
            uiPanel2.Cursor = Cursors.Hand;
            uiPanel2.FillColor = Color.FromArgb(255, 184, 0);
            uiPanel2.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiPanel2.Location = new Point(321, 64);
            uiPanel2.Margin = new Padding(4, 5, 4, 5);
            uiPanel2.MinimumSize = new Size(1, 1);
            uiPanel2.Name = "uiPanel2";
            uiPanel2.RectColor = Color.FromArgb(255, 184, 0);
            uiPanel2.Size = new Size(120, 80);
            uiPanel2.Style = Sunny.UI.UIStyle.Custom;
            uiPanel2.StyleCustomMode = true;
            uiPanel2.TabIndex = 1;
            uiPanel2.Text = "橙色";
            uiPanel2.TextAlignment = ContentAlignment.MiddleCenter;
            uiPanel2.Click += uiPanel3_Click;
            // 
            // uiPanel3
            // 
            uiPanel3.Cursor = Cursors.Hand;
            uiPanel3.FillColor = Color.FromArgb(0, 190, 172);
            uiPanel3.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiPanel3.Location = new Point(21, 64);
            uiPanel3.Margin = new Padding(4, 5, 4, 5);
            uiPanel3.MinimumSize = new Size(1, 1);
            uiPanel3.Name = "uiPanel3";
            uiPanel3.RectColor = Color.FromArgb(0, 190, 172);
            uiPanel3.Size = new Size(120, 80);
            uiPanel3.Style = Sunny.UI.UIStyle.Custom;
            uiPanel3.StyleCustomMode = true;
            uiPanel3.TabIndex = 2;
            uiPanel3.Text = "默认色";
            uiPanel3.TextAlignment = ContentAlignment.MiddleCenter;
            uiPanel3.Click += uiPanel3_Click;
            // 
            // uiPanel4
            // 
            uiPanel4.Cursor = Cursors.Hand;
            uiPanel4.FillColor = Color.FromArgb(255, 87, 34);
            uiPanel4.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiPanel4.Location = new Point(476, 64);
            uiPanel4.Margin = new Padding(4, 5, 4, 5);
            uiPanel4.MinimumSize = new Size(1, 1);
            uiPanel4.Name = "uiPanel4";
            uiPanel4.RectColor = Color.FromArgb(255, 87, 34);
            uiPanel4.Size = new Size(120, 80);
            uiPanel4.Style = Sunny.UI.UIStyle.Custom;
            uiPanel4.StyleCustomMode = true;
            uiPanel4.TabIndex = 1;
            uiPanel4.Text = "红色";
            uiPanel4.TextAlignment = ContentAlignment.MiddleCenter;
            uiPanel4.Click += uiPanel3_Click;
            // 
            // uiPanel5
            // 
            uiPanel5.Cursor = Cursors.Hand;
            uiPanel5.FillColor = Color.FromArgb(236, 98, 161);
            uiPanel5.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiPanel5.Location = new Point(631, 64);
            uiPanel5.Margin = new Padding(4, 5, 4, 5);
            uiPanel5.MinimumSize = new Size(1, 1);
            uiPanel5.Name = "uiPanel5";
            uiPanel5.RectColor = Color.FromArgb(236, 98, 161);
            uiPanel5.Size = new Size(120, 80);
            uiPanel5.Style = Sunny.UI.UIStyle.Custom;
            uiPanel5.StyleCustomMode = true;
            uiPanel5.TabIndex = 1;
            uiPanel5.Text = "猛男粉";
            uiPanel5.TextAlignment = ContentAlignment.MiddleCenter;
            uiPanel5.Click += uiPanel3_Click;
            // 
            // uiPanel6
            // 
            uiPanel6.Cursor = Cursors.Hand;
            uiPanel6.FillColor = Color.FromArgb(130, 58, 183);
            uiPanel6.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiPanel6.Location = new Point(21, 181);
            uiPanel6.Margin = new Padding(4, 5, 4, 5);
            uiPanel6.MinimumSize = new Size(1, 1);
            uiPanel6.Name = "uiPanel6";
            uiPanel6.RectColor = Color.FromArgb(130, 58, 183);
            uiPanel6.Size = new Size(120, 80);
            uiPanel6.Style = Sunny.UI.UIStyle.Custom;
            uiPanel6.StyleCustomMode = true;
            uiPanel6.TabIndex = 0;
            uiPanel6.Text = "性感紫";
            uiPanel6.TextAlignment = ContentAlignment.MiddleCenter;
            uiPanel6.Click += uiPanel3_Click;
            // 
            // uiPanel7
            // 
            uiPanel7.Cursor = Cursors.Hand;
            uiPanel7.FillColor = Color.FromArgb(80, 126, 164);
            uiPanel7.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiPanel7.Location = new Point(321, 181);
            uiPanel7.Margin = new Padding(4, 5, 4, 5);
            uiPanel7.MinimumSize = new Size(1, 1);
            uiPanel7.Name = "uiPanel7";
            uiPanel7.RectColor = Color.FromArgb(80, 126, 164);
            uiPanel7.Size = new Size(120, 80);
            uiPanel7.Style = Sunny.UI.UIStyle.Custom;
            uiPanel7.StyleCustomMode = true;
            uiPanel7.TabIndex = 1;
            uiPanel7.Text = "蓝灰色";
            uiPanel7.TextAlignment = ContentAlignment.MiddleCenter;
            uiPanel7.Click += uiPanel3_Click;
            // 
            // uiPanel9
            // 
            uiPanel9.BackColor = Color.FromArgb(243, 249, 255);
            uiPanel9.Cursor = Cursors.Hand;
            uiPanel9.FillColor = Color.FromArgb(140, 100, 80);
            uiPanel9.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiPanel9.Location = new Point(631, 181);
            uiPanel9.Margin = new Padding(4, 5, 4, 5);
            uiPanel9.MinimumSize = new Size(1, 1);
            uiPanel9.Name = "uiPanel9";
            uiPanel9.RectColor = Color.FromArgb(140, 100, 80);
            uiPanel9.Size = new Size(120, 80);
            uiPanel9.Style = Sunny.UI.UIStyle.Custom;
            uiPanel9.StyleCustomMode = true;
            uiPanel9.TabIndex = 1;
            uiPanel9.Text = "咖啡色";
            uiPanel9.TextAlignment = ContentAlignment.MiddleCenter;
            uiPanel9.Click += uiPanel3_Click;
            // 
            // uiPanel10
            // 
            uiPanel10.Cursor = Cursors.Hand;
            uiPanel10.FillColor = Color.FromArgb(0, 188, 212);
            uiPanel10.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiPanel10.Location = new Point(169, 181);
            uiPanel10.Margin = new Padding(4, 5, 4, 5);
            uiPanel10.MinimumSize = new Size(1, 1);
            uiPanel10.Name = "uiPanel10";
            uiPanel10.RectColor = Color.FromArgb(0, 188, 212);
            uiPanel10.Size = new Size(120, 80);
            uiPanel10.Style = Sunny.UI.UIStyle.Custom;
            uiPanel10.StyleCustomMode = true;
            uiPanel10.TabIndex = 2;
            uiPanel10.Text = "蓝色";
            uiPanel10.TextAlignment = ContentAlignment.MiddleCenter;
            uiPanel10.Click += uiPanel3_Click;
            // 
            // uiPanel8
            // 
            uiPanel8.Cursor = Cursors.Hand;
            uiPanel8.FillColor = Color.FromArgb(121, 190, 60);
            uiPanel8.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiPanel8.Location = new Point(476, 181);
            uiPanel8.Margin = new Padding(4, 5, 4, 5);
            uiPanel8.MinimumSize = new Size(1, 1);
            uiPanel8.Name = "uiPanel8";
            uiPanel8.RectColor = Color.FromArgb(121, 190, 60);
            uiPanel8.Size = new Size(120, 80);
            uiPanel8.Style = Sunny.UI.UIStyle.Custom;
            uiPanel8.StyleCustomMode = true;
            uiPanel8.TabIndex = 1;
            uiPanel8.Text = "奥龙帽";
            uiPanel8.TextAlignment = ContentAlignment.MiddleCenter;
            uiPanel8.Click += uiPanel3_Click;
            // 
            // FrmColorful
            // 
            AutoScaleMode = AutoScaleMode.None;
            ClientSize = new Size(785, 312);
            ControlBoxFillHoverColor = Color.FromArgb(51, 203, 189);
            Controls.Add(uiPanel10);
            Controls.Add(uiPanel3);
            Controls.Add(uiPanel9);
            Controls.Add(uiPanel5);
            Controls.Add(uiPanel8);
            Controls.Add(uiPanel4);
            Controls.Add(uiPanel7);
            Controls.Add(uiPanel2);
            Controls.Add(uiPanel6);
            Controls.Add(uiPanel1);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FrmColorful";
            RectColor = Color.FromArgb(0, 190, 172);
            ShowIcon = false;
            Style = Sunny.UI.UIStyle.Colorful;
            Text = "多彩主题";
            TitleColor = Color.FromArgb(0, 190, 172);
            ZoomScaleRect = new Rectangle(19, 19, 800, 450);
            ResumeLayout(false);
        }

        #endregion

        private Sunny.UI.UIPanel uiPanel1;
        private Sunny.UI.UIPanel uiPanel2;
        private Sunny.UI.UIPanel uiPanel3;
        private Sunny.UI.UIPanel uiPanel4;
        private Sunny.UI.UIPanel uiPanel5;
        private Sunny.UI.UIPanel uiPanel6;
        private Sunny.UI.UIPanel uiPanel7;
        private Sunny.UI.UIPanel uiPanel9;
        private Sunny.UI.UIPanel uiPanel10;
        private Sunny.UI.UIPanel uiPanel8;
    }
}