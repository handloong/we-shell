﻿using AutoUpdaterDotNET;
using Sunny.UI;
using System.Diagnostics;
using System.IO.Pipes;
using System.Reflection;
using System.Security.Principal;
using TouchSocket.Rpc;
using TouchSocket.Sockets;
using WeShellCore;

namespace WeShell
{
    public partial class FrmLogin : Sunny.UI.UIForm
    {
        public FrmLogin()
        {
            InitializeComponent();

            this.Text = $"WeShell v{Assembly.GetExecutingAssembly().GetName().Version} [{(System.Environment.Is64BitProcess ? "x64" : "x86")}]";
#if DEBUG
            App.InitializeWsJsonRpc("ws://127.0.0.1:7707/ws");
#endif

#if !DEBUG
            App.InitializeWsJsonRpc("ws://10.32.44.33:7707/ws");
#endif
        }

        public UserLoginResponse UserLoginResponse { get; internal set; }

        private async void btnLogin_Click(object sender, EventArgs e)
        {
            var loginText = btnLogin.Text;

            var account = txtAccount.Text.Trim();
            var password = txtPassword.Text.Trim();

            if (string.IsNullOrWhiteSpace(account))
            {
                ShowErrorTip("用户名不能为空");
                txtAccount.Focus();
                return;
            }
            if (string.IsNullOrWhiteSpace(password))
            {
                ShowErrorTip("密码不能为空");
                txtPassword.Focus();
                return;
            }

            try
            {
                btnLogin.Enabled = false;
                btnLogin.Text = "Loading..";

                if (!App.WebSocketJsonRpcClient.Online)
                {
                    await App.WebSocketJsonRpcClient.ConnectAsync(5000);
                }

                ILoginServer loginServer = new LoginServer(App.WebSocketJsonRpcClient);
                var retval = await loginServer.CheckLoginAsync(new LoginInput
                {
                    Account = account,
                    Password = password
                }, new InvokeOption { Timeout = 15000 });
                if (retval.Successful)
                {
                    UserLoginResponse = retval.Data.As<UserLoginResponse>();
                    Glb.Token = UserLoginResponse.Token;

                    if (UserLoginResponse.NeedChangePassword)
                    {
                        var cpd = new FrmChangePassword(password, async x =>
                        {
                            txtPassword.Text = x;

                            retval = await loginServer.CheckLoginAsync(new LoginInput
                            {
                                Account = account,
                                Password = x
                            }, new InvokeOption { Timeout = 15000 });

                            UserLoginResponse = retval.Data.As<UserLoginResponse>();
                            Glb.Token = UserLoginResponse.Token;

                        }).ShowDialog();
                        if (cpd != DialogResult.OK)
                        {
                            ShowErrorDialog("修改密码失败,系统退出");
                        }
                        else
                        {
                            this.DialogResult = DialogResult.OK;
                        }
                    }
                    else
                    {
                        this.DialogResult = DialogResult.OK;
                    }

                    var config = EasyConf.Read<Config>();
                    config.T1 = account.Encrypt();
                    EasyConf.Write(config);
                    this.Close();
                }
                else
                {
                    ShowErrorTip(retval.Message);
                }
            }
            catch (Exception ex)
            {
                ShowErrorTip($"连接服务器异常,请联系开发人员:[邓振振] {ex.Message}");
            }
            finally
            {
                btnLogin.Enabled = true;
                btnLogin.Text = loginText;
            }
        }

        private void FrmLogin_Shown(object sender, EventArgs e)
        {
            this.TopMost = true;
            this.TopMost = false;

            AutoUpdater.Start(@"http://10.32.44.80:8005/WeShell/AutoUpdater.xml");

            var config = EasyConf.Read<Config>();
            UIStyles.InitColorful(config.GetColorFul(), Color.White);

            txtAccount.Text = config.T1.Decrypt();
            if (!string.IsNullOrWhiteSpace(txtAccount.Text))
            {
                txtPassword.Focus();
            }
        }

        private void txtAccount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrWhiteSpace(txtAccount.Text))
                {
                    ShowErrorTip("用户名不能为空");
                    txtAccount.Focus();
                    return;
                }
                txtPassword.Focus();
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (string.IsNullOrWhiteSpace(txtPassword.Text))
                {
                    ShowErrorTip("密码不能为空");
                    txtPassword.Focus();
                    return;
                }

                btnLogin_Click(sender, e);
            }
        }

        private void linkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var url = @"https://gitee.com/handloong/we-shell";
            try
            {
                Process.Start(new ProcessStartInfo(url) { UseShellExecute = true });
            }
            catch
            {
                UIMessageDialog.ShowMessageDialog(url, UILocalize.InfoTitle, showCancelButton: false, Style);
            }
        }
    }
}