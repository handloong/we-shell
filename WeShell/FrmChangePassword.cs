﻿using Sunny.UI;
using WeShellCore;

namespace WeShell
{
    public partial class FrmChangePassword : UIForm
    {
        private readonly string _oldPassword;
        private readonly Action<string> _action;
        private IUserServer _server;
        public FrmChangePassword(string oldPassword, Action<string> action)
        {
            InitializeComponent();
            _oldPassword = oldPassword;
            _action = action;
            if (!string.IsNullOrWhiteSpace(_oldPassword))
            {
                txtOldPassword.Text = _oldPassword;
                txtOldPassword.Enabled = false;
                txtNewPassword1.Focus();
            }

            _server = new UserServer(App.WebSocketJsonRpcClient);
            UIStyles.InitColorful(EasyConf.Read<Config>().GetColorFul(), Color.White);
            this.TopMost = true;
        }

        private async void btnOk_Click(object sender, EventArgs e)
        {
            var oldPassword = txtOldPassword.Text;
            var newPassword1 = txtNewPassword1.Text;
            var newPassword2 = txtNewPassword2.Text;
            if (string.IsNullOrWhiteSpace(oldPassword))
            {
                ShowErrorTip("老密码不能为空");
                txtOldPassword.Focus();
                return;
            }

            if (string.IsNullOrWhiteSpace(newPassword1))
            {
                ShowErrorTip("新密码不能为空");
                txtNewPassword1.Focus();
                return;
            }


            if (string.IsNullOrWhiteSpace(newPassword2))
            {
                ShowErrorTip("请重复新密码");
                txtNewPassword2.Focus();
                return;
            }

            if (newPassword1 != newPassword2)
            {
                ShowErrorTip("两次密码不一致");
                txtNewPassword2.Focus();
                return;
            }

            var similarity = PwdUtils.Similarity(oldPassword, newPassword1);
            if (similarity > 80)
            {
                ShowErrorTip("新密码请勿与旧密码过于相似");
                txtNewPassword2.Focus();
                return;
            }

            var result = await _server.ChangePassswordAsync(new UserChangePassword
            {
                Token = Glb.Token,
                OldPassword = oldPassword,
                NewPassword = newPassword1,
            });

            if (result.Successful)
            {
                _action.Invoke(newPassword1);
                DialogResult = DialogResult.OK;
                this.Close();
            }
            else
            {
                ShowErrorTip($"重置密码失败 {result.Message}");
            }
        }
    }
}
