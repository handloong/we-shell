﻿namespace WeShell
{
    partial class FrmChangePassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            uiLabel1 = new Sunny.UI.UILabel();
            txtOldPassword = new Sunny.UI.UITextBox();
            uiLabel2 = new Sunny.UI.UILabel();
            txtNewPassword1 = new Sunny.UI.UITextBox();
            uiLabel3 = new Sunny.UI.UILabel();
            txtNewPassword2 = new Sunny.UI.UITextBox();
            btnOk = new Sunny.UI.UIButton();
            SuspendLayout();
            // 
            // uiLabel1
            // 
            uiLabel1.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiLabel1.Location = new Point(55, 62);
            uiLabel1.Name = "uiLabel1";
            uiLabel1.Size = new Size(90, 29);
            uiLabel1.Style = Sunny.UI.UIStyle.Colorful;
            uiLabel1.TabIndex = 0;
            uiLabel1.Text = "旧密码";
            uiLabel1.TextAlign = ContentAlignment.MiddleRight;
            // 
            // txtOldPassword
            // 
            txtOldPassword.ButtonFillColor = Color.FromArgb(0, 190, 172);
            txtOldPassword.ButtonFillHoverColor = Color.FromArgb(51, 203, 189);
            txtOldPassword.ButtonFillPressColor = Color.FromArgb(0, 152, 138);
            txtOldPassword.ButtonRectColor = Color.FromArgb(0, 190, 172);
            txtOldPassword.ButtonRectHoverColor = Color.FromArgb(51, 203, 189);
            txtOldPassword.ButtonRectPressColor = Color.FromArgb(0, 152, 138);
            txtOldPassword.FillColor2 = Color.FromArgb(238, 251, 250);
            txtOldPassword.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtOldPassword.Location = new Point(169, 55);
            txtOldPassword.Margin = new Padding(4, 5, 4, 5);
            txtOldPassword.MinimumSize = new Size(1, 16);
            txtOldPassword.Name = "txtOldPassword";
            txtOldPassword.Padding = new Padding(5);
            txtOldPassword.PasswordChar = '*';
            txtOldPassword.RectColor = Color.FromArgb(0, 190, 172);
            txtOldPassword.ScrollBarColor = Color.FromArgb(0, 190, 172);
            txtOldPassword.ShowText = false;
            txtOldPassword.Size = new Size(240, 36);
            txtOldPassword.Style = Sunny.UI.UIStyle.Colorful;
            txtOldPassword.TabIndex = 1;
            txtOldPassword.TextAlignment = ContentAlignment.MiddleLeft;
            txtOldPassword.Watermark = "";
            // 
            // uiLabel2
            // 
            uiLabel2.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiLabel2.Location = new Point(55, 124);
            uiLabel2.Name = "uiLabel2";
            uiLabel2.Size = new Size(90, 29);
            uiLabel2.Style = Sunny.UI.UIStyle.Colorful;
            uiLabel2.TabIndex = 0;
            uiLabel2.Text = "新密码";
            uiLabel2.TextAlign = ContentAlignment.MiddleRight;
            // 
            // txtNewPassword1
            // 
            txtNewPassword1.ButtonFillColor = Color.FromArgb(0, 190, 172);
            txtNewPassword1.ButtonFillHoverColor = Color.FromArgb(51, 203, 189);
            txtNewPassword1.ButtonFillPressColor = Color.FromArgb(0, 152, 138);
            txtNewPassword1.ButtonRectColor = Color.FromArgb(0, 190, 172);
            txtNewPassword1.ButtonRectHoverColor = Color.FromArgb(51, 203, 189);
            txtNewPassword1.ButtonRectPressColor = Color.FromArgb(0, 152, 138);
            txtNewPassword1.FillColor2 = Color.FromArgb(238, 251, 250);
            txtNewPassword1.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtNewPassword1.Location = new Point(169, 117);
            txtNewPassword1.Margin = new Padding(4, 5, 4, 5);
            txtNewPassword1.MinimumSize = new Size(1, 16);
            txtNewPassword1.Name = "txtNewPassword1";
            txtNewPassword1.Padding = new Padding(5);
            txtNewPassword1.PasswordChar = '*';
            txtNewPassword1.RectColor = Color.FromArgb(0, 190, 172);
            txtNewPassword1.ScrollBarColor = Color.FromArgb(0, 190, 172);
            txtNewPassword1.ShowText = false;
            txtNewPassword1.Size = new Size(240, 36);
            txtNewPassword1.Style = Sunny.UI.UIStyle.Colorful;
            txtNewPassword1.TabIndex = 2;
            txtNewPassword1.TextAlignment = ContentAlignment.MiddleLeft;
            txtNewPassword1.Watermark = "";
            // 
            // uiLabel3
            // 
            uiLabel3.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiLabel3.Location = new Point(20, 185);
            uiLabel3.Name = "uiLabel3";
            uiLabel3.Size = new Size(125, 29);
            uiLabel3.Style = Sunny.UI.UIStyle.Colorful;
            uiLabel3.TabIndex = 0;
            uiLabel3.Text = "重复新密码";
            uiLabel3.TextAlign = ContentAlignment.MiddleRight;
            // 
            // txtNewPassword2
            // 
            txtNewPassword2.ButtonFillColor = Color.FromArgb(0, 190, 172);
            txtNewPassword2.ButtonFillHoverColor = Color.FromArgb(51, 203, 189);
            txtNewPassword2.ButtonFillPressColor = Color.FromArgb(0, 152, 138);
            txtNewPassword2.ButtonRectColor = Color.FromArgb(0, 190, 172);
            txtNewPassword2.ButtonRectHoverColor = Color.FromArgb(51, 203, 189);
            txtNewPassword2.ButtonRectPressColor = Color.FromArgb(0, 152, 138);
            txtNewPassword2.FillColor2 = Color.FromArgb(238, 251, 250);
            txtNewPassword2.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtNewPassword2.Location = new Point(169, 178);
            txtNewPassword2.Margin = new Padding(4, 5, 4, 5);
            txtNewPassword2.MinimumSize = new Size(1, 16);
            txtNewPassword2.Name = "txtNewPassword2";
            txtNewPassword2.Padding = new Padding(5);
            txtNewPassword2.PasswordChar = '*';
            txtNewPassword2.RectColor = Color.FromArgb(0, 190, 172);
            txtNewPassword2.ScrollBarColor = Color.FromArgb(0, 190, 172);
            txtNewPassword2.ShowText = false;
            txtNewPassword2.Size = new Size(240, 36);
            txtNewPassword2.Style = Sunny.UI.UIStyle.Colorful;
            txtNewPassword2.TabIndex = 3;
            txtNewPassword2.TextAlignment = ContentAlignment.MiddleLeft;
            txtNewPassword2.Watermark = "";
            // 
            // btnOk
            // 
            btnOk.FillColor = Color.FromArgb(0, 190, 172);
            btnOk.FillColor2 = Color.FromArgb(0, 190, 172);
            btnOk.FillHoverColor = Color.FromArgb(51, 203, 189);
            btnOk.FillPressColor = Color.FromArgb(0, 152, 138);
            btnOk.FillSelectedColor = Color.FromArgb(0, 152, 138);
            btnOk.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnOk.Location = new Point(169, 249);
            btnOk.MinimumSize = new Size(1, 1);
            btnOk.Name = "btnOk";
            btnOk.RectColor = Color.FromArgb(0, 190, 172);
            btnOk.RectHoverColor = Color.FromArgb(51, 203, 189);
            btnOk.RectPressColor = Color.FromArgb(0, 152, 138);
            btnOk.RectSelectedColor = Color.FromArgb(0, 152, 138);
            btnOk.Size = new Size(240, 44);
            btnOk.Style = Sunny.UI.UIStyle.Colorful;
            btnOk.TabIndex = 4;
            btnOk.Text = "确认";
            btnOk.Click += btnOk_Click;
            // 
            // FrmChangePassword
            // 
            AcceptButton = btnOk;
            AutoScaleMode = AutoScaleMode.None;
            ClientSize = new Size(542, 335);
            ControlBoxFillHoverColor = Color.FromArgb(51, 203, 189);
            Controls.Add(btnOk);
            Controls.Add(txtNewPassword2);
            Controls.Add(uiLabel3);
            Controls.Add(txtNewPassword1);
            Controls.Add(uiLabel2);
            Controls.Add(txtOldPassword);
            Controls.Add(uiLabel1);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FrmChangePassword";
            RectColor = Color.FromArgb(0, 190, 172);
            ShowIcon = false;
            Style = Sunny.UI.UIStyle.Colorful;
            Text = "修改密码";
            TitleColor = Color.FromArgb(0, 190, 172);
            ZoomScaleRect = new Rectangle(19, 19, 800, 450);
            ResumeLayout(false);
        }

        #endregion

        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UITextBox txtOldPassword;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UITextBox txtNewPassword1;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UITextBox txtNewPassword2;
        private Sunny.UI.UIButton btnOk;
    }
}