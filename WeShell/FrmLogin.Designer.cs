﻿namespace WeShell
{
    partial class FrmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmLogin));
            btnLogin = new Sunny.UI.UIButton();
            txtAccount = new Sunny.UI.UITextBox();
            txtPassword = new Sunny.UI.UITextBox();
            label1 = new Label();
            linkLabel = new LinkLabel();
            SuspendLayout();
            // 
            // btnLogin
            // 
            btnLogin.FillColor = Color.FromArgb(0, 190, 172);
            btnLogin.FillColor2 = Color.FromArgb(0, 190, 172);
            btnLogin.FillHoverColor = Color.FromArgb(51, 203, 189);
            btnLogin.FillPressColor = Color.FromArgb(0, 152, 138);
            btnLogin.FillSelectedColor = Color.FromArgb(0, 152, 138);
            btnLogin.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnLogin.Location = new Point(109, 287);
            btnLogin.MinimumSize = new Size(1, 1);
            btnLogin.Name = "btnLogin";
            btnLogin.RectColor = Color.FromArgb(0, 190, 172);
            btnLogin.RectHoverColor = Color.FromArgb(51, 203, 189);
            btnLogin.RectPressColor = Color.FromArgb(0, 152, 138);
            btnLogin.RectSelectedColor = Color.FromArgb(0, 152, 138);
            btnLogin.Size = new Size(331, 44);
            btnLogin.Style = Sunny.UI.UIStyle.Colorful;
            btnLogin.TabIndex = 3;
            btnLogin.Text = "登录";
            btnLogin.TipsFont = new Font("宋体", 9F, FontStyle.Regular, GraphicsUnit.Point);
            btnLogin.Click += btnLogin_Click;
            // 
            // txtAccount
            // 
            txtAccount.ButtonFillColor = Color.FromArgb(0, 190, 172);
            txtAccount.ButtonFillHoverColor = Color.FromArgb(51, 203, 189);
            txtAccount.ButtonFillPressColor = Color.FromArgb(0, 152, 138);
            txtAccount.ButtonRectColor = Color.FromArgb(0, 190, 172);
            txtAccount.ButtonRectHoverColor = Color.FromArgb(51, 203, 189);
            txtAccount.ButtonRectPressColor = Color.FromArgb(0, 152, 138);
            txtAccount.FillColor2 = Color.FromArgb(238, 251, 250);
            txtAccount.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtAccount.Location = new Point(109, 120);
            txtAccount.Margin = new Padding(4, 5, 4, 5);
            txtAccount.MinimumSize = new Size(1, 16);
            txtAccount.Name = "txtAccount";
            txtAccount.Padding = new Padding(5);
            txtAccount.RectColor = Color.FromArgb(0, 190, 172);
            txtAccount.ScrollBarColor = Color.FromArgb(0, 190, 172);
            txtAccount.ShowText = false;
            txtAccount.Size = new Size(331, 36);
            txtAccount.Style = Sunny.UI.UIStyle.Colorful;
            txtAccount.TabIndex = 1;
            txtAccount.TextAlignment = ContentAlignment.MiddleLeft;
            txtAccount.Watermark = "请输入用户名";
            txtAccount.KeyDown += txtAccount_KeyDown;
            // 
            // txtPassword
            // 
            txtPassword.ButtonFillColor = Color.FromArgb(0, 190, 172);
            txtPassword.ButtonFillHoverColor = Color.FromArgb(51, 203, 189);
            txtPassword.ButtonFillPressColor = Color.FromArgb(0, 152, 138);
            txtPassword.ButtonRectColor = Color.FromArgb(0, 190, 172);
            txtPassword.ButtonRectHoverColor = Color.FromArgb(51, 203, 189);
            txtPassword.ButtonRectPressColor = Color.FromArgb(0, 152, 138);
            txtPassword.FillColor2 = Color.FromArgb(238, 251, 250);
            txtPassword.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtPassword.Location = new Point(109, 206);
            txtPassword.Margin = new Padding(4, 5, 4, 5);
            txtPassword.MinimumSize = new Size(1, 16);
            txtPassword.Name = "txtPassword";
            txtPassword.Padding = new Padding(5);
            txtPassword.PasswordChar = '*';
            txtPassword.RectColor = Color.FromArgb(0, 190, 172);
            txtPassword.ScrollBarColor = Color.FromArgb(0, 190, 172);
            txtPassword.ShowText = false;
            txtPassword.Size = new Size(331, 36);
            txtPassword.Style = Sunny.UI.UIStyle.Colorful;
            txtPassword.TabIndex = 2;
            txtPassword.TextAlignment = ContentAlignment.MiddleLeft;
            txtPassword.Watermark = "请输入密码";
            txtPassword.KeyDown += txtPassword_KeyDown;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.ForeColor = Color.Silver;
            label1.Location = new Point(241, 382);
            label1.Name = "label1";
            label1.Size = new Size(199, 20);
            label1.TabIndex = 4;
            label1.Text = "design by HandLoong";
            // 
            // linkLabel
            // 
            linkLabel.AutoSize = true;
            linkLabel.Font = new Font("宋体", 9F, FontStyle.Regular, GraphicsUnit.Point);
            linkLabel.ForeColor = Color.Silver;
            linkLabel.LinkBehavior = LinkBehavior.HoverUnderline;
            linkLabel.LinkColor = Color.Silver;
            linkLabel.Location = new Point(109, 386);
            linkLabel.Name = "linkLabel";
            linkLabel.Size = new Size(67, 15);
            linkLabel.TabIndex = 5;
            linkLabel.TabStop = true;
            linkLabel.Text = "开源地址";
            linkLabel.LinkClicked += linkLabel_LinkClicked;
            // 
            // FrmLogin
            // 
            AutoScaleMode = AutoScaleMode.None;
            ClientSize = new Size(553, 428);
            ControlBoxFillHoverColor = Color.FromArgb(51, 203, 189);
            Controls.Add(linkLabel);
            Controls.Add(label1);
            Controls.Add(txtPassword);
            Controls.Add(txtAccount);
            Controls.Add(btnLogin);
            Icon = (Icon)resources.GetObject("$this.Icon");
            MaximizeBox = false;
            Name = "FrmLogin";
            RectColor = Color.FromArgb(0, 190, 172);
            Style = Sunny.UI.UIStyle.Colorful;
            Text = "WeShell";
            TitleColor = Color.FromArgb(0, 190, 172);
            ZoomScaleRect = new Rectangle(19, 19, 800, 450);
            Shown += FrmLogin_Shown;
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Sunny.UI.UIButton btnLogin;
        private Sunny.UI.UITextBox txtAccount;
        private Sunny.UI.UITextBox txtPassword;
        private Label label1;
        private LinkLabel linkLabel;
    }
}