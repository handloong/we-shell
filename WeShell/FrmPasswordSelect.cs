﻿using Sunny.UI;
using WeShellCore;

namespace WeShell
{
    public partial class FrmPasswordSelect : UIForm
    {
        private readonly List<ServerAuth> _serverAuths;
        private readonly Action<ServerAuth> _action;

        public FrmPasswordSelect(List<ServerAuth> serverAuths, Action<ServerAuth> action)
        {
            InitializeComponent();
            _serverAuths = serverAuths;
            _action = action;
            foreach (var item in _serverAuths)
            {
                UIButton uIButton = new UIButton();
                uIButton.Text = item.UserName;
                uIButton.Tag = item;
                uIButton.Width = this.Width - 10;

                uIButton.Click += UIButton_Click;
                uiFlowLayoutPanel.Controls.Add(uIButton);
            }

            UIStyles.InitColorful(EasyConf.Read<Config>().GetColorFul(), Color.White);
        }

        private void UIButton_Click(object sender, EventArgs e)
        {
            var obj = (sender as UIButton).Tag as ServerAuth;
            _action.Invoke(obj);
            this.Close();
        }
    }
}
