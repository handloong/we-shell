﻿using WeShellCore;

namespace WeShell
{
    public class BaseResponse
    {
        public bool Successful { get; set; }

        public string Message { get; set; }

        public object Data { get; set; }

        public PageResponse Page { get; set; }
    }
}