﻿namespace WeShell
{
    partial class FrmEditUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DataGridViewCellStyle dataGridViewCellStyle11 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle12 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle13 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle14 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle15 = new DataGridViewCellStyle();
            txtQueryAccount = new Sunny.UI.UITextBox();
            txtQueryName = new Sunny.UI.UITextBox();
            btnGetPageList = new Sunny.UI.UISymbolButton();
            btnDelete = new Sunny.UI.UISymbolButton();
            dgvUsers = new Sunny.UI.UIDataGridView();
            T_Id = new DataGridViewTextBoxColumn();
            T_Account = new DataGridViewTextBoxColumn();
            T_Name = new DataGridViewTextBoxColumn();
            T_Status = new DataGridViewTextBoxColumn();
            T_SuperAdmin = new DataGridViewTextBoxColumn();
            T_Password = new DataGridViewTextBoxColumn();
            uiPagination = new Sunny.UI.UIPagination();
            uiLabel1 = new Sunny.UI.UILabel();
            uiLabel2 = new Sunny.UI.UILabel();
            cbStatus = new Sunny.UI.UICheckBox();
            cbSuperAdmin = new Sunny.UI.UICheckBox();
            cboTreeGroups = new Sunny.UI.UIComboTreeView();
            uiLabel3 = new Sunny.UI.UILabel();
            btnSave = new Sunny.UI.UIButton();
            lblId = new Sunny.UI.UILabel();
            txtAccount = new Sunny.UI.UITextBox();
            txtName = new Sunny.UI.UITextBox();
            btnAdd = new Sunny.UI.UIButton();
            uiLabel4 = new Sunny.UI.UILabel();
            btnResetPassword = new Sunny.UI.UISymbolButton();
            ((System.ComponentModel.ISupportInitialize)dgvUsers).BeginInit();
            SuspendLayout();
            // 
            // txtQueryAccount
            // 
            txtQueryAccount.ButtonFillColor = Color.FromArgb(0, 190, 172);
            txtQueryAccount.ButtonFillHoverColor = Color.FromArgb(51, 203, 189);
            txtQueryAccount.ButtonFillPressColor = Color.FromArgb(0, 152, 138);
            txtQueryAccount.ButtonRectColor = Color.FromArgb(0, 190, 172);
            txtQueryAccount.ButtonRectHoverColor = Color.FromArgb(51, 203, 189);
            txtQueryAccount.ButtonRectPressColor = Color.FromArgb(0, 152, 138);
            txtQueryAccount.FillColor2 = Color.FromArgb(238, 251, 250);
            txtQueryAccount.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtQueryAccount.Location = new Point(25, 59);
            txtQueryAccount.Margin = new Padding(4, 5, 4, 5);
            txtQueryAccount.MinimumSize = new Size(1, 16);
            txtQueryAccount.Name = "txtQueryAccount";
            txtQueryAccount.Padding = new Padding(5);
            txtQueryAccount.RectColor = Color.FromArgb(0, 190, 172);
            txtQueryAccount.ScrollBarColor = Color.FromArgb(0, 190, 172);
            txtQueryAccount.ShowText = false;
            txtQueryAccount.Size = new Size(128, 36);
            txtQueryAccount.Style = Sunny.UI.UIStyle.Colorful;
            txtQueryAccount.TabIndex = 1;
            txtQueryAccount.TextAlignment = ContentAlignment.MiddleLeft;
            txtQueryAccount.Watermark = "账号";
            // 
            // txtQueryName
            // 
            txtQueryName.ButtonFillColor = Color.FromArgb(0, 190, 172);
            txtQueryName.ButtonFillHoverColor = Color.FromArgb(51, 203, 189);
            txtQueryName.ButtonFillPressColor = Color.FromArgb(0, 152, 138);
            txtQueryName.ButtonRectColor = Color.FromArgb(0, 190, 172);
            txtQueryName.ButtonRectHoverColor = Color.FromArgb(51, 203, 189);
            txtQueryName.ButtonRectPressColor = Color.FromArgb(0, 152, 138);
            txtQueryName.FillColor2 = Color.FromArgb(238, 251, 250);
            txtQueryName.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtQueryName.Location = new Point(173, 59);
            txtQueryName.Margin = new Padding(4, 5, 4, 5);
            txtQueryName.MinimumSize = new Size(1, 16);
            txtQueryName.Name = "txtQueryName";
            txtQueryName.Padding = new Padding(5);
            txtQueryName.RectColor = Color.FromArgb(0, 190, 172);
            txtQueryName.ScrollBarColor = Color.FromArgb(0, 190, 172);
            txtQueryName.ShowText = false;
            txtQueryName.Size = new Size(117, 36);
            txtQueryName.Style = Sunny.UI.UIStyle.Colorful;
            txtQueryName.TabIndex = 2;
            txtQueryName.TextAlignment = ContentAlignment.MiddleLeft;
            txtQueryName.Watermark = "姓名";
            // 
            // btnGetPageList
            // 
            btnGetPageList.Cursor = Cursors.Hand;
            btnGetPageList.FillColor = Color.FromArgb(0, 190, 172);
            btnGetPageList.FillColor2 = Color.FromArgb(0, 190, 172);
            btnGetPageList.FillHoverColor = Color.FromArgb(51, 203, 189);
            btnGetPageList.FillPressColor = Color.FromArgb(0, 152, 138);
            btnGetPageList.FillSelectedColor = Color.FromArgb(0, 152, 138);
            btnGetPageList.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnGetPageList.Location = new Point(470, 60);
            btnGetPageList.MinimumSize = new Size(1, 1);
            btnGetPageList.Name = "btnGetPageList";
            btnGetPageList.RadiusSides = Sunny.UI.UICornerRadiusSides.LeftTop | Sunny.UI.UICornerRadiusSides.LeftBottom;
            btnGetPageList.RectColor = Color.FromArgb(0, 190, 172);
            btnGetPageList.RectHoverColor = Color.FromArgb(51, 203, 189);
            btnGetPageList.RectPressColor = Color.FromArgb(0, 152, 138);
            btnGetPageList.RectSelectedColor = Color.FromArgb(0, 152, 138);
            btnGetPageList.Size = new Size(46, 35);
            btnGetPageList.Style = Sunny.UI.UIStyle.Colorful;
            btnGetPageList.Symbol = 61473;
            btnGetPageList.TabIndex = 3;
            btnGetPageList.TipsFont = new Font("宋体", 9F, FontStyle.Regular, GraphicsUnit.Point);
            btnGetPageList.Click += btnGetPageList_Click;
            // 
            // btnDelete
            // 
            btnDelete.Cursor = Cursors.Hand;
            btnDelete.FillColor = Color.FromArgb(0, 190, 172);
            btnDelete.FillColor2 = Color.FromArgb(0, 190, 172);
            btnDelete.FillHoverColor = Color.FromArgb(51, 203, 189);
            btnDelete.FillPressColor = Color.FromArgb(0, 152, 138);
            btnDelete.FillSelectedColor = Color.FromArgb(0, 152, 138);
            btnDelete.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnDelete.Location = new Point(562, 60);
            btnDelete.MinimumSize = new Size(1, 1);
            btnDelete.Name = "btnDelete";
            btnDelete.RadiusSides = Sunny.UI.UICornerRadiusSides.RightTop | Sunny.UI.UICornerRadiusSides.RightBottom;
            btnDelete.RectColor = Color.FromArgb(0, 190, 172);
            btnDelete.RectHoverColor = Color.FromArgb(51, 203, 189);
            btnDelete.RectPressColor = Color.FromArgb(0, 152, 138);
            btnDelete.RectSelectedColor = Color.FromArgb(0, 152, 138);
            btnDelete.Size = new Size(46, 35);
            btnDelete.Style = Sunny.UI.UIStyle.Colorful;
            btnDelete.Symbol = 559691;
            btnDelete.TabIndex = 4;
            btnDelete.Click += btnDelete_Click;
            // 
            // dgvUsers
            // 
            dgvUsers.AllowUserToAddRows = false;
            dgvUsers.AllowUserToDeleteRows = false;
            dgvUsers.AllowUserToResizeColumns = false;
            dgvUsers.AllowUserToResizeRows = false;
            dataGridViewCellStyle11.BackColor = Color.FromArgb(238, 251, 250);
            dgvUsers.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle11;
            dgvUsers.BackgroundColor = Color.FromArgb(238, 251, 250);
            dgvUsers.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle12.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = Color.FromArgb(0, 190, 172);
            dataGridViewCellStyle12.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle12.ForeColor = Color.White;
            dataGridViewCellStyle12.SelectionBackColor = Color.FromArgb(0, 190, 172);
            dataGridViewCellStyle12.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = DataGridViewTriState.True;
            dgvUsers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle12;
            dgvUsers.ColumnHeadersHeight = 32;
            dgvUsers.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dgvUsers.Columns.AddRange(new DataGridViewColumn[] { T_Id, T_Account, T_Name, T_Status, T_SuperAdmin, T_Password });
            dataGridViewCellStyle13.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = Color.White;
            dataGridViewCellStyle13.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle13.ForeColor = Color.FromArgb(48, 48, 48);
            dataGridViewCellStyle13.SelectionBackColor = Color.FromArgb(204, 242, 238);
            dataGridViewCellStyle13.SelectionForeColor = Color.FromArgb(48, 48, 48);
            dataGridViewCellStyle13.WrapMode = DataGridViewTriState.False;
            dgvUsers.DefaultCellStyle = dataGridViewCellStyle13;
            dgvUsers.EditMode = DataGridViewEditMode.EditOnEnter;
            dgvUsers.EnableHeadersVisualStyles = false;
            dgvUsers.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            dgvUsers.GridColor = Color.FromArgb(34, 199, 183);
            dgvUsers.Location = new Point(25, 119);
            dgvUsers.Name = "dgvUsers";
            dgvUsers.ReadOnly = true;
            dgvUsers.RectColor = Color.FromArgb(0, 190, 172);
            dataGridViewCellStyle14.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = Color.FromArgb(238, 251, 250);
            dataGridViewCellStyle14.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle14.ForeColor = Color.FromArgb(48, 48, 48);
            dataGridViewCellStyle14.SelectionBackColor = Color.FromArgb(0, 190, 172);
            dataGridViewCellStyle14.SelectionForeColor = Color.FromArgb(48, 48, 48);
            dataGridViewCellStyle14.WrapMode = DataGridViewTriState.True;
            dgvUsers.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            dgvUsers.RowHeadersVisible = false;
            dgvUsers.RowHeadersWidth = 51;
            dataGridViewCellStyle15.BackColor = Color.White;
            dataGridViewCellStyle15.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle15.ForeColor = Color.FromArgb(48, 48, 48);
            dataGridViewCellStyle15.SelectionBackColor = Color.FromArgb(204, 242, 238);
            dataGridViewCellStyle15.SelectionForeColor = Color.FromArgb(48, 48, 48);
            dgvUsers.RowsDefaultCellStyle = dataGridViewCellStyle15;
            dgvUsers.RowTemplate.Height = 29;
            dgvUsers.ScrollBarBackColor = Color.FromArgb(238, 251, 250);
            dgvUsers.ScrollBarColor = Color.FromArgb(0, 190, 172);
            dgvUsers.ScrollBarRectColor = Color.FromArgb(0, 190, 172);
            dgvUsers.ScrollBars = ScrollBars.None;
            dgvUsers.SelectedIndex = -1;
            dgvUsers.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvUsers.Size = new Size(583, 413);
            dgvUsers.StripeOddColor = Color.FromArgb(238, 251, 250);
            dgvUsers.Style = Sunny.UI.UIStyle.Colorful;
            dgvUsers.TabIndex = 9;
            dgvUsers.CellDoubleClick += dgvUsers_CellDoubleClick;
            // 
            // T_Id
            // 
            T_Id.DataPropertyName = "Id";
            T_Id.HeaderText = "Id";
            T_Id.MinimumWidth = 6;
            T_Id.Name = "T_Id";
            T_Id.ReadOnly = true;
            T_Id.Visible = false;
            T_Id.Width = 125;
            // 
            // T_Account
            // 
            T_Account.DataPropertyName = "Account";
            T_Account.HeaderText = "账号";
            T_Account.MinimumWidth = 6;
            T_Account.Name = "T_Account";
            T_Account.ReadOnly = true;
            T_Account.Width = 150;
            // 
            // T_Name
            // 
            T_Name.DataPropertyName = "Name";
            T_Name.HeaderText = "姓名";
            T_Name.MinimumWidth = 6;
            T_Name.Name = "T_Name";
            T_Name.ReadOnly = true;
            T_Name.Width = 180;
            // 
            // T_Status
            // 
            T_Status.DataPropertyName = "Status";
            T_Status.HeaderText = "状态";
            T_Status.MinimumWidth = 6;
            T_Status.Name = "T_Status";
            T_Status.ReadOnly = true;
            T_Status.Width = 125;
            // 
            // T_SuperAdmin
            // 
            T_SuperAdmin.DataPropertyName = "SuperAdmin";
            T_SuperAdmin.HeaderText = "超管";
            T_SuperAdmin.MinimumWidth = 6;
            T_SuperAdmin.Name = "T_SuperAdmin";
            T_SuperAdmin.ReadOnly = true;
            T_SuperAdmin.Width = 125;
            // 
            // T_Password
            // 
            T_Password.DataPropertyName = "Password";
            T_Password.HeaderText = "Password";
            T_Password.MinimumWidth = 6;
            T_Password.Name = "T_Password";
            T_Password.ReadOnly = true;
            T_Password.Visible = false;
            T_Password.Width = 125;
            // 
            // uiPagination
            // 
            uiPagination.FillColor = Color.FromArgb(238, 251, 250);
            uiPagination.FillColor2 = Color.FromArgb(238, 251, 250);
            uiPagination.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiPagination.Location = new Point(25, 536);
            uiPagination.Margin = new Padding(4, 5, 4, 5);
            uiPagination.MinimumSize = new Size(1, 1);
            uiPagination.Name = "uiPagination";
            uiPagination.PageSize = 30;
            uiPagination.RectColor = Color.FromArgb(0, 190, 172);
            uiPagination.RectSides = ToolStripStatusLabelBorderSides.None;
            uiPagination.ShowText = false;
            uiPagination.Size = new Size(665, 44);
            uiPagination.Style = Sunny.UI.UIStyle.Colorful;
            uiPagination.TabIndex = 11;
            uiPagination.Text = null;
            uiPagination.TextAlignment = ContentAlignment.MiddleCenter;
            // 
            // uiLabel1
            // 
            uiLabel1.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiLabel1.Location = new Point(647, 119);
            uiLabel1.Name = "uiLabel1";
            uiLabel1.Size = new Size(60, 29);
            uiLabel1.Style = Sunny.UI.UIStyle.Colorful;
            uiLabel1.TabIndex = 12;
            uiLabel1.Text = "账号*";
            uiLabel1.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            uiLabel2.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiLabel2.Location = new Point(647, 182);
            uiLabel2.Name = "uiLabel2";
            uiLabel2.Size = new Size(60, 29);
            uiLabel2.Style = Sunny.UI.UIStyle.Colorful;
            uiLabel2.TabIndex = 12;
            uiLabel2.Text = "姓名*";
            uiLabel2.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // cbStatus
            // 
            cbStatus.CheckBoxColor = Color.FromArgb(0, 190, 172);
            cbStatus.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            cbStatus.Location = new Point(714, 228);
            cbStatus.MinimumSize = new Size(1, 1);
            cbStatus.Name = "cbStatus";
            cbStatus.Size = new Size(115, 36);
            cbStatus.Style = Sunny.UI.UIStyle.Colorful;
            cbStatus.TabIndex = 7;
            cbStatus.Text = "有效";
            // 
            // cbSuperAdmin
            // 
            cbSuperAdmin.CheckBoxColor = Color.FromArgb(0, 190, 172);
            cbSuperAdmin.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            cbSuperAdmin.Location = new Point(851, 228);
            cbSuperAdmin.MinimumSize = new Size(1, 1);
            cbSuperAdmin.Name = "cbSuperAdmin";
            cbSuperAdmin.Size = new Size(163, 36);
            cbSuperAdmin.Style = Sunny.UI.UIStyle.Colorful;
            cbSuperAdmin.TabIndex = 8;
            cbSuperAdmin.Text = "超管";
            // 
            // cboTreeGroups
            // 
            cboTreeGroups.CheckBoxes = true;
            cboTreeGroups.DropDownStyle = Sunny.UI.UIDropDownStyle.DropDownList;
            cboTreeGroups.FillColor = Color.White;
            cboTreeGroups.FillColor2 = Color.FromArgb(238, 251, 250);
            cboTreeGroups.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            cboTreeGroups.Location = new Point(714, 270);
            cboTreeGroups.Margin = new Padding(4, 5, 4, 5);
            cboTreeGroups.MinimumSize = new Size(63, 0);
            cboTreeGroups.Name = "cboTreeGroups";
            cboTreeGroups.Padding = new Padding(0, 0, 30, 2);
            cboTreeGroups.RectColor = Color.FromArgb(0, 190, 172);
            cboTreeGroups.Size = new Size(284, 36);
            cboTreeGroups.Style = Sunny.UI.UIStyle.Colorful;
            cboTreeGroups.TabIndex = 9;
            cboTreeGroups.TextAlignment = ContentAlignment.MiddleLeft;
            cboTreeGroups.Watermark = "";
            // 
            // uiLabel3
            // 
            uiLabel3.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiLabel3.Location = new Point(647, 275);
            uiLabel3.Name = "uiLabel3";
            uiLabel3.Size = new Size(60, 29);
            uiLabel3.Style = Sunny.UI.UIStyle.Colorful;
            uiLabel3.TabIndex = 12;
            uiLabel3.Text = "权限";
            uiLabel3.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // btnSave
            // 
            btnSave.FillColor = Color.FromArgb(0, 190, 172);
            btnSave.FillColor2 = Color.FromArgb(0, 190, 172);
            btnSave.FillHoverColor = Color.FromArgb(51, 203, 189);
            btnSave.FillPressColor = Color.FromArgb(0, 152, 138);
            btnSave.FillSelectedColor = Color.FromArgb(0, 152, 138);
            btnSave.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnSave.Location = new Point(873, 418);
            btnSave.MinimumSize = new Size(1, 1);
            btnSave.Name = "btnSave";
            btnSave.RectColor = Color.FromArgb(0, 190, 172);
            btnSave.RectHoverColor = Color.FromArgb(51, 203, 189);
            btnSave.RectPressColor = Color.FromArgb(0, 152, 138);
            btnSave.RectSelectedColor = Color.FromArgb(0, 152, 138);
            btnSave.Size = new Size(125, 44);
            btnSave.Style = Sunny.UI.UIStyle.Colorful;
            btnSave.TabIndex = 10;
            btnSave.Text = "编辑";
            btnSave.TipsFont = new Font("宋体", 9F, FontStyle.Regular, GraphicsUnit.Point);
            btnSave.Click += btnSave_Click;
            // 
            // lblId
            // 
            lblId.BackColor = Color.Red;
            lblId.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            lblId.Location = new Point(714, 59);
            lblId.Name = "lblId";
            lblId.Size = new Size(125, 29);
            lblId.Style = Sunny.UI.UIStyle.Colorful;
            lblId.TabIndex = 16;
            lblId.TextAlign = ContentAlignment.MiddleLeft;
            lblId.Visible = false;
            // 
            // txtAccount
            // 
            txtAccount.ButtonFillColor = Color.FromArgb(0, 190, 172);
            txtAccount.ButtonFillHoverColor = Color.FromArgb(51, 203, 189);
            txtAccount.ButtonFillPressColor = Color.FromArgb(0, 152, 138);
            txtAccount.ButtonRectColor = Color.FromArgb(0, 190, 172);
            txtAccount.ButtonRectHoverColor = Color.FromArgb(51, 203, 189);
            txtAccount.ButtonRectPressColor = Color.FromArgb(0, 152, 138);
            txtAccount.FillColor2 = Color.FromArgb(238, 251, 250);
            txtAccount.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtAccount.Location = new Point(714, 112);
            txtAccount.Margin = new Padding(4, 5, 4, 5);
            txtAccount.MinimumSize = new Size(1, 16);
            txtAccount.Name = "txtAccount";
            txtAccount.Padding = new Padding(5);
            txtAccount.RectColor = Color.FromArgb(0, 190, 172);
            txtAccount.ScrollBarColor = Color.FromArgb(0, 190, 172);
            txtAccount.ShowText = false;
            txtAccount.Size = new Size(284, 36);
            txtAccount.Style = Sunny.UI.UIStyle.Colorful;
            txtAccount.TabIndex = 5;
            txtAccount.TextAlignment = ContentAlignment.MiddleLeft;
            txtAccount.Watermark = "";
            // 
            // txtName
            // 
            txtName.ButtonFillColor = Color.FromArgb(0, 190, 172);
            txtName.ButtonFillHoverColor = Color.FromArgb(51, 203, 189);
            txtName.ButtonFillPressColor = Color.FromArgb(0, 152, 138);
            txtName.ButtonRectColor = Color.FromArgb(0, 190, 172);
            txtName.ButtonRectHoverColor = Color.FromArgb(51, 203, 189);
            txtName.ButtonRectPressColor = Color.FromArgb(0, 152, 138);
            txtName.FillColor2 = Color.FromArgb(238, 251, 250);
            txtName.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtName.Location = new Point(714, 175);
            txtName.Margin = new Padding(4, 5, 4, 5);
            txtName.MinimumSize = new Size(1, 16);
            txtName.Name = "txtName";
            txtName.Padding = new Padding(5);
            txtName.RectColor = Color.FromArgb(0, 190, 172);
            txtName.ScrollBarColor = Color.FromArgb(0, 190, 172);
            txtName.ShowText = false;
            txtName.Size = new Size(284, 36);
            txtName.Style = Sunny.UI.UIStyle.Colorful;
            txtName.TabIndex = 6;
            txtName.TextAlignment = ContentAlignment.MiddleLeft;
            txtName.Watermark = "";
            // 
            // btnAdd
            // 
            btnAdd.FillColor = Color.FromArgb(0, 190, 172);
            btnAdd.FillColor2 = Color.FromArgb(0, 190, 172);
            btnAdd.FillHoverColor = Color.FromArgb(51, 203, 189);
            btnAdd.FillPressColor = Color.FromArgb(0, 152, 138);
            btnAdd.FillSelectedColor = Color.FromArgb(0, 152, 138);
            btnAdd.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnAdd.Location = new Point(714, 418);
            btnAdd.MinimumSize = new Size(1, 1);
            btnAdd.Name = "btnAdd";
            btnAdd.RectColor = Color.FromArgb(0, 190, 172);
            btnAdd.RectHoverColor = Color.FromArgb(51, 203, 189);
            btnAdd.RectPressColor = Color.FromArgb(0, 152, 138);
            btnAdd.RectSelectedColor = Color.FromArgb(0, 152, 138);
            btnAdd.Size = new Size(125, 44);
            btnAdd.Style = Sunny.UI.UIStyle.Colorful;
            btnAdd.TabIndex = 17;
            btnAdd.Text = "新增";
            btnAdd.Click += btnAdd_Click;
            // 
            // uiLabel4
            // 
            uiLabel4.Font = new Font("宋体", 8F, FontStyle.Regular, GraphicsUnit.Point);
            uiLabel4.Location = new Point(714, 339);
            uiLabel4.Name = "uiLabel4";
            uiLabel4.Size = new Size(366, 29);
            uiLabel4.Style = Sunny.UI.UIStyle.Colorful;
            uiLabel4.TabIndex = 18;
            uiLabel4.Text = "新增用户密码默认123456";
            uiLabel4.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // btnResetPassword
            // 
            btnResetPassword.Cursor = Cursors.Hand;
            btnResetPassword.FillColor = Color.FromArgb(0, 190, 172);
            btnResetPassword.FillColor2 = Color.FromArgb(0, 190, 172);
            btnResetPassword.FillHoverColor = Color.FromArgb(51, 203, 189);
            btnResetPassword.FillPressColor = Color.FromArgb(0, 152, 138);
            btnResetPassword.FillSelectedColor = Color.FromArgb(0, 152, 138);
            btnResetPassword.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnResetPassword.Location = new Point(516, 60);
            btnResetPassword.MinimumSize = new Size(1, 1);
            btnResetPassword.Name = "btnResetPassword";
            btnResetPassword.RadiusSides = Sunny.UI.UICornerRadiusSides.None;
            btnResetPassword.RectColor = Color.FromArgb(0, 190, 172);
            btnResetPassword.RectHoverColor = Color.FromArgb(51, 203, 189);
            btnResetPassword.RectPressColor = Color.FromArgb(0, 152, 138);
            btnResetPassword.RectSelectedColor = Color.FromArgb(0, 152, 138);
            btnResetPassword.Size = new Size(46, 35);
            btnResetPassword.Style = Sunny.UI.UIStyle.Colorful;
            btnResetPassword.Symbol = 61475;
            btnResetPassword.TabIndex = 3;
            btnResetPassword.Click += btnResetPassword_Click;
            // 
            // FrmEditUser
            // 
            AutoScaleMode = AutoScaleMode.None;
            ClientSize = new Size(1089, 585);
            ControlBoxFillHoverColor = Color.FromArgb(51, 203, 189);
            Controls.Add(uiLabel4);
            Controls.Add(btnAdd);
            Controls.Add(lblId);
            Controls.Add(btnSave);
            Controls.Add(cboTreeGroups);
            Controls.Add(cbSuperAdmin);
            Controls.Add(cbStatus);
            Controls.Add(uiLabel3);
            Controls.Add(uiLabel2);
            Controls.Add(uiLabel1);
            Controls.Add(txtName);
            Controls.Add(txtAccount);
            Controls.Add(uiPagination);
            Controls.Add(dgvUsers);
            Controls.Add(btnDelete);
            Controls.Add(btnResetPassword);
            Controls.Add(btnGetPageList);
            Controls.Add(txtQueryName);
            Controls.Add(txtQueryAccount);
            Name = "FrmEditUser";
            RectColor = Color.FromArgb(0, 190, 172);
            Style = Sunny.UI.UIStyle.Colorful;
            Text = "用户管理";
            TitleColor = Color.FromArgb(0, 190, 172);
            ZoomScaleRect = new Rectangle(19, 19, 800, 450);
            Load += FrmEditUser_Load;
            ((System.ComponentModel.ISupportInitialize)dgvUsers).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private Sunny.UI.UITextBox txtQueryAccount;
        private Sunny.UI.UITextBox txtQueryName;
        private Sunny.UI.UISymbolButton btnGetPageList;
        private Sunny.UI.UISymbolButton btnDelete;
        private Sunny.UI.UIDataGridView dgvUsers;
        private Sunny.UI.UIPagination uiPagination;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UICheckBox cbStatus;
        private Sunny.UI.UICheckBox cbSuperAdmin;
        private Sunny.UI.UIComboTreeView cboTreeGroups;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UIButton btnSave;
        private Sunny.UI.UILabel lblId;
        private Sunny.UI.UITextBox txtAccount;
        private Sunny.UI.UITextBox txtName;
        private DataGridViewTextBoxColumn T_Id;
        private DataGridViewTextBoxColumn T_Account;
        private DataGridViewTextBoxColumn T_Name;
        private DataGridViewTextBoxColumn T_Status;
        private DataGridViewTextBoxColumn T_SuperAdmin;
        private DataGridViewTextBoxColumn T_Password;
        private Sunny.UI.UIButton btnAdd;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UISymbolButton btnResetPassword;
    }
}