﻿using Sunny.UI;

namespace WeShell
{
    public class Config
    {
        public string T1 { get; set; }//用户名
        public string C1 { get; set; }
        public string L1 { get; set; }
        public string L2 { get; set; }


        public Color GetColorFul()
        {
            try
            {
                if (string.IsNullOrWhiteSpace(C1))
                    return Color.FromArgb(0, 190, 172);

                var rgb = C1.Decrypt().Split('.');
                var r = Convert.ToInt32(rgb[0]);
                var g = Convert.ToInt32(rgb[1]);
                var b = Convert.ToInt32(rgb[2]);
                return Color.FromArgb(r, g, b);
            }
            catch
            {
                return Color.FromArgb(0, 190, 172);
            }
        }
        public Size GetSize(Size @default)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(L1))
                    return @default;

                var xys = L1.Decrypt().Split('.');
                var x = Convert.ToInt32(xys[0]);
                var y = Convert.ToInt32(xys[1]);
                return new Size(x, y);
            }
            catch
            {
                return @default;
            }
        }
        public Point GetLocation(Point @default)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(L2))
                    return @default;
                var xys = L2.Decrypt().Split('.');
                var x = Convert.ToInt32(xys[0]);
                var y = Convert.ToInt32(xys[1]);
                return new Point(x, y);
            }
            catch
            {
                return @default;
            }
        }
        public void SetSize(Size size)
        {
            L1 = $"{size.Width}.{size.Height}".Encrypt();
        }
        public void SetLocation(Point point)
        {
            L2 = $"{point.X}.{point.Y}".Encrypt();
        }
        public void SetColorful(int r, int g, int b)
        {
            C1 = $"{r}.{g}.{b}".Encrypt();
        }
    }
}
