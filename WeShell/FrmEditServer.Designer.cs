﻿namespace WeShell
{
    partial class FrmEditServer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DataGridViewCellStyle dataGridViewCellStyle7 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle8 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle10 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle11 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle12 = new DataGridViewCellStyle();
            DataGridViewCellStyle dataGridViewCellStyle9 = new DataGridViewCellStyle();
            uiLabel1 = new Sunny.UI.UILabel();
            uiLabel2 = new Sunny.UI.UILabel();
            uiLabel3 = new Sunny.UI.UILabel();
            txtPort = new Sunny.UI.UITextBox();
            txtName = new Sunny.UI.UITextBox();
            txtIP = new Sunny.UI.UITextBox();
            rdoSSH = new Sunny.UI.UIRadioButton();
            rdoWindows = new Sunny.UI.UIRadioButton();
            uiLabel4 = new Sunny.UI.UILabel();
            txtDutyUser = new Sunny.UI.UITextBox();
            uiLabel5 = new Sunny.UI.UILabel();
            cboGroup = new Sunny.UI.UIComboTreeView();
            dgvUsers = new Sunny.UI.UIDataGridView();
            UserName = new DataGridViewTextBoxColumn();
            Password = new DataGridViewTextBoxColumn();
            Allow = new DataGridViewCheckBoxColumn();
            btnSave = new Sunny.UI.UIButton();
            btnSaveClose = new Sunny.UI.UIButton();
            btnClose = new Sunny.UI.UIButton();
            cbStatus = new Sunny.UI.UICheckBox();
            txtRemark = new Sunny.UI.UITextBox();
            uiLabel6 = new Sunny.UI.UILabel();
            picIcon = new PictureBox();
            uiCheckBox = new Sunny.UI.UICheckBox();
            ((System.ComponentModel.ISupportInitialize)dgvUsers).BeginInit();
            ((System.ComponentModel.ISupportInitialize)picIcon).BeginInit();
            SuspendLayout();
            // 
            // uiLabel1
            // 
            uiLabel1.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiLabel1.Location = new Point(48, 155);
            uiLabel1.Name = "uiLabel1";
            uiLabel1.Size = new Size(68, 29);
            uiLabel1.Style = Sunny.UI.UIStyle.Colorful;
            uiLabel1.TabIndex = 0;
            uiLabel1.Text = "IP *";
            uiLabel1.TextAlign = ContentAlignment.MiddleRight;
            // 
            // uiLabel2
            // 
            uiLabel2.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiLabel2.Location = new Point(47, 272);
            uiLabel2.Name = "uiLabel2";
            uiLabel2.Size = new Size(68, 29);
            uiLabel2.Style = Sunny.UI.UIStyle.Colorful;
            uiLabel2.TabIndex = 0;
            uiLabel2.Text = "名称";
            uiLabel2.TextAlign = ContentAlignment.MiddleRight;
            // 
            // uiLabel3
            // 
            uiLabel3.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiLabel3.Location = new Point(48, 208);
            uiLabel3.Name = "uiLabel3";
            uiLabel3.Size = new Size(68, 29);
            uiLabel3.Style = Sunny.UI.UIStyle.Colorful;
            uiLabel3.TabIndex = 0;
            uiLabel3.Text = "Port*";
            uiLabel3.TextAlign = ContentAlignment.MiddleRight;
            // 
            // txtPort
            // 
            txtPort.ButtonFillColor = Color.FromArgb(0, 190, 172);
            txtPort.ButtonFillHoverColor = Color.FromArgb(51, 203, 189);
            txtPort.ButtonFillPressColor = Color.FromArgb(0, 152, 138);
            txtPort.ButtonRectColor = Color.FromArgb(0, 190, 172);
            txtPort.ButtonRectHoverColor = Color.FromArgb(51, 203, 189);
            txtPort.ButtonRectPressColor = Color.FromArgb(0, 152, 138);
            txtPort.DoubleValue = 22D;
            txtPort.FillColor2 = Color.FromArgb(238, 251, 250);
            txtPort.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtPort.IntValue = 22;
            txtPort.Location = new Point(122, 208);
            txtPort.Margin = new Padding(4, 5, 4, 5);
            txtPort.MinimumSize = new Size(1, 16);
            txtPort.Name = "txtPort";
            txtPort.Padding = new Padding(5);
            txtPort.RectColor = Color.FromArgb(0, 190, 172);
            txtPort.ScrollBarColor = Color.FromArgb(0, 190, 172);
            txtPort.ShowText = false;
            txtPort.Size = new Size(335, 36);
            txtPort.Style = Sunny.UI.UIStyle.Colorful;
            txtPort.TabIndex = 6;
            txtPort.Text = "22";
            txtPort.TextAlignment = ContentAlignment.MiddleLeft;
            txtPort.Type = Sunny.UI.UITextBox.UIEditType.Integer;
            txtPort.Watermark = "";
            // 
            // txtName
            // 
            txtName.ButtonFillColor = Color.FromArgb(0, 190, 172);
            txtName.ButtonFillHoverColor = Color.FromArgb(51, 203, 189);
            txtName.ButtonFillPressColor = Color.FromArgb(0, 152, 138);
            txtName.ButtonRectColor = Color.FromArgb(0, 190, 172);
            txtName.ButtonRectHoverColor = Color.FromArgb(51, 203, 189);
            txtName.ButtonRectPressColor = Color.FromArgb(0, 152, 138);
            txtName.FillColor2 = Color.FromArgb(238, 251, 250);
            txtName.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtName.Location = new Point(122, 266);
            txtName.Margin = new Padding(4, 5, 4, 5);
            txtName.MinimumSize = new Size(1, 16);
            txtName.Name = "txtName";
            txtName.Padding = new Padding(5);
            txtName.RectColor = Color.FromArgb(0, 190, 172);
            txtName.ScrollBarColor = Color.FromArgb(0, 190, 172);
            txtName.ShowText = false;
            txtName.Size = new Size(335, 36);
            txtName.Style = Sunny.UI.UIStyle.Colorful;
            txtName.TabIndex = 3;
            txtName.TextAlignment = ContentAlignment.MiddleLeft;
            txtName.Watermark = "";
            // 
            // txtIP
            // 
            txtIP.ButtonFillColor = Color.FromArgb(0, 190, 172);
            txtIP.ButtonFillHoverColor = Color.FromArgb(51, 203, 189);
            txtIP.ButtonFillPressColor = Color.FromArgb(0, 152, 138);
            txtIP.ButtonRectColor = Color.FromArgb(0, 190, 172);
            txtIP.ButtonRectHoverColor = Color.FromArgb(51, 203, 189);
            txtIP.ButtonRectPressColor = Color.FromArgb(0, 152, 138);
            txtIP.FillColor2 = Color.FromArgb(238, 251, 250);
            txtIP.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtIP.Location = new Point(122, 149);
            txtIP.Margin = new Padding(4, 5, 4, 5);
            txtIP.MinimumSize = new Size(1, 16);
            txtIP.Name = "txtIP";
            txtIP.Padding = new Padding(5);
            txtIP.RectColor = Color.FromArgb(0, 190, 172);
            txtIP.ScrollBarColor = Color.FromArgb(0, 190, 172);
            txtIP.ShowText = false;
            txtIP.Size = new Size(293, 36);
            txtIP.Style = Sunny.UI.UIStyle.Colorful;
            txtIP.TabIndex = 2;
            txtIP.TextAlignment = ContentAlignment.MiddleLeft;
            txtIP.Watermark = "";
            // 
            // rdoSSH
            // 
            rdoSSH.Checked = true;
            rdoSSH.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            rdoSSH.Location = new Point(125, 95);
            rdoSSH.MinimumSize = new Size(1, 1);
            rdoSSH.Name = "rdoSSH";
            rdoSSH.RadioButtonColor = Color.FromArgb(0, 190, 172);
            rdoSSH.Size = new Size(90, 36);
            rdoSSH.Style = Sunny.UI.UIStyle.Colorful;
            rdoSSH.TabIndex = 99;
            rdoSSH.TagString = "";
            rdoSSH.Text = "SSH";
            rdoSSH.CheckedChanged += rdoSSH_CheckedChanged;
            // 
            // rdoWindows
            // 
            rdoWindows.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            rdoWindows.Location = new Point(231, 95);
            rdoWindows.MinimumSize = new Size(1, 1);
            rdoWindows.Name = "rdoWindows";
            rdoWindows.RadioButtonColor = Color.FromArgb(0, 190, 172);
            rdoWindows.Size = new Size(103, 36);
            rdoWindows.Style = Sunny.UI.UIStyle.Colorful;
            rdoWindows.TabIndex = 99;
            rdoWindows.TagString = "";
            rdoWindows.Text = "Windows";
            rdoWindows.CheckedChanged += rdoWindows_CheckedChanged;
            // 
            // uiLabel4
            // 
            uiLabel4.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiLabel4.Location = new Point(24, 330);
            uiLabel4.Name = "uiLabel4";
            uiLabel4.Size = new Size(92, 29);
            uiLabel4.Style = Sunny.UI.UIStyle.Colorful;
            uiLabel4.TabIndex = 0;
            uiLabel4.Text = "责任人";
            uiLabel4.TextAlign = ContentAlignment.MiddleRight;
            // 
            // txtDutyUser
            // 
            txtDutyUser.ButtonFillColor = Color.FromArgb(0, 190, 172);
            txtDutyUser.ButtonFillHoverColor = Color.FromArgb(51, 203, 189);
            txtDutyUser.ButtonFillPressColor = Color.FromArgb(0, 152, 138);
            txtDutyUser.ButtonRectColor = Color.FromArgb(0, 190, 172);
            txtDutyUser.ButtonRectHoverColor = Color.FromArgb(51, 203, 189);
            txtDutyUser.ButtonRectPressColor = Color.FromArgb(0, 152, 138);
            txtDutyUser.FillColor2 = Color.FromArgb(238, 251, 250);
            txtDutyUser.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtDutyUser.Location = new Point(123, 328);
            txtDutyUser.Margin = new Padding(4, 5, 4, 5);
            txtDutyUser.MinimumSize = new Size(1, 16);
            txtDutyUser.Name = "txtDutyUser";
            txtDutyUser.Padding = new Padding(5);
            txtDutyUser.RectColor = Color.FromArgb(0, 190, 172);
            txtDutyUser.ScrollBarColor = Color.FromArgb(0, 190, 172);
            txtDutyUser.ShowText = false;
            txtDutyUser.Size = new Size(335, 36);
            txtDutyUser.Style = Sunny.UI.UIStyle.Colorful;
            txtDutyUser.TabIndex = 7;
            txtDutyUser.TextAlignment = ContentAlignment.MiddleLeft;
            txtDutyUser.Watermark = "";
            // 
            // uiLabel5
            // 
            uiLabel5.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiLabel5.Location = new Point(30, 51);
            uiLabel5.Name = "uiLabel5";
            uiLabel5.Size = new Size(86, 29);
            uiLabel5.Style = Sunny.UI.UIStyle.Colorful;
            uiLabel5.TabIndex = 4;
            uiLabel5.Text = "所属组*";
            uiLabel5.TextAlign = ContentAlignment.MiddleRight;
            // 
            // cboGroup
            // 
            cboGroup.CanSelectRootNode = true;
            cboGroup.DropDownStyle = Sunny.UI.UIDropDownStyle.DropDownList;
            cboGroup.FillColor = Color.White;
            cboGroup.FillColor2 = Color.FromArgb(238, 251, 250);
            cboGroup.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            cboGroup.Location = new Point(124, 51);
            cboGroup.Margin = new Padding(4, 5, 4, 5);
            cboGroup.MinimumSize = new Size(63, 0);
            cboGroup.Name = "cboGroup";
            cboGroup.Padding = new Padding(0, 0, 30, 2);
            cboGroup.RectColor = Color.FromArgb(0, 190, 172);
            cboGroup.Size = new Size(334, 36);
            cboGroup.Style = Sunny.UI.UIStyle.Colorful;
            cboGroup.TabIndex = 1;
            cboGroup.TextAlignment = ContentAlignment.MiddleLeft;
            cboGroup.Watermark = "";
            cboGroup.NodeSelected += cboGroup_NodeSelected;
            // 
            // dgvUsers
            // 
            dgvUsers.AllowUserToResizeColumns = false;
            dgvUsers.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.BackColor = Color.FromArgb(238, 251, 250);
            dgvUsers.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            dgvUsers.BackgroundColor = Color.FromArgb(238, 251, 250);
            dgvUsers.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle8.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = Color.FromArgb(0, 190, 172);
            dataGridViewCellStyle8.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle8.ForeColor = Color.White;
            dataGridViewCellStyle8.SelectionBackColor = Color.FromArgb(0, 190, 172);
            dataGridViewCellStyle8.SelectionForeColor = SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = DataGridViewTriState.True;
            dgvUsers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            dgvUsers.ColumnHeadersHeight = 32;
            dgvUsers.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dgvUsers.Columns.AddRange(new DataGridViewColumn[] { UserName, Password, Allow });
            dataGridViewCellStyle10.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = Color.White;
            dataGridViewCellStyle10.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle10.ForeColor = Color.FromArgb(48, 48, 48);
            dataGridViewCellStyle10.SelectionBackColor = Color.FromArgb(204, 242, 238);
            dataGridViewCellStyle10.SelectionForeColor = Color.FromArgb(48, 48, 48);
            dataGridViewCellStyle10.WrapMode = DataGridViewTriState.False;
            dgvUsers.DefaultCellStyle = dataGridViewCellStyle10;
            dgvUsers.EditMode = DataGridViewEditMode.EditOnEnter;
            dgvUsers.EnableHeadersVisualStyles = false;
            dgvUsers.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            dgvUsers.GridColor = Color.FromArgb(34, 199, 183);
            dgvUsers.Location = new Point(482, 51);
            dgvUsers.Name = "dgvUsers";
            dgvUsers.RectColor = Color.FromArgb(0, 190, 172);
            dataGridViewCellStyle11.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = Color.FromArgb(238, 251, 250);
            dataGridViewCellStyle11.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle11.ForeColor = Color.FromArgb(48, 48, 48);
            dataGridViewCellStyle11.SelectionBackColor = Color.FromArgb(0, 190, 172);
            dataGridViewCellStyle11.SelectionForeColor = Color.FromArgb(48, 48, 48);
            dataGridViewCellStyle11.WrapMode = DataGridViewTriState.True;
            dgvUsers.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            dgvUsers.RowHeadersVisible = false;
            dgvUsers.RowHeadersWidth = 51;
            dataGridViewCellStyle12.BackColor = Color.White;
            dataGridViewCellStyle12.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            dataGridViewCellStyle12.ForeColor = Color.FromArgb(48, 48, 48);
            dataGridViewCellStyle12.SelectionBackColor = Color.FromArgb(204, 242, 238);
            dataGridViewCellStyle12.SelectionForeColor = Color.FromArgb(48, 48, 48);
            dgvUsers.RowsDefaultCellStyle = dataGridViewCellStyle12;
            dgvUsers.RowTemplate.Height = 29;
            dgvUsers.ScrollBarBackColor = Color.FromArgb(238, 251, 250);
            dgvUsers.ScrollBarColor = Color.FromArgb(0, 190, 172);
            dgvUsers.ScrollBarRectColor = Color.FromArgb(0, 190, 172);
            dgvUsers.ScrollBars = ScrollBars.None;
            dgvUsers.SelectedIndex = -1;
            dgvUsers.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgvUsers.Size = new Size(512, 308);
            dgvUsers.StripeOddColor = Color.FromArgb(238, 251, 250);
            dgvUsers.Style = Sunny.UI.UIStyle.Colorful;
            dgvUsers.TabIndex = 8;
            // 
            // UserName
            // 
            UserName.DataPropertyName = "UserName";
            UserName.HeaderText = "用户名";
            UserName.MinimumWidth = 6;
            UserName.Name = "UserName";
            UserName.Width = 150;
            // 
            // Password
            // 
            Password.DataPropertyName = "Password";
            dataGridViewCellStyle9.Format = "*";
            Password.DefaultCellStyle = dataGridViewCellStyle9;
            Password.HeaderText = "密码";
            Password.MinimumWidth = 6;
            Password.Name = "Password";
            Password.Width = 260;
            // 
            // Allow
            // 
            Allow.DataPropertyName = "Allow";
            Allow.HeaderText = "允许授权";
            Allow.MinimumWidth = 6;
            Allow.Name = "Allow";
            Allow.Resizable = DataGridViewTriState.True;
            Allow.SortMode = DataGridViewColumnSortMode.Automatic;
            Allow.Width = 110;
            // 
            // btnSave
            // 
            btnSave.FillColor = Color.FromArgb(0, 190, 172);
            btnSave.FillColor2 = Color.FromArgb(0, 190, 172);
            btnSave.FillHoverColor = Color.FromArgb(51, 203, 189);
            btnSave.FillPressColor = Color.FromArgb(0, 152, 138);
            btnSave.FillSelectedColor = Color.FromArgb(0, 152, 138);
            btnSave.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnSave.Location = new Point(591, 381);
            btnSave.MinimumSize = new Size(1, 1);
            btnSave.Name = "btnSave";
            btnSave.RectColor = Color.FromArgb(0, 190, 172);
            btnSave.RectHoverColor = Color.FromArgb(51, 203, 189);
            btnSave.RectPressColor = Color.FromArgb(0, 152, 138);
            btnSave.RectSelectedColor = Color.FromArgb(0, 152, 138);
            btnSave.Size = new Size(125, 44);
            btnSave.Style = Sunny.UI.UIStyle.Colorful;
            btnSave.TabIndex = 9;
            btnSave.Text = "保存";
            btnSave.Click += btnSave_Click;
            // 
            // btnSaveClose
            // 
            btnSaveClose.FillColor = Color.FromArgb(0, 190, 172);
            btnSaveClose.FillColor2 = Color.FromArgb(0, 190, 172);
            btnSaveClose.FillHoverColor = Color.FromArgb(51, 203, 189);
            btnSaveClose.FillPressColor = Color.FromArgb(0, 152, 138);
            btnSaveClose.FillSelectedColor = Color.FromArgb(0, 152, 138);
            btnSaveClose.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnSaveClose.Location = new Point(722, 381);
            btnSaveClose.MinimumSize = new Size(1, 1);
            btnSaveClose.Name = "btnSaveClose";
            btnSaveClose.RectColor = Color.FromArgb(0, 190, 172);
            btnSaveClose.RectHoverColor = Color.FromArgb(51, 203, 189);
            btnSaveClose.RectPressColor = Color.FromArgb(0, 152, 138);
            btnSaveClose.RectSelectedColor = Color.FromArgb(0, 152, 138);
            btnSaveClose.Size = new Size(142, 44);
            btnSaveClose.Style = Sunny.UI.UIStyle.Colorful;
            btnSaveClose.TabIndex = 10;
            btnSaveClose.Text = "保存并关闭";
            btnSaveClose.TipsFont = new Font("宋体", 9F, FontStyle.Regular, GraphicsUnit.Point);
            btnSaveClose.Click += btnSaveClose_Click;
            // 
            // btnClose
            // 
            btnClose.FillColor = Color.FromArgb(0, 190, 172);
            btnClose.FillColor2 = Color.FromArgb(0, 190, 172);
            btnClose.FillHoverColor = Color.FromArgb(51, 203, 189);
            btnClose.FillPressColor = Color.FromArgb(0, 152, 138);
            btnClose.FillSelectedColor = Color.FromArgb(0, 152, 138);
            btnClose.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            btnClose.Location = new Point(870, 381);
            btnClose.MinimumSize = new Size(1, 1);
            btnClose.Name = "btnClose";
            btnClose.RectColor = Color.FromArgb(0, 190, 172);
            btnClose.RectHoverColor = Color.FromArgb(51, 203, 189);
            btnClose.RectPressColor = Color.FromArgb(0, 152, 138);
            btnClose.RectSelectedColor = Color.FromArgb(0, 152, 138);
            btnClose.Size = new Size(125, 44);
            btnClose.Style = Sunny.UI.UIStyle.Colorful;
            btnClose.TabIndex = 11;
            btnClose.Text = "关闭";
            btnClose.TipsFont = new Font("宋体", 9F, FontStyle.Regular, GraphicsUnit.Point);
            btnClose.Click += btnClose_Click;
            // 
            // cbStatus
            // 
            cbStatus.CheckBoxColor = Color.FromArgb(0, 190, 172);
            cbStatus.Checked = true;
            cbStatus.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            cbStatus.Location = new Point(376, 95);
            cbStatus.MinimumSize = new Size(1, 1);
            cbStatus.Name = "cbStatus";
            cbStatus.Size = new Size(87, 36);
            cbStatus.Style = Sunny.UI.UIStyle.Colorful;
            cbStatus.TabIndex = 100;
            cbStatus.Text = "可见";
            // 
            // txtRemark
            // 
            txtRemark.ButtonFillColor = Color.FromArgb(0, 190, 172);
            txtRemark.ButtonFillHoverColor = Color.FromArgb(51, 203, 189);
            txtRemark.ButtonFillPressColor = Color.FromArgb(0, 152, 138);
            txtRemark.ButtonRectColor = Color.FromArgb(0, 190, 172);
            txtRemark.ButtonRectHoverColor = Color.FromArgb(51, 203, 189);
            txtRemark.ButtonRectPressColor = Color.FromArgb(0, 152, 138);
            txtRemark.FillColor2 = Color.FromArgb(238, 251, 250);
            txtRemark.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtRemark.Location = new Point(122, 389);
            txtRemark.Margin = new Padding(4, 5, 4, 5);
            txtRemark.MinimumSize = new Size(1, 16);
            txtRemark.Name = "txtRemark";
            txtRemark.Padding = new Padding(5);
            txtRemark.RectColor = Color.FromArgb(0, 190, 172);
            txtRemark.ScrollBarColor = Color.FromArgb(0, 190, 172);
            txtRemark.ShowText = false;
            txtRemark.Size = new Size(338, 36);
            txtRemark.Style = Sunny.UI.UIStyle.Colorful;
            txtRemark.TabIndex = 101;
            txtRemark.TextAlignment = ContentAlignment.MiddleLeft;
            txtRemark.Watermark = "";
            // 
            // uiLabel6
            // 
            uiLabel6.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiLabel6.Location = new Point(23, 389);
            uiLabel6.Name = "uiLabel6";
            uiLabel6.Size = new Size(92, 29);
            uiLabel6.Style = Sunny.UI.UIStyle.Colorful;
            uiLabel6.TabIndex = 0;
            uiLabel6.Text = "备注";
            uiLabel6.TextAlign = ContentAlignment.MiddleRight;
            // 
            // picIcon
            // 
            picIcon.Cursor = Cursors.Hand;
            picIcon.Image = Properties.Resources.na;
            picIcon.Location = new Point(433, 155);
            picIcon.Name = "picIcon";
            picIcon.Size = new Size(24, 24);
            picIcon.SizeMode = PictureBoxSizeMode.Zoom;
            picIcon.TabIndex = 102;
            picIcon.TabStop = false;
            picIcon.Click += picIcon_Click;
            // 
            // uiCheckBox
            // 
            uiCheckBox.CheckBoxColor = Color.FromArgb(0, 190, 172);
            uiCheckBox.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiCheckBox.Location = new Point(482, 389);
            uiCheckBox.MinimumSize = new Size(1, 1);
            uiCheckBox.Name = "uiCheckBox";
            uiCheckBox.Size = new Size(86, 36);
            uiCheckBox.Style = Sunny.UI.UIStyle.Colorful;
            uiCheckBox.TabIndex = 103;
            uiCheckBox.Text = "刷新";
            // 
            // FrmEditServer
            // 
            AutoScaleMode = AutoScaleMode.None;
            ClientSize = new Size(1014, 454);
            ControlBoxFillHoverColor = Color.FromArgb(51, 203, 189);
            Controls.Add(uiCheckBox);
            Controls.Add(picIcon);
            Controls.Add(txtRemark);
            Controls.Add(cbStatus);
            Controls.Add(btnClose);
            Controls.Add(btnSaveClose);
            Controls.Add(btnSave);
            Controls.Add(dgvUsers);
            Controls.Add(cboGroup);
            Controls.Add(uiLabel5);
            Controls.Add(rdoWindows);
            Controls.Add(rdoSSH);
            Controls.Add(txtIP);
            Controls.Add(txtDutyUser);
            Controls.Add(txtName);
            Controls.Add(txtPort);
            Controls.Add(uiLabel6);
            Controls.Add(uiLabel4);
            Controls.Add(uiLabel3);
            Controls.Add(uiLabel2);
            Controls.Add(uiLabel1);
            Name = "FrmEditServer";
            RectColor = Color.FromArgb(0, 190, 172);
            Style = Sunny.UI.UIStyle.Colorful;
            Text = "";
            TitleColor = Color.FromArgb(0, 190, 172);
            ZoomScaleRect = new Rectangle(19, 19, 800, 450);
            Load += FrmEditServer_Load;
            ((System.ComponentModel.ISupportInitialize)dgvUsers).EndInit();
            ((System.ComponentModel.ISupportInitialize)picIcon).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UILabel uiLabel2;
        private Sunny.UI.UILabel uiLabel3;
        private Sunny.UI.UITextBox txtPort;
        private Sunny.UI.UITextBox txtName;
        private Sunny.UI.UITextBox txtIP;
        private Sunny.UI.UIRadioButton rdoSSH;
        private Sunny.UI.UIRadioButton rdoWindows;
        private Sunny.UI.UILabel uiLabel4;
        private Sunny.UI.UITextBox txtDutyUser;
        private Sunny.UI.UILabel uiLabel5;
        private Sunny.UI.UIComboTreeView cboGroup;
        private Sunny.UI.UIDataGridView dgvUsers;
        private Sunny.UI.UIButton btnSave;
        private Sunny.UI.UIButton btnSaveClose;
        private Sunny.UI.UIButton btnClose;
        private Sunny.UI.UICheckBox cbStatus;
        private Sunny.UI.UITextBox txtRemark;
        private Sunny.UI.UILabel uiLabel6;
        private DataGridViewTextBoxColumn UserName;
        private DataGridViewTextBoxColumn Password;
        private DataGridViewCheckBoxColumn Allow;
        private PictureBox picIcon;
        private Sunny.UI.UICheckBox uiCheckBox;
    }
}