using Sunny.UI;
using System.IO.Pipes;

namespace WeShell
{
    internal static class Program
    {
        static Mutex _mutex = new Mutex(true, Glb.MutexName);

        [STAThread]
        private static void Main()
        {
            if (_mutex.WaitOne(TimeSpan.Zero, true))
            {
                ApplicationConfiguration.Initialize();
                Rebex.Licensing.Key = KeyUtils.CreateTrilKey();
                Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);//处理处理未捕获的异常
                Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);  //处理UI线程异常
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);//处理非UI线程异常

                var login = new FrmLogin();
                var dialog = login.ShowDialog();

                if (dialog == DialogResult.OK)
                {
                    Application.Run(new FrmMain(login.UserLoginResponse));
                }
                _mutex.ReleaseMutex();
            }
            else
            {
                Application.Exit();
            }
        }

        private static void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            var ex = e.Exception;
            ShowErrorNotifier($"Error:{ex.Message}");
        }

        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var ex = (System.Exception)e.ExceptionObject;
            ShowErrorNotifier($"Error:{ex.Message}");
        }

        private static void ShowErrorNotifier(string desc, bool isDialog = false, int timeout = 2000)
        {
            UINotifierHelper.ShowNotifier(desc, UINotifierType.ERROR, UILocalize.ErrorTitle, isDialog, timeout);
        }
    }
}