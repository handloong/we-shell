﻿namespace WeShell
{
    partial class FrmIcon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            flowLayoutPanel = new FlowLayoutPanel();
            SuspendLayout();
            // 
            // flowLayoutPanel
            // 
            flowLayoutPanel.Dock = DockStyle.Fill;
            flowLayoutPanel.Location = new Point(0, 35);
            flowLayoutPanel.Name = "flowLayoutPanel";
            flowLayoutPanel.Size = new Size(664, 353);
            flowLayoutPanel.TabIndex = 0;
            // 
            // FrmIcon
            // 
            AutoScaleMode = AutoScaleMode.None;
            ClientSize = new Size(664, 388);
            ControlBoxFillHoverColor = Color.FromArgb(70, 70, 70);
            Controls.Add(flowLayoutPanel);
            EscClose = true;
            ForeColor = Color.White;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FrmIcon";
            RectColor = Color.FromArgb(18, 58, 92);
            ShowIcon = false;
            Style = Sunny.UI.UIStyle.Black;
            Text = "Icon选择器,ESC退出";
            TitleColor = Color.FromArgb(21, 21, 21);
            ZoomScaleRect = new Rectangle(19, 19, 800, 450);
            Load += FrmIcon_Load;
            ResumeLayout(false);
        }

        #endregion

        private FlowLayoutPanel flowLayoutPanel;
    }
}