﻿using Sunny.UI;
using WeShell.Properties;
using WeShellCore;

namespace WeShell
{
    public partial class FrmEditGroup : UIForm
    {
        private readonly Action<GroupInput> _action;    //点击OK组装的input           
        private readonly List<GroupResponse> _groupResponses;  // 所有上级
        private readonly GroupResponse _group;//编辑的对象
        private List<ResourcesDto> _resources;//icon资源
        private bool _add;//是否是添加

        public FrmEditGroup(Action<GroupInput> action, List<GroupResponse> groupResponses, GroupResponse group = null)
        {
            InitializeComponent();
            this._action = action;
            _groupResponses = groupResponses;
            _group = group;
            _resources = ResourcesHelper.GetImageList();
            _add = string.IsNullOrEmpty(_group?.Id);

            //添加父级选项卡 Begin
            var topLevelNode = new TreeNode("----顶级----");
            topLevelNode.Tag = new GroupResponse { Id = "0" };
            tvParentId.Nodes.Add(topLevelNode);
            var selectedTreeNode = TreeUtils.PopulateTreeViewParentId(_groupResponses, tvParentId.Nodes, _group?.ParentId);
            tvParentId.SelectedNode = selectedTreeNode ?? topLevelNode;
            //添加父级选项卡 End

            ResourcesDto setResource = null;
            if (!_add)
            {
                setResource = _resources.FirstOrDefault(x => x.Name.Equals(_group.Icon, StringComparison.OrdinalIgnoreCase));
                setResource ??= _resources.FirstOrDefault(x => x.Name.Equals("folder", StringComparison.OrdinalIgnoreCase));

                txtGroupName.Text = _group.Name;
                txtGroupName.SelectAll();
            }
            else
            {
                //新增
                setResource = _resources.FirstOrDefault(x => x.Name.Equals("folder", StringComparison.OrdinalIgnoreCase));
            }

            picIcon.Image = setResource?.Bitmap;
            picIcon.Tag = setResource?.Name;



            UIStyles.InitColorful(EasyConf.Read<Config>().GetColorFul(), Color.White);
        }

        private void txtGroupName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                var edit = txtGroupName.Text.Trim();
                if (string.IsNullOrEmpty(edit))
                {
                    ShowErrorTip("不能为空");
                }
                else
                {
                    var pid = (tvParentId.SelectedNode.Tag as GroupResponse).Id;

                    if (!_add && _group.Id == pid)
                    {
                        var smd = UIMessageDialog.ShowMessageDialog($"该组的上级等于自身,将提升此组层级为顶级,是否继续?", "温馨提示", true, Style);
                        if (smd)
                            pid = "0";
                        else
                            return;
                    }

                    _action.Invoke(new GroupInput
                    {
                        Name = edit,
                        Icon = Convert.ToString(picIcon.Tag),
                        ParentId = pid
                    });
                    Close();
                }
            }
        }

        //protected override bool ProcessCmdKey(ref System.Windows.Forms.Message msg, System.Windows.Forms.Keys keyData)
        //{
        //    int WM_KEYDOWN = 256;
        //    int WM_SYSKEYDOWN = 260;
        //    if (msg.Msg == WM_KEYDOWN | msg.Msg == WM_SYSKEYDOWN)
        //    {
        //        switch (keyData)
        //        {
        //            case Keys.Escape:
        //                this.Close();
        //                break;
        //        }
        //    }
        //    return false;
        //}

        private void picIcon_Click(object sender, EventArgs e)
        {
            new FrmIcon(_resources, (n, i) =>
            {
                picIcon.Image = i;
                picIcon.Tag = n;
            }).ShowDialog();
        }

        private async void tvParentId_NodeSelected(object sender, TreeNode node)
        {
            await Task.Delay(100);
            txtGroupName.Focus();
        }
    }
}