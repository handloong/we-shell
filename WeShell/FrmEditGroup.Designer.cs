﻿namespace WeShell
{
    partial class FrmEditGroup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            txtGroupName = new Sunny.UI.UITextBox();
            picIcon = new PictureBox();
            tvParentId = new Sunny.UI.UIComboTreeView();
            uiLabel1 = new Sunny.UI.UILabel();
            uiLabel2 = new Sunny.UI.UILabel();
            ((System.ComponentModel.ISupportInitialize)picIcon).BeginInit();
            SuspendLayout();
            // 
            // txtGroupName
            // 
            txtGroupName.ButtonFillColor = Color.FromArgb(0, 190, 172);
            txtGroupName.ButtonFillHoverColor = Color.FromArgb(51, 203, 189);
            txtGroupName.ButtonFillPressColor = Color.FromArgb(0, 152, 138);
            txtGroupName.ButtonRectColor = Color.FromArgb(0, 190, 172);
            txtGroupName.ButtonRectHoverColor = Color.FromArgb(51, 203, 189);
            txtGroupName.ButtonRectPressColor = Color.FromArgb(0, 152, 138);
            txtGroupName.FillColor2 = Color.FromArgb(238, 251, 250);
            txtGroupName.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtGroupName.Location = new Point(85, 122);
            txtGroupName.Margin = new Padding(4, 5, 4, 5);
            txtGroupName.MinimumSize = new Size(1, 16);
            txtGroupName.Name = "txtGroupName";
            txtGroupName.Padding = new Padding(5);
            txtGroupName.RectColor = Color.FromArgb(0, 190, 172);
            txtGroupName.ScrollBarColor = Color.FromArgb(0, 190, 172);
            txtGroupName.ShowText = false;
            txtGroupName.Size = new Size(251, 36);
            txtGroupName.Style = Sunny.UI.UIStyle.Colorful;
            txtGroupName.TabIndex = 1;
            txtGroupName.TextAlignment = ContentAlignment.MiddleLeft;
            txtGroupName.Watermark = "";
            txtGroupName.KeyDown += txtGroupName_KeyDown;
            // 
            // picIcon
            // 
            picIcon.Cursor = Cursors.Hand;
            picIcon.Image = Properties.Resources.na;
            picIcon.Location = new Point(357, 134);
            picIcon.Name = "picIcon";
            picIcon.Size = new Size(24, 24);
            picIcon.SizeMode = PictureBoxSizeMode.Zoom;
            picIcon.TabIndex = 103;
            picIcon.TabStop = false;
            picIcon.Click += picIcon_Click;
            // 
            // tvParentId
            // 
            tvParentId.CanSelectRootNode = true;
            tvParentId.DropDownStyle = Sunny.UI.UIDropDownStyle.DropDownList;
            tvParentId.FillColor = Color.White;
            tvParentId.FillColor2 = Color.FromArgb(238, 251, 250);
            tvParentId.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            tvParentId.Location = new Point(85, 56);
            tvParentId.Margin = new Padding(4, 5, 4, 5);
            tvParentId.MinimumSize = new Size(63, 0);
            tvParentId.Name = "tvParentId";
            tvParentId.Padding = new Padding(0, 0, 30, 2);
            tvParentId.RectColor = Color.FromArgb(0, 190, 172);
            tvParentId.Size = new Size(251, 36);
            tvParentId.Style = Sunny.UI.UIStyle.Colorful;
            tvParentId.TabIndex = 104;
            tvParentId.TextAlignment = ContentAlignment.MiddleLeft;
            tvParentId.Watermark = "";
            tvParentId.NodeSelected += tvParentId_NodeSelected;
            // 
            // uiLabel1
            // 
            uiLabel1.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiLabel1.Location = new Point(23, 60);
            uiLabel1.Name = "uiLabel1";
            uiLabel1.Size = new Size(57, 29);
            uiLabel1.Style = Sunny.UI.UIStyle.Colorful;
            uiLabel1.TabIndex = 105;
            uiLabel1.Text = "上级";
            uiLabel1.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // uiLabel2
            // 
            uiLabel2.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiLabel2.Location = new Point(21, 125);
            uiLabel2.Name = "uiLabel2";
            uiLabel2.Size = new Size(57, 29);
            uiLabel2.Style = Sunny.UI.UIStyle.Colorful;
            uiLabel2.TabIndex = 105;
            uiLabel2.Text = "名称";
            uiLabel2.TextAlign = ContentAlignment.MiddleLeft;
            // 
            // FrmEditGroup
            // 
            AutoScaleMode = AutoScaleMode.None;
            ClientSize = new Size(401, 204);
            ControlBoxFillHoverColor = Color.FromArgb(51, 203, 189);
            Controls.Add(uiLabel2);
            Controls.Add(uiLabel1);
            Controls.Add(tvParentId);
            Controls.Add(picIcon);
            Controls.Add(txtGroupName);
            EscClose = true;
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FrmEditGroup";
            RectColor = Color.FromArgb(0, 190, 172);
            Style = Sunny.UI.UIStyle.Colorful;
            Text = "Enter确认,ESC取消";
            TitleColor = Color.FromArgb(0, 190, 172);
            ZoomScaleRect = new Rectangle(19, 19, 800, 450);
            ((System.ComponentModel.ISupportInitialize)picIcon).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private Sunny.UI.UITextBox txtGroupName;
        private PictureBox picIcon;
        private Sunny.UI.UIComboTreeView tvParentId;
        private Sunny.UI.UILabel uiLabel1;
        private Sunny.UI.UILabel uiLabel2;
    }
}