﻿using Sunny.UI;
using System.Data;

namespace WeShell
{
    public partial class FrmIcon : UIForm
    {
        private readonly List<ResourcesDto> _resources;
        private readonly Action<string, Bitmap> _action;

        public FrmIcon(List<ResourcesDto> resources, Action<string, Bitmap> action)
        {
            InitializeComponent();

            this.DoubleBuffered = true;
            SetStyle(ControlStyles.ResizeRedraw, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);

            _resources = resources;
            _action = action;

            //此页面默认黑色,不做全局皮肤处理
            //UIStyles.InitColorful(EasyConf.Read<Config>().GetColorFul(), Color.White);
        }

        private void FrmIcon_Load(object sender, EventArgs e)
        {
            Task.Run(() =>
            {
                foreach (ResourcesDto resource in _resources.OrderBy(x => x.Name))
                {
                    var pic = new PictureBox();
                    pic.Size = new Size(48, 48);
                    pic.Image = resource.Bitmap;
                    pic.Cursor = Cursors.Hand;
                    pic.BorderStyle = BorderStyle.FixedSingle;
                    pic.SizeMode = PictureBoxSizeMode.StretchImage;
                    pic.Click += (s, e) =>
                    {
                        _action.Invoke(resource.Name, resource.Bitmap);
                        this.Close();
                    };
                    this.BeginInvoke(() =>
                    {
                        flowLayoutPanel.Controls.Add(pic);
                    });
                }
            });
        }
    }
}
