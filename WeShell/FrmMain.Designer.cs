﻿namespace WeShell
{
    partial class FrmMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmMain));
            tabControl = new Sunny.UI.UITabControl();
            tabIndex = new TabPage();
            uiSplitContainer = new Sunny.UI.UISplitContainer();
            uiTreeView = new TreeView();
            uiContextMenuStripEdit = new Sunny.UI.UIContextMenuStrip();
            tsAddGroup = new ToolStripMenuItem();
            tsModifyGroup = new ToolStripMenuItem();
            tsDeleteGroup = new ToolStripMenuItem();
            tsAddServer = new ToolStripMenuItem();
            tsModifyServer = new ToolStripMenuItem();
            tsDeleteServer = new ToolStripMenuItem();
            tsDetail = new ToolStripMenuItem();
            tsLoadTree = new ToolStripMenuItem();
            tsUserManager = new ToolStripMenuItem();
            tsOpenNewShell = new ToolStripMenuItem();
            imageList = new ImageList(components);
            uiContextRT = new Sunny.UI.UIContextMenuStrip();
            btnColorful = new ToolStripMenuItem();
            系统设置ToolStripMenuItem = new ToolStripMenuItem();
            修改密码ToolStripMenuItem = new ToolStripMenuItem();
            uiStyleManager = new Sunny.UI.UIStyleManager(components);
            tabControl.SuspendLayout();
            uiSplitContainer.BeginInit();
            uiSplitContainer.Panel1.SuspendLayout();
            uiSplitContainer.Panel2.SuspendLayout();
            uiSplitContainer.SuspendLayout();
            uiContextMenuStripEdit.SuspendLayout();
            uiContextRT.SuspendLayout();
            SuspendLayout();
            // 
            // tabControl
            // 
            tabControl.Controls.Add(tabIndex);
            tabControl.Dock = DockStyle.Fill;
            tabControl.DrawMode = TabDrawMode.OwnerDrawFixed;
            tabControl.FillColor = Color.FromArgb(238, 251, 250);
            tabControl.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            tabControl.ItemSize = new Size(150, 40);
            tabControl.Location = new Point(0, 0);
            tabControl.MainPage = "";
            tabControl.MenuStyle = Sunny.UI.UIMenuStyle.Custom;
            tabControl.Name = "tabControl";
            tabControl.SelectedIndex = 0;
            tabControl.Size = new Size(959, 676);
            tabControl.SizeMode = TabSizeMode.Fixed;
            tabControl.Style = Sunny.UI.UIStyle.Colorful;
            tabControl.TabIndex = 3;
            tabControl.TabSelectedForeColor = Color.FromArgb(0, 190, 172);
            tabControl.TabSelectedHighColor = Color.FromArgb(0, 190, 172);
            tabControl.TabUnSelectedForeColor = Color.FromArgb(240, 240, 240);
            tabControl.BeforeRemoveTabPage += tabControl_BeforeRemoveTabPage;
            // 
            // tabIndex
            // 
            tabIndex.BackColor = Color.FromArgb(238, 251, 250);
            tabIndex.Location = new Point(0, 40);
            tabIndex.Name = "tabIndex";
            tabIndex.Size = new Size(959, 636);
            tabIndex.TabIndex = 20;
            tabIndex.Text = "首页";
            // 
            // uiSplitContainer
            // 
            uiSplitContainer.ArrowColor = Color.FromArgb(0, 190, 172);
            uiSplitContainer.Dock = DockStyle.Fill;
            uiSplitContainer.FixedPanel = FixedPanel.Panel1;
            uiSplitContainer.Location = new Point(2, 36);
            uiSplitContainer.MinimumSize = new Size(20, 20);
            uiSplitContainer.Name = "uiSplitContainer";
            // 
            // uiSplitContainer.Panel1
            // 
            uiSplitContainer.Panel1.Controls.Add(uiTreeView);
            // 
            // uiSplitContainer.Panel2
            // 
            uiSplitContainer.Panel2.Controls.Add(tabControl);
            uiSplitContainer.Size = new Size(1252, 676);
            uiSplitContainer.SplitterDistance = 283;
            uiSplitContainer.SplitterWidth = 10;
            uiSplitContainer.Style = Sunny.UI.UIStyle.Colorful;
            uiSplitContainer.TabIndex = 5;
            // 
            // uiTreeView
            // 
            uiTreeView.ContextMenuStrip = uiContextMenuStripEdit;
            uiTreeView.Dock = DockStyle.Fill;
            uiTreeView.Font = new Font("宋体", 9F, FontStyle.Regular, GraphicsUnit.Point);
            uiTreeView.Location = new Point(0, 0);
            uiTreeView.Name = "uiTreeView";
            uiTreeView.Size = new Size(283, 676);
            uiTreeView.TabIndex = 0;
            uiTreeView.BeforeExpand += uiTreeView_BeforeExpand;
            uiTreeView.MouseDoubleClick += uiTreeView_MouseDoubleClick;
            uiTreeView.MouseDown += uiTreeView_MouseDown;
            // 
            // uiContextMenuStripEdit
            // 
            uiContextMenuStripEdit.BackColor = Color.FromArgb(238, 251, 250);
            uiContextMenuStripEdit.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiContextMenuStripEdit.ForeColor = Color.FromArgb(48, 48, 48);
            uiContextMenuStripEdit.ImageScalingSize = new Size(20, 20);
            uiContextMenuStripEdit.Items.AddRange(new ToolStripItem[] { tsAddGroup, tsModifyGroup, tsDeleteGroup, tsAddServer, tsModifyServer, tsDeleteServer, tsDetail, tsLoadTree, tsUserManager, tsOpenNewShell });
            uiContextMenuStripEdit.Name = "uiContextMenuStripEdit";
            uiContextMenuStripEdit.Size = new Size(179, 244);
            uiContextMenuStripEdit.Style = Sunny.UI.UIStyle.Colorful;
            // 
            // tsAddGroup
            // 
            tsAddGroup.Name = "tsAddGroup";
            tsAddGroup.Size = new Size(178, 24);
            tsAddGroup.Text = "新增组";
            tsAddGroup.Click += tsAddGroup_Click;
            // 
            // tsModifyGroup
            // 
            tsModifyGroup.Name = "tsModifyGroup";
            tsModifyGroup.Size = new Size(178, 24);
            tsModifyGroup.Text = "修改组";
            tsModifyGroup.Click += tsModifyGroup_Click;
            // 
            // tsDeleteGroup
            // 
            tsDeleteGroup.BackColor = Color.FromArgb(255, 192, 192);
            tsDeleteGroup.Name = "tsDeleteGroup";
            tsDeleteGroup.Size = new Size(178, 24);
            tsDeleteGroup.Text = "删除组";
            tsDeleteGroup.Click += tsDeleteGroup_ClickAsync;
            // 
            // tsAddServer
            // 
            tsAddServer.Name = "tsAddServer";
            tsAddServer.Size = new Size(178, 24);
            tsAddServer.Text = "新增会话";
            tsAddServer.Click += tsAddServer_Click;
            // 
            // tsModifyServer
            // 
            tsModifyServer.Name = "tsModifyServer";
            tsModifyServer.Size = new Size(178, 24);
            tsModifyServer.Text = "编辑会话";
            tsModifyServer.Click += tsEditServer_Click;
            // 
            // tsDeleteServer
            // 
            tsDeleteServer.BackColor = Color.FromArgb(255, 192, 192);
            tsDeleteServer.Name = "tsDeleteServer";
            tsDeleteServer.Size = new Size(178, 24);
            tsDeleteServer.Text = "删除会话";
            tsDeleteServer.Click += tsDeleteServer_ClickAsync;
            // 
            // tsDetail
            // 
            tsDetail.Name = "tsDetail";
            tsDetail.Size = new Size(178, 24);
            tsDetail.Text = "详情";
            tsDetail.Click += tsDetail_Click;
            // 
            // tsLoadTree
            // 
            tsLoadTree.Name = "tsLoadTree";
            tsLoadTree.Size = new Size(178, 24);
            tsLoadTree.Text = "刷新";
            tsLoadTree.Click += tsLoadTree_Click;
            // 
            // tsUserManager
            // 
            tsUserManager.Name = "tsUserManager";
            tsUserManager.Size = new Size(178, 24);
            tsUserManager.Text = "用户管理";
            tsUserManager.Click += tsUserManager_Click;
            // 
            // tsOpenNewShell
            // 
            tsOpenNewShell.Name = "tsOpenNewShell";
            tsOpenNewShell.Size = new Size(178, 24);
            tsOpenNewShell.Text = "打开新窗口";
            tsOpenNewShell.Click += tsOpenNewShell_Click;
            // 
            // imageList
            // 
            imageList.ColorDepth = ColorDepth.Depth16Bit;
            imageList.ImageSize = new Size(24, 24);
            imageList.TransparentColor = Color.Transparent;
            // 
            // uiContextRT
            // 
            uiContextRT.BackColor = Color.FromArgb(238, 251, 250);
            uiContextRT.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiContextRT.ForeColor = Color.FromArgb(48, 48, 48);
            uiContextRT.ImageScalingSize = new Size(20, 20);
            uiContextRT.Items.AddRange(new ToolStripItem[] { btnColorful, 系统设置ToolStripMenuItem, 修改密码ToolStripMenuItem });
            uiContextRT.Name = "uiContextRT";
            uiContextRT.Size = new Size(159, 76);
            uiContextRT.Style = Sunny.UI.UIStyle.Colorful;
            // 
            // btnColorful
            // 
            btnColorful.Name = "btnColorful";
            btnColorful.Size = new Size(158, 24);
            btnColorful.Text = "多彩主题";
            btnColorful.Click += btnColorful_Click;
            // 
            // 系统设置ToolStripMenuItem
            // 
            系统设置ToolStripMenuItem.Name = "系统设置ToolStripMenuItem";
            系统设置ToolStripMenuItem.Size = new Size(158, 24);
            系统设置ToolStripMenuItem.Text = "系统设置";
            系统设置ToolStripMenuItem.Click += tsSystemConfigMenuItem_Click;
            // 
            // 修改密码ToolStripMenuItem
            // 
            修改密码ToolStripMenuItem.Name = "修改密码ToolStripMenuItem";
            修改密码ToolStripMenuItem.Size = new Size(158, 24);
            修改密码ToolStripMenuItem.Text = "修改密码";
            修改密码ToolStripMenuItem.Click += tsChangePwdMenuItem_Click;
            // 
            // uiStyleManager
            // 
            uiStyleManager.DPIScale = true;
            uiStyleManager.GlobalFont = true;
            uiStyleManager.GlobalFontScale = 125;
            // 
            // FrmMain
            // 
            AutoScaleMode = AutoScaleMode.None;
            ClientSize = new Size(1256, 714);
            CloseAskString = "";
            ControlBoxFillHoverColor = Color.FromArgb(51, 203, 189);
            Controls.Add(uiSplitContainer);
            ExtendBox = true;
            ExtendMenu = uiContextRT;
            Icon = (Icon)resources.GetObject("$this.Icon");
            Name = "FrmMain";
            Padding = new Padding(2, 36, 2, 2);
            RectColor = Color.FromArgb(0, 190, 172);
            ShowDragStretch = true;
            ShowRadius = false;
            ShowShadow = true;
            ShowTitleIcon = true;
            Style = Sunny.UI.UIStyle.Colorful;
            Text = "敬请期待";
            TitleColor = Color.FromArgb(0, 190, 172);
            ZoomScaleRect = new Rectangle(19, 19, 705, 331);
            FormClosing += FrmMain_FormClosing;
            Load += FrmMain_Load;
            Move += FrmMain_Move;
            Resize += FrmMain_Resize;
            tabControl.ResumeLayout(false);
            uiSplitContainer.Panel1.ResumeLayout(false);
            uiSplitContainer.Panel2.ResumeLayout(false);
            uiSplitContainer.EndInit();
            uiSplitContainer.ResumeLayout(false);
            uiContextMenuStripEdit.ResumeLayout(false);
            uiContextRT.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion
        private Sunny.UI.UITabControl tabControl;
        private Sunny.UI.UISplitContainer uiSplitContainer;
        private TabPage tabIndex;
        private Sunny.UI.UIContextMenuStrip uiContextMenuStripEdit;
        private ToolStripMenuItem tsAddGroup;
        private ToolStripMenuItem tsAddServer;
        private ImageList imageList;
        private ToolStripMenuItem tsDetail;
        private ToolStripMenuItem tsModifyGroup;
        private ToolStripMenuItem tsDeleteGroup;
        private ToolStripMenuItem tsModifyServer;
        private ToolStripMenuItem tsDeleteServer;
        private ToolStripMenuItem tsLoadTree;
        private ToolStripMenuItem tsOpenNewShell;
        private ToolStripMenuItem tsUserManager;
        private Sunny.UI.UIContextMenuStrip uiContextRT;
        private ToolStripMenuItem btnColorful;
        private Sunny.UI.UIStyleManager uiStyleManager;
        private ToolStripMenuItem 系统设置ToolStripMenuItem;
        private ToolStripMenuItem 修改密码ToolStripMenuItem;
        private TreeView uiTreeView;
    }
}