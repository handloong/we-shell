﻿namespace WeShell
{
    partial class FrmUIPage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            SplitContainer = new Sunny.UI.UISplitContainer();
            statusStrip1 = new StatusStrip();
            pbTransfer = new ToolStripProgressBar();
            lblStatus = new ToolStripStatusLabel();
            uiButton1 = new Sunny.UI.UIButton();
            txtServerDir = new Sunny.UI.UITextBox();
            listServer = new ListView();
            name = new ColumnHeader();
            size = new ColumnHeader();
            date = new ColumnHeader();
            imageList = new ImageList(components);
            SplitContainer.BeginInit();
            SplitContainer.Panel1.SuspendLayout();
            SplitContainer.SuspendLayout();
            statusStrip1.SuspendLayout();
            SuspendLayout();
            // 
            // SplitContainer
            // 
            SplitContainer.Dock = DockStyle.Fill;
            SplitContainer.FixedPanel = FixedPanel.Panel1;
            SplitContainer.Location = new Point(0, 0);
            SplitContainer.MinimumSize = new Size(20, 20);
            SplitContainer.Name = "SplitContainer";
            // 
            // SplitContainer.Panel1
            // 
            SplitContainer.Panel1.Controls.Add(statusStrip1);
            SplitContainer.Panel1.Controls.Add(uiButton1);
            SplitContainer.Panel1.Controls.Add(txtServerDir);
            SplitContainer.Panel1.Controls.Add(listServer);
            // 
            // SplitContainer.Panel2
            // 
            SplitContainer.Panel2.AutoScroll = true;
            SplitContainer.Size = new Size(1356, 716);
            SplitContainer.SplitterDistance = 408;
            SplitContainer.SplitterWidth = 11;
            SplitContainer.TabIndex = 0;
            // 
            // statusStrip1
            // 
            statusStrip1.ImageScalingSize = new Size(20, 20);
            statusStrip1.Items.AddRange(new ToolStripItem[] { pbTransfer, lblStatus });
            statusStrip1.Location = new Point(0, 690);
            statusStrip1.Name = "statusStrip1";
            statusStrip1.Size = new Size(408, 26);
            statusStrip1.TabIndex = 6;
            statusStrip1.Text = "statusStrip1";
            // 
            // pbTransfer
            // 
            pbTransfer.Name = "pbTransfer";
            pbTransfer.Size = new Size(100, 18);
            // 
            // lblStatus
            // 
            lblStatus.Name = "lblStatus";
            lblStatus.Size = new Size(39, 20);
            lblStatus.Text = "就绪";
            // 
            // uiButton1
            // 
            uiButton1.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            uiButton1.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            uiButton1.Location = new Point(347, 5);
            uiButton1.MinimumSize = new Size(1, 1);
            uiButton1.Name = "uiButton1";
            uiButton1.Size = new Size(59, 37);
            uiButton1.TabIndex = 5;
            uiButton1.Text = "上传";
            uiButton1.TipsFont = new Font("宋体", 9F, FontStyle.Regular, GraphicsUnit.Point);
            uiButton1.Click += uiButton1_Click;
            // 
            // txtServerDir
            // 
            txtServerDir.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
            txtServerDir.Font = new Font("宋体", 12F, FontStyle.Regular, GraphicsUnit.Point);
            txtServerDir.Location = new Point(1, 5);
            txtServerDir.Margin = new Padding(4, 5, 4, 5);
            txtServerDir.MinimumSize = new Size(1, 16);
            txtServerDir.Name = "txtServerDir";
            txtServerDir.Padding = new Padding(5);
            txtServerDir.ShowText = false;
            txtServerDir.Size = new Size(345, 37);
            txtServerDir.TabIndex = 4;
            txtServerDir.Text = "/app";
            txtServerDir.TextAlignment = ContentAlignment.MiddleLeft;
            txtServerDir.Watermark = "";
            txtServerDir.KeyDown += txtServerDir_KeyDown;
            // 
            // listServer
            // 
            listServer.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            listServer.Columns.AddRange(new ColumnHeader[] { name, size, date });
            listServer.Font = new Font("宋体", 8F, FontStyle.Regular, GraphicsUnit.Point);
            listServer.FullRowSelect = true;
            listServer.Location = new Point(1, 46);
            listServer.MultiSelect = false;
            listServer.Name = "listServer";
            listServer.Size = new Size(405, 645);
            listServer.SmallImageList = imageList;
            listServer.TabIndex = 3;
            listServer.UseCompatibleStateImageBehavior = false;
            listServer.View = View.Details;
            listServer.DoubleClick += listServer_DoubleClick;
            // 
            // name
            // 
            name.Name = "name";
            name.Text = "名称";
            name.Width = 180;
            // 
            // size
            // 
            size.Name = "size";
            size.Text = "大小(kb)";
            size.TextAlign = HorizontalAlignment.Right;
            size.Width = 80;
            // 
            // date
            // 
            date.Name = "date";
            date.Text = "最后修改日期";
            date.Width = 160;
            // 
            // imageList
            // 
            imageList.ColorDepth = ColorDepth.Depth32Bit;
            imageList.ImageSize = new Size(20, 20);
            imageList.TransparentColor = Color.Transparent;
            // 
            // FrmUIPage
            // 
            AutoScaleMode = AutoScaleMode.None;
            ClientSize = new Size(1356, 716);
            Controls.Add(SplitContainer);
            Name = "FrmUIPage";
            Text = "FPage";
            SplitContainer.Panel1.ResumeLayout(false);
            SplitContainer.Panel1.PerformLayout();
            SplitContainer.EndInit();
            SplitContainer.ResumeLayout(false);
            statusStrip1.ResumeLayout(false);
            statusStrip1.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        public Sunny.UI.UISplitContainer SplitContainer;
        private ListView listServer;
        private ColumnHeader name;
        private ColumnHeader size;
        private ColumnHeader date;
        private Sunny.UI.UITextBox txtServerDir;
        private Sunny.UI.UIButton uiButton1;
        private StatusStrip statusStrip1;
        private ToolStripProgressBar pbTransfer;
        private ToolStripStatusLabel lblStatus;
        private ImageList imageList;
    }
}