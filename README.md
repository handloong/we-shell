## 💐 序言

> 无私奉献不是天方夜谭，有时候，我们也可以做到。

# WeShell

#### 介绍
服务器密码频繁变动对服务器没有权限管控.针对这种情况开发了WeShell工具.
该工具基于Rebex组件实现的 SSH/SFTP 远程工具, 支持RDP 三种常用连接方式.
使用JsonRpc来完成服务端和客户端通讯, 



#### 开源协议
本工具MIT协议.但请注意上游类库依赖协议

第三方类库 :  TouchSocket / Rebex / SunnyUI / .Net6 Winform

#### 安装教程
1.部署WeShellServer , 使用CodeFirst模式,只需要填写数据库连接串即可

2.修改Login的HandCode服务器 (公网配置推荐使用域名)

```
        public FrmLogin()
        {
            InitializeComponent();

            lblVersion.Text = $"v{Assembly.GetExecutingAssembly().GetName().Version}";
#if DEBUG
            App.InitializeWsJsonRpc("ws://127.0.0.1:7707/ws");
#endif

#if !DEBUG
            App.InitializeWsJsonRpc("ws://10.32.44.33:7707/ws");
#endif
        }
```
3.如果想新增图标只要双击WeShell项目中的Resources.resx资源文件,把图标拖进去程序自动识别,推荐使用阿里巴巴icon上的大图标(200x200 300x300这种)


#### 使用说明
登录

![输入图片说明](Images/login.png)

用户管理 (超级管理员可以看到所有数据,不是超管只能看到分配到对应组的数据)

![输入图片说明](Images/user.png)

无限级组菜单,支持自定义图标

![输入图片说明](Images/group.png)

新增会话,支持多用户密码分配,支持自定义图标

![输入图片说明](Images/session.png)

用户选择器(单用户不弹出)

![输入图片说明](Images/userselect.png)

连接Linux自动打开SFTP,支持多彩页面

![输入图片说明](Images/ssh_sfp_colorful.png)

连接windows (rdp)

![输入图片说明](Images/rdp.png)


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


