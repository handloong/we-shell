﻿namespace WeShellServer
{
    public class SysConst
    {
        /// <summary>
        /// 用户有哪些单独的Server
        /// </summary>
        public const string Relation_SYS_USER_HAS_SERVER = "SYS_USER_HAS_SERVER";

        /// <summary>
        /// 用户有哪些分类
        /// </summary>
        public const string Relation_SYS_USER_HAS_CATEGORY = "SYS_USER_HAS_CATEGORY";

        /// <summary>
        /// 分类有哪些Server
        /// </summary>
        public const string Relation_SYS_CATEGORY_HAS_SERVER = "SYS_CATEGORY_HAS_SERVER";

        public const string Tip_AccessDenied = "权限不足,无法操作 / access denied";

        public const string Tip_NotSuperAdmin = "权限不足,无法操作 / access denied";

        public static string Y => "Y";
        public static string N => "N";

        public static string PwdKey => "ZZKK6666KKZZ8888";
    }
}