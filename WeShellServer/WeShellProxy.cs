/*
此代码由Rpc工具直接生成，非必要请不要修改此处代码
*/
#pragma warning disable
using System;
using TouchSocket.Core;
using TouchSocket.Sockets;
using TouchSocket.Rpc;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
namespace WeShell
{
public interface IServer:TouchSocket.Rpc.IRemoteServer
{
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
BaseResponse SaveForm(WeShellCore.ServerInput input,IInvokeOption invokeOption = default);
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
Task<BaseResponse> SaveFormAsync(WeShellCore.ServerInput input,IInvokeOption invokeOption = default);

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
BaseResponse List(WeShellCore.ServerQueryInput input,IInvokeOption invokeOption = default);
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
Task<BaseResponse> ListAsync(WeShellCore.ServerQueryInput input,IInvokeOption invokeOption = default);

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
BaseResponse ListAuth(WeShellCore.BaseInput input,IInvokeOption invokeOption = default);
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
Task<BaseResponse> ListAuthAsync(WeShellCore.BaseInput input,IInvokeOption invokeOption = default);

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
BaseResponse Detail(WeShellCore.BaseInput input,IInvokeOption invokeOption = default);
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
Task<BaseResponse> DetailAsync(WeShellCore.BaseInput input,IInvokeOption invokeOption = default);

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
BaseResponse Delete(WeShellCore.BaseInput input,IInvokeOption invokeOption = default);
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
Task<BaseResponse> DeleteAsync(WeShellCore.BaseInput input,IInvokeOption invokeOption = default);

}
public class Server :IServer
{
public Server(IRpcClient client)
{
this.Client=client;
}
public IRpcClient Client{get;private set; }
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public BaseResponse SaveForm(WeShellCore.ServerInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)Client.Invoke(typeof(BaseResponse),"Server.SaveForm",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public async Task<BaseResponse> SaveFormAsync(WeShellCore.ServerInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
return (BaseResponse) await Client.InvokeAsync(typeof(BaseResponse),"Server.SaveForm",invokeOption, parameters);
}

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public BaseResponse List(WeShellCore.ServerQueryInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)Client.Invoke(typeof(BaseResponse),"Server.List",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public async Task<BaseResponse> ListAsync(WeShellCore.ServerQueryInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
return (BaseResponse) await Client.InvokeAsync(typeof(BaseResponse),"Server.List",invokeOption, parameters);
}

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public BaseResponse ListAuth(WeShellCore.BaseInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)Client.Invoke(typeof(BaseResponse),"Server.ListAuth",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public async Task<BaseResponse> ListAuthAsync(WeShellCore.BaseInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
return (BaseResponse) await Client.InvokeAsync(typeof(BaseResponse),"Server.ListAuth",invokeOption, parameters);
}

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public BaseResponse Detail(WeShellCore.BaseInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)Client.Invoke(typeof(BaseResponse),"Server.Detail",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public async Task<BaseResponse> DetailAsync(WeShellCore.BaseInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
return (BaseResponse) await Client.InvokeAsync(typeof(BaseResponse),"Server.Detail",invokeOption, parameters);
}

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public BaseResponse Delete(WeShellCore.BaseInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)Client.Invoke(typeof(BaseResponse),"Server.Delete",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public async Task<BaseResponse> DeleteAsync(WeShellCore.BaseInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
return (BaseResponse) await Client.InvokeAsync(typeof(BaseResponse),"Server.Delete",invokeOption, parameters);
}

}
public static class ServerExtensions
{
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public static BaseResponse SaveForm<TClient>(this TClient client,WeShellCore.ServerInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)client.Invoke(typeof(BaseResponse),"Server.SaveForm",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public static async Task<BaseResponse> SaveFormAsync<TClient>(this TClient client,WeShellCore.ServerInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
return (BaseResponse) await client.InvokeAsync(typeof(BaseResponse),"Server.SaveForm",invokeOption, parameters);
}

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public static BaseResponse List<TClient>(this TClient client,WeShellCore.ServerQueryInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)client.Invoke(typeof(BaseResponse),"Server.List",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public static async Task<BaseResponse> ListAsync<TClient>(this TClient client,WeShellCore.ServerQueryInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
return (BaseResponse) await client.InvokeAsync(typeof(BaseResponse),"Server.List",invokeOption, parameters);
}

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public static BaseResponse ListAuth<TClient>(this TClient client,WeShellCore.BaseInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)client.Invoke(typeof(BaseResponse),"Server.ListAuth",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public static async Task<BaseResponse> ListAuthAsync<TClient>(this TClient client,WeShellCore.BaseInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
return (BaseResponse) await client.InvokeAsync(typeof(BaseResponse),"Server.ListAuth",invokeOption, parameters);
}

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public static BaseResponse Detail<TClient>(this TClient client,WeShellCore.BaseInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)client.Invoke(typeof(BaseResponse),"Server.Detail",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public static async Task<BaseResponse> DetailAsync<TClient>(this TClient client,WeShellCore.BaseInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
return (BaseResponse) await client.InvokeAsync(typeof(BaseResponse),"Server.Detail",invokeOption, parameters);
}

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public static BaseResponse Delete<TClient>(this TClient client,WeShellCore.BaseInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)client.Invoke(typeof(BaseResponse),"Server.Delete",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public static async Task<BaseResponse> DeleteAsync<TClient>(this TClient client,WeShellCore.BaseInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
return (BaseResponse) await client.InvokeAsync(typeof(BaseResponse),"Server.Delete",invokeOption, parameters);
}

}
public interface IUserServer:TouchSocket.Rpc.IRemoteServer
{
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
BaseResponse SaveForm(WeShellCore.UserInput input,IInvokeOption invokeOption = default);
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
Task<BaseResponse> SaveFormAsync(WeShellCore.UserInput input,IInvokeOption invokeOption = default);

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
BaseResponse GetPageList(WeShellCore.UserQueryInput input,IInvokeOption invokeOption = default);
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
Task<BaseResponse> GetPageListAsync(WeShellCore.UserQueryInput input,IInvokeOption invokeOption = default);

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
BaseResponse GroupIds(WeShellCore.BaseInput input,IInvokeOption invokeOption = default);
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
Task<BaseResponse> GroupIdsAsync(WeShellCore.BaseInput input,IInvokeOption invokeOption = default);

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
BaseResponse Delete(WeShellCore.BaseInput input,IInvokeOption invokeOption = default);
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
Task<BaseResponse> DeleteAsync(WeShellCore.BaseInput input,IInvokeOption invokeOption = default);

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
BaseResponse RestEveryOnePasssword(WeShellCore.BaseInput input,IInvokeOption invokeOption = default);
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
Task<BaseResponse> RestEveryOnePassswordAsync(WeShellCore.BaseInput input,IInvokeOption invokeOption = default);

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
BaseResponse ChangePasssword(WeShellCore.UserChangePassword input,IInvokeOption invokeOption = default);
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
Task<BaseResponse> ChangePassswordAsync(WeShellCore.UserChangePassword input,IInvokeOption invokeOption = default);

}
public class UserServer :IUserServer
{
public UserServer(IRpcClient client)
{
this.Client=client;
}
public IRpcClient Client{get;private set; }
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public BaseResponse SaveForm(WeShellCore.UserInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)Client.Invoke(typeof(BaseResponse),"UserServer.SaveForm",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public async Task<BaseResponse> SaveFormAsync(WeShellCore.UserInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
return (BaseResponse) await Client.InvokeAsync(typeof(BaseResponse),"UserServer.SaveForm",invokeOption, parameters);
}

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public BaseResponse GetPageList(WeShellCore.UserQueryInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)Client.Invoke(typeof(BaseResponse),"UserServer.GetPageList",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public async Task<BaseResponse> GetPageListAsync(WeShellCore.UserQueryInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
return (BaseResponse) await Client.InvokeAsync(typeof(BaseResponse),"UserServer.GetPageList",invokeOption, parameters);
}

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public BaseResponse GroupIds(WeShellCore.BaseInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)Client.Invoke(typeof(BaseResponse),"UserServer.GroupIds",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public async Task<BaseResponse> GroupIdsAsync(WeShellCore.BaseInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
return (BaseResponse) await Client.InvokeAsync(typeof(BaseResponse),"UserServer.GroupIds",invokeOption, parameters);
}

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public BaseResponse Delete(WeShellCore.BaseInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)Client.Invoke(typeof(BaseResponse),"UserServer.Delete",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public async Task<BaseResponse> DeleteAsync(WeShellCore.BaseInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
return (BaseResponse) await Client.InvokeAsync(typeof(BaseResponse),"UserServer.Delete",invokeOption, parameters);
}

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public BaseResponse RestEveryOnePasssword(WeShellCore.BaseInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)Client.Invoke(typeof(BaseResponse),"UserServer.RestEveryOnePasssword",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public async Task<BaseResponse> RestEveryOnePassswordAsync(WeShellCore.BaseInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
return (BaseResponse) await Client.InvokeAsync(typeof(BaseResponse),"UserServer.RestEveryOnePasssword",invokeOption, parameters);
}

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public BaseResponse ChangePasssword(WeShellCore.UserChangePassword input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)Client.Invoke(typeof(BaseResponse),"UserServer.ChangePasssword",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public async Task<BaseResponse> ChangePassswordAsync(WeShellCore.UserChangePassword input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
return (BaseResponse) await Client.InvokeAsync(typeof(BaseResponse),"UserServer.ChangePasssword",invokeOption, parameters);
}

}
public static class UserServerExtensions
{
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public static BaseResponse SaveForm<TClient>(this TClient client,WeShellCore.UserInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)client.Invoke(typeof(BaseResponse),"UserServer.SaveForm",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public static async Task<BaseResponse> SaveFormAsync<TClient>(this TClient client,WeShellCore.UserInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
return (BaseResponse) await client.InvokeAsync(typeof(BaseResponse),"UserServer.SaveForm",invokeOption, parameters);
}

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public static BaseResponse GetPageList<TClient>(this TClient client,WeShellCore.UserQueryInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)client.Invoke(typeof(BaseResponse),"UserServer.GetPageList",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public static async Task<BaseResponse> GetPageListAsync<TClient>(this TClient client,WeShellCore.UserQueryInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
return (BaseResponse) await client.InvokeAsync(typeof(BaseResponse),"UserServer.GetPageList",invokeOption, parameters);
}

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public static BaseResponse GroupIds<TClient>(this TClient client,WeShellCore.BaseInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)client.Invoke(typeof(BaseResponse),"UserServer.GroupIds",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public static async Task<BaseResponse> GroupIdsAsync<TClient>(this TClient client,WeShellCore.BaseInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
return (BaseResponse) await client.InvokeAsync(typeof(BaseResponse),"UserServer.GroupIds",invokeOption, parameters);
}

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public static BaseResponse Delete<TClient>(this TClient client,WeShellCore.BaseInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)client.Invoke(typeof(BaseResponse),"UserServer.Delete",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public static async Task<BaseResponse> DeleteAsync<TClient>(this TClient client,WeShellCore.BaseInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
return (BaseResponse) await client.InvokeAsync(typeof(BaseResponse),"UserServer.Delete",invokeOption, parameters);
}

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public static BaseResponse RestEveryOnePasssword<TClient>(this TClient client,WeShellCore.BaseInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)client.Invoke(typeof(BaseResponse),"UserServer.RestEveryOnePasssword",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public static async Task<BaseResponse> RestEveryOnePassswordAsync<TClient>(this TClient client,WeShellCore.BaseInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
return (BaseResponse) await client.InvokeAsync(typeof(BaseResponse),"UserServer.RestEveryOnePasssword",invokeOption, parameters);
}

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public static BaseResponse ChangePasssword<TClient>(this TClient client,WeShellCore.UserChangePassword input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)client.Invoke(typeof(BaseResponse),"UserServer.ChangePasssword",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public static async Task<BaseResponse> ChangePassswordAsync<TClient>(this TClient client,WeShellCore.UserChangePassword input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
return (BaseResponse) await client.InvokeAsync(typeof(BaseResponse),"UserServer.ChangePasssword",invokeOption, parameters);
}

}
public interface IGroupServer:TouchSocket.Rpc.IRemoteServer
{
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
BaseResponse SaveForm(WeShellCore.GroupInput input,IInvokeOption invokeOption = default);
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
Task<BaseResponse> SaveFormAsync(WeShellCore.GroupInput input,IInvokeOption invokeOption = default);

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
BaseResponse Delete(WeShellCore.BaseInput input,IInvokeOption invokeOption = default);
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
Task<BaseResponse> DeleteAsync(WeShellCore.BaseInput input,IInvokeOption invokeOption = default);

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
BaseResponse List(WeShellCore.GroupQueryInput input,IInvokeOption invokeOption = default);
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
Task<BaseResponse> ListAsync(WeShellCore.GroupQueryInput input,IInvokeOption invokeOption = default);

}
public class GroupServer :IGroupServer
{
public GroupServer(IRpcClient client)
{
this.Client=client;
}
public IRpcClient Client{get;private set; }
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public BaseResponse SaveForm(WeShellCore.GroupInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)Client.Invoke(typeof(BaseResponse),"GroupServer.SaveForm",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public async Task<BaseResponse> SaveFormAsync(WeShellCore.GroupInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
return (BaseResponse) await Client.InvokeAsync(typeof(BaseResponse),"GroupServer.SaveForm",invokeOption, parameters);
}

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public BaseResponse Delete(WeShellCore.BaseInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)Client.Invoke(typeof(BaseResponse),"GroupServer.Delete",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public async Task<BaseResponse> DeleteAsync(WeShellCore.BaseInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
return (BaseResponse) await Client.InvokeAsync(typeof(BaseResponse),"GroupServer.Delete",invokeOption, parameters);
}

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public BaseResponse List(WeShellCore.GroupQueryInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)Client.Invoke(typeof(BaseResponse),"GroupServer.List",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public async Task<BaseResponse> ListAsync(WeShellCore.GroupQueryInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
return (BaseResponse) await Client.InvokeAsync(typeof(BaseResponse),"GroupServer.List",invokeOption, parameters);
}

}
public static class GroupServerExtensions
{
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public static BaseResponse SaveForm<TClient>(this TClient client,WeShellCore.GroupInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)client.Invoke(typeof(BaseResponse),"GroupServer.SaveForm",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public static async Task<BaseResponse> SaveFormAsync<TClient>(this TClient client,WeShellCore.GroupInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
return (BaseResponse) await client.InvokeAsync(typeof(BaseResponse),"GroupServer.SaveForm",invokeOption, parameters);
}

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public static BaseResponse Delete<TClient>(this TClient client,WeShellCore.BaseInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)client.Invoke(typeof(BaseResponse),"GroupServer.Delete",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public static async Task<BaseResponse> DeleteAsync<TClient>(this TClient client,WeShellCore.BaseInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
return (BaseResponse) await client.InvokeAsync(typeof(BaseResponse),"GroupServer.Delete",invokeOption, parameters);
}

///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public static BaseResponse List<TClient>(this TClient client,WeShellCore.GroupQueryInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)client.Invoke(typeof(BaseResponse),"GroupServer.List",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public static async Task<BaseResponse> ListAsync<TClient>(this TClient client,WeShellCore.GroupQueryInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
return (BaseResponse) await client.InvokeAsync(typeof(BaseResponse),"GroupServer.List",invokeOption, parameters);
}

}
public interface ILoginServer:TouchSocket.Rpc.IRemoteServer
{
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
BaseResponse CheckLogin(WeShellCore.LoginInput input,IInvokeOption invokeOption = default);
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
Task<BaseResponse> CheckLoginAsync(WeShellCore.LoginInput input,IInvokeOption invokeOption = default);

}
public class LoginServer :ILoginServer
{
public LoginServer(IRpcClient client)
{
this.Client=client;
}
public IRpcClient Client{get;private set; }
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public BaseResponse CheckLogin(WeShellCore.LoginInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)Client.Invoke(typeof(BaseResponse),"LoginServer.CheckLogin",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public async Task<BaseResponse> CheckLoginAsync(WeShellCore.LoginInput input,IInvokeOption invokeOption = default)
{
if(Client==null)
{
throw new RpcException("IRpcClient为空，请先初始化或者进行赋值");
}
object[] parameters = new object[]{input};
return (BaseResponse) await Client.InvokeAsync(typeof(BaseResponse),"LoginServer.CheckLogin",invokeOption, parameters);
}

}
public static class LoginServerExtensions
{
///<summary>
///无注释信息
///</summary>
/// <exception cref="System.TimeoutException">调用超时</exception>
/// <exception cref="TouchSocket.Rpc.RpcInvokeException">Rpc调用异常</exception>
/// <exception cref="System.Exception">其他异常</exception>
public static BaseResponse CheckLogin<TClient>(this TClient client,WeShellCore.LoginInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
BaseResponse returnData=(BaseResponse)client.Invoke(typeof(BaseResponse),"LoginServer.CheckLogin",invokeOption, parameters);
return returnData;
}
///<summary>
///无注释信息
///</summary>
public static async Task<BaseResponse> CheckLoginAsync<TClient>(this TClient client,WeShellCore.LoginInput input,IInvokeOption invokeOption = default) where TClient:
TouchSocket.JsonRpc.IJsonRpcClient{
object[] parameters = new object[]{input};
return (BaseResponse) await client.InvokeAsync(typeof(BaseResponse),"LoginServer.CheckLogin",invokeOption, parameters);
}

}
}
