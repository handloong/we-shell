﻿using SqlSugar;

namespace WeShellServer.Entity
{
    [SugarTable("Sys_Relation")]
    public class SysRelation : PrimaryKeyEntity
    {
        /// <summary>
        /// 对象ID
        ///</summary>
        [SugarColumn(ColumnName = "ObjectId", ColumnDescription = "对象ID", IsNullable = false)]
        public string ObjectId { get; set; }

        /// <summary>
        /// 目标ID
        ///</summary>
        [SugarColumn(ColumnName = "TargetId", ColumnDescription = "目标ID", IsNullable = true)]
        public string TargetId { get; set; }

        /// <summary>
        /// 分类
        ///</summary>
        [SugarColumn(ColumnName = "Category", ColumnDescription = "分类", Length = 200, IsNullable = false)]
        public string Category { get; set; }
    }
}