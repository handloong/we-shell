﻿using SqlSugar;

namespace WeShellServer
{
    [SugarTable("Sys_User")]
    public class SysUser : PrimaryKeyEntity
    {
        [SugarColumn(ColumnName = "Account", ColumnDescription = "账号", Length = 200, IsNullable = false)]
        public string Account { get; set; }

        [SugarColumn(ColumnName = "Name", ColumnDescription = "名称", Length = 200, IsNullable = false)]
        public string Name { get; set; }

        [SugarColumn(ColumnName = "Password", ColumnDescription = "密码", Length = 200, IsNullable = false)]
        public string Password { get; set; }

        [SugarColumn(ColumnName = "LastLoginTime", ColumnDescription = "上次登录时间", IsNullable = true)]
        public DateTime? LastLoginTime { get; set; }

        [SugarColumn(ColumnName = "LastChangePasswordTime", ColumnDescription = "上次修改密码时间", IsNullable = true)]
        public DateTime? LastChangePasswordTime { get; set; }

        [SugarColumn(ColumnDescription = "状态,Y有效,N冻结", IsNullable = true)]
        public string Status { get; set; }

        [SugarColumn(ColumnDescription = "SuperAdmin,Y是,N不是", IsNullable = true)]
        public string SuperAdmin { get; set; }

        [SugarColumn(ColumnName = "Token", ColumnDescription = "Token", IsNullable = true)]
        public string Token { get; set; }

        [SugarColumn(ColumnName = "ErrorTimes", ColumnDescription = "密码错误次数", IsNullable = true)]
        public int ErrorTimes { get; set; }

        [SugarColumn(ColumnName = "LastError", ColumnDescription = "上次密码错误时间", IsNullable = true)]
        public DateTime? LastError { get; set; }

        public bool IsSuperAdmin()
        {
            return this.SuperAdmin == "Y";
        }
    }
}