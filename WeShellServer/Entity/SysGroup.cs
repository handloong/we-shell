﻿using SqlSugar;

namespace WeShellServer
{
    [SugarTable("Sys_Group")]
    public class SysGroup : PrimaryKeyEntity
    {
        [SugarColumn(ColumnName = "ParentId", ColumnDescription = "上级ID", Length = 200, IsNullable = false)]
        public string ParentId { get; set; }

        [SugarColumn(ColumnName = "Name", ColumnDescription = "名称", Length = 200, IsNullable = false)]
        public string Name { get; set; }

        [SugarColumn(ColumnName = "Icon", ColumnDescription = "Icon", Length = 256, IsNullable = true)]
        public string Icon { get; set; }

        [SugarColumn(ColumnName = "SortCode", ColumnDescription = "排序码", IsNullable = true)]
        public int? SortCode { get; set; }
    }
}