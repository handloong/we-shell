﻿using SqlSugar;

namespace WeShellServer
{
    [SugarTable("Sys_Server")]
    public class SysServer : PrimaryKeyEntity
    {
        [SugarColumn(ColumnName = "IP", ColumnDescription = "IP", Length = 64, IsNullable = false)]
        public string IP { get; set; }

        [SugarColumn(ColumnName = "Name", ColumnDescription = "服务器名称", Length = 50, IsNullable = true)]
        public string Name { get; set; }

        [SugarColumn(ColumnName = "Port", ColumnDescription = "端口", IsNullable = false)]
        public int Port { get; set; }

        [SugarColumn(ColumnName = "DutyUser", ColumnDescription = "责任人", Length = 100, IsNullable = true)]
        public string DutyUser { get; set; }

        [SugarColumn(ColumnName = "GroupId", ColumnDescription = "组ID", Length = 36, IsNullable = false)]
        public string GroupId { get; set; }

        [SugarColumn(ColumnName = "Type", ColumnDescription = "服务器类型:Linux/Windows", Length = 50, IsNullable = false)]
        public string Type { get; set; }

        [SugarColumn(ColumnName = "Status", ColumnDescription = "状态,Y可见,N不可见", Length = 10, IsNullable = false)]
        public string Status { get; set; }

        [SugarColumn(ColumnName = "Icon", ColumnDescription = "Icon", Length = 256, IsNullable = true)]
        public string Icon { get; set; }
    }

    [SugarTable("Sys_ServerPass")]
    public class SysServerPass : PrimaryKeyEntity
    {
        [SugarColumn(ColumnName = "ServerId", ColumnDescription = "服务器ID", Length = 36, IsNullable = false)]
        public string ServerId { get; set; }

        [SugarColumn(ColumnName = "UserName", ColumnDescription = "登陆用户", Length = 64, IsNullable = false)]
        public string UserName { get; set; }

        /// <summary>
        /// 需要根据ServerId作为盐加密
        /// </summary>
        [SugarColumn(ColumnName = "Password", ColumnDescription = "登陆密码", Length = 200, IsNullable = false)]
        public string Password { get; set; }

        /// <summary>
        /// 允许授权
        /// </summary>
        [SugarColumn(ColumnName = "Allow", ColumnDescription = "允许授权", Length = 2, IsNullable = false)]
        public string Allow { get; set; }
    }
}