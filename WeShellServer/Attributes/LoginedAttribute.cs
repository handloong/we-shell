﻿using Newtonsoft.Json.Linq;
using SqlSugar;
using SqlSugar.Extensions;
using TouchSocket.Core;
using TouchSocket.Rpc;
using TouchSocket.Sockets;
using WeShellCore;

namespace WeShellServer
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class LoginedAttribute : RpcActionFilterAttribute
    {
        public override async Task<InvokeResult> ExecutingAsync(ICallContext callContext, object[] parameters, InvokeResult invokeResult)
        {
            var json = ((TouchSocket.JsonRpc.JsonRpcCallContextBase)callContext).JsonString;

            // 解析 JSON 字符串为 JToken 对象
            JToken jsonToken = JToken.Parse(json);

            // 判断 Token 属性是否存在
            string token = jsonToken["params"]?[0]?["Token"]?.ToString();

            if (string.IsNullOrWhiteSpace(token))
            {
                invokeResult = new InvokeResult()
                {
                    Status = InvokeStatus.Success,
                    Message = SysConst.Tip_AccessDenied,
                    Result = Retval.Error(SysConst.Tip_AccessDenied)
                };
            }
            else
            {
                //查看Token是否有效
                var _sqlSugarClient = App.Container.Resolve<ISqlSugarClient>();
                var user = await _sqlSugarClient.GetSysUser(new BaseInput { Token = token });
                if (user == null || user.Status != SysConst.Y)
                {
                    invokeResult = new InvokeResult()
                    {
                        Status = InvokeStatus.Success,
                        Message = SysConst.Tip_AccessDenied,
                        Result = Retval.Error(SysConst.Tip_AccessDenied)
                    };
                }
            }

            //if (!(parameters[0] is BaseInput))
            //{
            //    invokeResult = new InvokeResult()
            //    {
            //        Status = InvokeStatus.Success,
            //        Message = "不允许执行",
            //        Result = Retval.Error("该参数不继承BaseInput")
            //    };
            //}


            if (callContext.Caller is ISocketClient client)
            {
                client.Logger.Info($"即将执行Rpc-{callContext.MethodInstance.Name}");
            }
            return invokeResult;
        }

        public override Task<InvokeResult> ExecutedAsync(ICallContext callContext, object[] parameters, InvokeResult invokeResult)
        {
            if (callContext.Caller is ISocketClient client)
            {
                client.Logger.Info($"执行RPC-{callContext.MethodInstance.Name}完成，状态={invokeResult.Status}");
            }
            return Task.FromResult(invokeResult);
        }

        public override Task<InvokeResult> ExecutExceptionAsync(ICallContext callContext, object[] parameters, InvokeResult invokeResult, Exception exception)
        {
            if (callContext.Caller is ISocketClient client)
            {
                client.Logger.Info($"执行RPC-{callContext.MethodInstance.Name}异常，信息={invokeResult.Message}");
            }
            return Task.FromResult(invokeResult);
        }
    }
}