﻿using SqlSugar;
using System.Xml.Linq;
using TouchSocket.Core;
using TouchSocket.JsonRpc;
using TouchSocket.Rpc;
using WeShellCore;
using WeShellServer.Entity;

namespace WeShellServer
{
    public class GroupServer : RpcServer
    {
        [SuperAdmin]
        [JsonRpc(InvokeKey = "GroupServer.SaveForm")]
        public async Task<BaseResponse> SaveForm(GroupInput input)
        {
            var _sqlSugarClient = App.Container.Resolve<ISqlSugarClient>();

            var group = new SysGroup
            {
                Id = string.IsNullOrWhiteSpace(input.Id) ? SnowFlakeSingle.Instance.NextId().ToString() : input.Id,
                Name = input.Name,
                ParentId = input.ParentId,
                Icon = input.Icon,
                Remark = input.Remark,
            };


            if (string.IsNullOrWhiteSpace(input.Id))
            {
                await _sqlSugarClient.Insertable(group).ExecuteCommandAsync();
            }
            else
            {
                await _sqlSugarClient.Updateable(group).IgnoreNullColumns(true).ExecuteCommandAsync();
            }
            return Retval.Successful("", "");
        }

        [SuperAdmin]
        [JsonRpc(InvokeKey = "GroupServer.Delete")]
        public async Task<BaseResponse> Delete(BaseInput input)
        {
            if (string.IsNullOrWhiteSpace(input.Id))
            {
                return Retval.Error("Id必填");
            }
            var _sqlSugarClient = App.Container.Resolve<ISqlSugarClient>();
            var any = await _sqlSugarClient.Queryable<SysServer>().AnyAsync(x => x.GroupId == input.Id);
            if (any)
            {
                return Retval.Error("该组下有Server,无法删除,请先转移");
            }

            await _sqlSugarClient.Deleteable<SysGroup>().Where(x => x.Id == input.Id).ExecuteCommandAsync();
            return Retval.Successful("", "");
        }

        [Logined]
        [JsonRpc(InvokeKey = "GroupServer.List")]
        public async Task<BaseResponse> List(GroupQueryInput input)
        {
            var _sqlSugarClient = App.Container.Resolve<ISqlSugarClient>();

            var user = await _sqlSugarClient.GetSysUser(input);

            var datas = new List<GroupResponseWithKey>();
            if (user.IsSuperAdmin())
            {
                datas = await _sqlSugarClient.Queryable<SysGroup>()
                                           .Select(x => new GroupResponseWithKey
                                           {
                                               Id = x.Id.SelectAll()
                                           }).OrderBy(x => x.Name).ToTreeAsync(x => x.Childs, x => x.ParentId, "0", x => x.Id);
            }
            else
            {
                var inIds = (await _sqlSugarClient.Queryable<SysGroup>()
                                         .InnerJoin<SysRelation>((x, y) => x.Id == y.ObjectId && y.TargetId == user.Id && y.Category == SysConst.Relation_SYS_USER_HAS_CATEGORY)
                                         .Select(x => x.Id)
                                         .ToListAsync())
                                         .Cast<object>().ToArray();

                datas = await _sqlSugarClient.Queryable<SysGroup>()
                                           .Select(x => new GroupResponseWithKey
                                           {
                                               Id = x.Id.SelectAll()
                                           }).OrderBy(x => x.Name).ToTreeAsync(x => x.Childs, x => x.ParentId, "0", inIds);
            }

            return Retval.Successful(datas, "");
        }
    }
}