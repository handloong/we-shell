﻿using SqlSugar;
using TouchSocket.Core;
using TouchSocket.JsonRpc;
using TouchSocket.Rpc;
using WeShellCore;
using WeShellServer.Entity;

namespace WeShellServer
{
    public class UserServer : RpcServer
    {
        [SuperAdmin]
        [JsonRpc(InvokeKey = "UserServer.SaveForm")]
        public async Task<BaseResponse> SaveForm(UserInput input)
        {
            var _sqlSugarClient = App.Container.Resolve<ISqlSugarClient>();

            var user = new SysUser
            {
                Id = string.IsNullOrWhiteSpace(input.Id) ? SnowFlakeSingle.Instance.NextId().ToString() : input.Id,
                Account = input.Account,
                Name = input.Name,
                Status = input.Status,
                SuperAdmin = input.SuperAdmin,
            };

            await _sqlSugarClient.Ado.BeginTranAsync();
            try
            {
                if (string.IsNullOrWhiteSpace(input.Id))
                {
                    user.Password = PwdUtils.AesEncryptor_Base64("123456", user.Id);//密码加密
                    await _sqlSugarClient.Insertable(user).ExecuteCommandAsync();
                }
                else
                {
                    //给用户授权组, 目标是用户,对象是组
                    await _sqlSugarClient.Deleteable<SysRelation>().Where(x => x.TargetId == user.Id && x.Category == SysConst.Relation_SYS_USER_HAS_CATEGORY).ExecuteCommandAsync();
                    await _sqlSugarClient.Updateable(user).IgnoreNullColumns(true).ExecuteCommandAsync();
                }

                foreach (var item in input.GroupIds)
                {
                    var srl = new SysRelation
                    {
                        Category = SysConst.Relation_SYS_USER_HAS_CATEGORY,
                        Id = SnowFlakeSingle.Instance.NextId().ToString(),
                        TargetId = user.Id,
                        ObjectId = item
                    };
                    await _sqlSugarClient.Insertable(srl).ExecuteCommandAsync();
                }

                await _sqlSugarClient.Ado.CommitTranAsync();
            }
            catch (Exception)
            {
                await _sqlSugarClient.Ado.RollbackTranAsync();
                throw;
            }
            return Retval.Successful("", "");
        }

        [SuperAdmin]
        [JsonRpc(InvokeKey = "UserServer.GetPageList")]
        public async Task<BaseResponse> GetPageList(UserQueryInput input)
        {
            var _sqlSugarClient = App.Container.Resolve<ISqlSugarClient>();
            RefAsync<int> total = 0;
            //查询所有
            var datas = await _sqlSugarClient.Queryable<SysUser>()
                                           .WhereIF(!input.Account.IsNullOrWhiteSpace(), x => x.Account.Contains(input.Account))
                                           .WhereIF(!input.Name.IsNullOrWhiteSpace(), x => x.Name.Contains(input.Name))
                                           .OrderBy(x => x.Name)
                                            .Select(x => new UserResponse
                                            {
                                                Id = x.Id.SelectAll()
                                            }).ToPageListAsync(input.PageIndex, input.PageSize, total);

            return Retval.Page(datas, input.PageIndex, input.PageIndex, total);
        }

        [SuperAdmin]
        [JsonRpc(InvokeKey = "UserServer.GroupIds")]
        public async Task<BaseResponse> GroupIds(BaseInput input)
        {
            var _sqlSugarClient = App.Container.Resolve<ISqlSugarClient>();

            //查询所有
            var datas = await _sqlSugarClient.Queryable<SysRelation>()
                                             .Where(x => x.TargetId == input.Id && x.Category == SysConst.Relation_SYS_USER_HAS_CATEGORY)
                                             .Select(x => x.ObjectId).ToListAsync();

            return Retval.Successful(datas, "");
        }

        [SuperAdmin]
        [JsonRpc(InvokeKey = "UserServer.Delete")]
        public async Task<BaseResponse> Delete(BaseInput input)
        {
            var _sqlSugarClient = App.Container.Resolve<ISqlSugarClient>();

            await _sqlSugarClient.Deleteable<SysUser>().Where(x => x.Id == input.Id).ExecuteCommandAsync();
            await _sqlSugarClient.Deleteable<SysRelation>().Where(x => x.TargetId == input.Id).ExecuteCommandAsync();

            return Retval.Successful("", "");
        }

        [SuperAdmin]
        [JsonRpc(InvokeKey = "UserServer.RestEveryOnePasssword")]
        public async Task<BaseResponse> RestEveryOnePasssword(BaseInput input)
        {
            var user = new SysUser
            {
                Id = string.IsNullOrWhiteSpace(input.Id) ? SnowFlakeSingle.Instance.NextId().ToString() : input.Id,
                Password = PwdUtils.AesEncryptor_Base64("123456", input.Id),
                Token = "RESTPWD_" + Guid.NewGuid().ToString(),
                LastChangePasswordTime = new DateTime(1996, 5, 21)//需要重新修改
            };
            var _sqlSugarClient = App.Container.Resolve<ISqlSugarClient>();
            await _sqlSugarClient.Updateable(user).IgnoreNullColumns(true).ExecuteCommandAsync();

            return Retval.Successful("", "");
        }

        [Logined]
        [JsonRpc(InvokeKey = "UserServer.ChangePasssword")]
        public async Task<BaseResponse> ChangePasssword(UserChangePassword input)
        {
            var _sqlSugarClient = App.Container.Resolve<ISqlSugarClient>();
            var loginUser = await _sqlSugarClient.GetSysUser(new BaseInput { Token = input.Token });

            var user = new SysUser
            {
                Id = loginUser.Id,
                Password = PwdUtils.AesEncryptor_Base64(input.NewPassword, loginUser.Id),
                LastChangePasswordTime = DateTime.Now,
                Token = Guid.NewGuid().ToString()//重置Token,登录失效
            };

            if (loginUser.Password != PwdUtils.AesEncryptor_Base64(input.OldPassword, loginUser.Id))
            {
                return Retval.Error("旧密码不正确");
            }

            var similarity = PwdUtils.Similarity(input.OldPassword, input.NewPassword);
            if (similarity > 80)
            {
                return Retval.Error("新密码请勿与旧密码过于相似");
            }

            await _sqlSugarClient.Updateable(user).IgnoreNullColumns(true).ExecuteCommandAsync();

            return Retval.Successful("", "");
        }
    }
}