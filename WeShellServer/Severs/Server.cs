﻿using SqlSugar;
using System.Data;
using TouchSocket.Core;
using TouchSocket.JsonRpc;
using TouchSocket.Rpc;
using WeShellCore;
using WeShellServer.Entity;

namespace WeShellServer
{
    public class Server : RpcServer
    {
        [SuperAdmin]
        [JsonRpc(InvokeKey = "Server.SaveForm")]
        public async Task<BaseResponse> SaveForm(ServerInput input)
        {
            var _sqlSugarClient = App.Container.Resolve<ISqlSugarClient>();
            var user = await _sqlSugarClient.GetSysUser(input);
            if (!user.IsSuperAdmin())
            {
                return Retval.Error(SysConst.Tip_AccessDenied);
            }
            var server = new SysServer
            {
                Id = string.IsNullOrWhiteSpace(input.Id) ? SnowFlakeSingle.Instance.NextId().ToString() : input.Id,
                DutyUser = input.DutyUser,
                GroupId = input.GroupId,
                IP = input.IP,
                Port = input.Port,
                Name = input.Name,
                Status = input.Status,
                Remark = input.Remark,
                Type = input.Type,
                Icon = input.Icon
            };

            await _sqlSugarClient.Ado.BeginTranAsync();
            try
            {
                if (string.IsNullOrWhiteSpace(input.Id))
                {
                    await _sqlSugarClient.Insertable(server).ExecuteCommandAsync();
                }
                else
                {
                    await _sqlSugarClient.Deleteable<SysServerPass>().Where(x => x.ServerId == server.Id).ExecuteCommandAsync();
                    await _sqlSugarClient.Updateable(server).ExecuteCommandAsync();
                }

                foreach (var item in input.ServerAuths)
                {
                    var ssp = new SysServerPass
                    {
                        ServerId = server.Id,
                        Allow = item.Allow ? SysConst.Y : SysConst.N,
                        Id = SnowFlakeSingle.Instance.NextId().ToString(),
                        Password = PwdUtils.AesEncryptor_Base64(item.Password, SysConst.PwdKey),
                        UserName = item.UserName,
                    };
                    await _sqlSugarClient.Insertable(ssp).ExecuteCommandAsync();
                }

                await _sqlSugarClient.Ado.CommitTranAsync();
            }
            catch (Exception)
            {
                await _sqlSugarClient.Ado.RollbackTranAsync();
                throw;
            }
            return Retval.Successful("", "");
        }

        [Logined]
        [JsonRpc(InvokeKey = "Server.List")]
        public async Task<BaseResponse> List(ServerQueryInput input)
        {
            var _sqlSugarClient = App.Container.Resolve<ISqlSugarClient>();

            var user = await _sqlSugarClient.GetSysUser(input);

            var datas = new List<ServerResponse>();

            if (user.IsSuperAdmin())
            {
                //查询所有
                datas = await _sqlSugarClient.Queryable<SysServer>()
                                               .Where(x => x.GroupId == input.GroupId)
                                               .Select(x => new ServerResponse
                                               {
                                                   Id = x.Id.SelectAll()
                                               }).ToListAsync();

            }
            else
            {
                //查询权限
                datas = await _sqlSugarClient.Queryable<SysServer>()
                                         .InnerJoin<SysRelation>((x, y) => x.GroupId == y.ObjectId && y.TargetId == user.Id && y.Category == SysConst.Relation_SYS_USER_HAS_CATEGORY)
                                         .Where((x, y) => x.GroupId == input.GroupId)
                                         .Distinct()
                                         .Select((x, y) => new ServerResponse
                                         {
                                             Id = x.Id.SelectAll()
                                         }).ToListAsync();

            }
            return Retval.Successful(datas, "");
        }

        [Logined]
        [JsonRpc(InvokeKey = "Server.ListAuth")]
        public async Task<BaseResponse> ListAuth(BaseInput input)
        {
            var _sqlSugarClient = App.Container.Resolve<ISqlSugarClient>();
            var ret = new List<ServerAuthResponse>();
            var datas = new List<SysServerPass>();

            var user = await _sqlSugarClient.GetSysUser(input);
            if (user.IsSuperAdmin())
            {
                datas = await _sqlSugarClient.Queryable<SysServerPass>()
                                             .Where(x => x.ServerId == input.Id)
                                             .ToListAsync();
            }
            else
            {
                datas = await _sqlSugarClient.Queryable<SysServerPass>()
                                            .InnerJoin<SysServer>((x, y) => x.ServerId == y.Id)
                                            .InnerJoin<SysRelation>((x, y, z) => y.GroupId == z.ObjectId && z.TargetId == user.Id)
                                            .Where((x, y, z) => x.ServerId == input.Id && x.Allow == SysConst.Y)
                                            .Distinct()
                                            .ToListAsync();

                //查询权限 并且 Allow为Y的
                //return Retval.Successful(new GroupResponse { }, "");
            }

            foreach (var item in datas)
            {
                ret.Add(new ServerAuthResponse { Allow = item.Allow == SysConst.Y ? true : false, Password = item.Password, UserName = item.UserName });
            }

            return Retval.Successful(ret, "");
        }

        [Logined]
        [JsonRpc(InvokeKey = "Server.Detail")]
        public async Task<BaseResponse> Detail(BaseInput input)
        {
            var _sqlSugarClient = App.Container.Resolve<ISqlSugarClient>();

            var response = await _sqlSugarClient.Queryable<SysServer>()
                                                  .Where(x => x.Id == input.Id)
                                                  .Select(x => new ServerResponse
                                                  {
                                                      Id = x.Id.SelectAll()
                                                  }).SingleAsync();

            return Retval.Successful(response, "");
        }

        [SuperAdmin]
        [JsonRpc(InvokeKey = "Server.Delete")]
        public async Task<BaseResponse> Delete(BaseInput input)
        {
            var _sqlSugarClient = App.Container.Resolve<ISqlSugarClient>();

            await _sqlSugarClient.Deleteable<SysServer>().Where(x => x.Id == input.Id).ExecuteCommandAsync();
            await _sqlSugarClient.Deleteable<SysServerPass>().Where(x => x.ServerId == input.Id).ExecuteCommandAsync();

            return Retval.Successful("", "");
        }
    }
}