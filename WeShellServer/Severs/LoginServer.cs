﻿using SqlSugar;
using TouchSocket.Core;
using TouchSocket.JsonRpc;
using TouchSocket.Rpc;
using WeShellCore;

namespace WeShellServer
{
    [AllowAnonymous]
    public class LoginServer : RpcServer
    {
        [JsonRpc(InvokeKey = "LoginServer.CheckLogin")]
        public async Task<BaseResponse> CheckLogin(LoginInput input)
        {
            var _sqlSugarClient = App.Container.Resolve<ISqlSugarClient>();

            var user = await _sqlSugarClient.Queryable<SysUser>().FirstAsync(x => x.Account == input.Account);

            if (user != null && PwdUtils.AesEncryptor_Base64(input.Password, user.Id) == user.Password && user.Status == "Y")
            {
                var token = PwdUtils.AesEncryptor_Base64(Guid.NewGuid().ToString(), user.Id);
                var now = _sqlSugarClient.GetDate();

                await _sqlSugarClient.Updateable<SysUser>()
                                     .SetColumns(x => x.Token == token)
                                     .SetColumns(x => x.LastError == new DateTime(1996, 5, 21))
                                     .SetColumns(x => x.ErrorTimes == 0)
                                     .SetColumns(x => x.LastLoginTime == now)
                                     .Where(x => x.Id == user.Id)
                                     .ExecuteCommandAsync();

                UserLoginResponse data = new()
                {
                    Account = input.Account,
                    Name = user.Name,
                    Token = token,
                    SuperAdmin = user.IsSuperAdmin()
                };

                //30天必须修改密码
                if (user.LastChangePasswordTime == null || (now - user.LastChangePasswordTime.Value).TotalDays >= 30)
                    data.NeedChangePassword = true;

                return Retval.Successful(data, "OK");
            }

            if (user is null)
                return Retval.Error("用户名密码错误");

            if (user.Status != "Y")
                return Retval.Error("用户已经被禁用");

            return Retval.Error("用户名密码错误");
        }
    }
}