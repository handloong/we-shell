﻿using Microsoft.Extensions.Configuration;
using SqlSugar;
using TouchSocket.Core;
using TouchSocket.Http;
using TouchSocket.JsonRpc;
using TouchSocket.Rpc;
using TouchSocket.Sockets;
using WeShellCore;

namespace WeShellServer
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();

            HttpService service = new HttpService();

            var port = Convert.ToInt32(configuration["RPCPort"]);
            service.Setup(new TouchSocketConfig()
                 .SetListenIPHosts(port)
                 .ConfigurePlugins(a =>
                 {
                     a.UseWebSocket()
                     .SetWSUrl("/ws");

                     a.UseWebSocketJsonRpc()
                     .SetAllowJsonRpc((socketClient, context) =>
                     {
                         //此处的作用是，通过连接的一些信息判断该ws是否执行JsonRpc。
                         //当然除了此处可以设置外，也可以通过socketClient.SetJsonRpc(true)直接设置。

                         return true;
                     })
                     .ConfigureRpcStore(store =>
                     {
                         store.RegisterServer<LoginServer>();
                         store.RegisterServer<GroupServer>();
                         store.RegisterServer<Server>();
                         store.RegisterServer<UserServer>();

#if DEBUG
                         File.WriteAllText("../../../WeShellProxy.cs", store.GetProxyCodes("WeShell", new Type[] { typeof(JsonRpcAttribute) }));
                         ConsoleLogger.Default.Info("成功生成代理");
#endif
                     });
                 }));

            service.Start();
            ConsoleLogger.Default.Info($"Server 开启成功:{port}");

            string connection = configuration["DataBase:ConnectionString"];
            var databaseType = (DbType)Enum.Parse(typeof(DbType), configuration["DataBase:DbType"]);

            App.Container = new Container();

            App.Container.RegisterTransient<ISqlSugarClient>(x =>
            {
                var db = new SqlSugarClient(new ConnectionConfig()
                {
                    DbType = databaseType,
                    ConnectionString = connection,
                    IsAutoCloseConnection = true
                });

                db.Aop.OnExecutingChangeSql = (sql, pars) =>
                {
                    //Oracle分页去重如果又OrderBy会生成 select distinct top xxx from...Oracle 不支持top在这里处理一下
                    if (db.CurrentConnectionConfig.DbType == DbType.Oracle && sql.Contains("DISTINCT TOP"))
                        sql = sql.Replace($"DISTINCT TOP {int.MaxValue}", "DISTINCT");

                    return new KeyValuePair<string, SugarParameter[]>(sql, pars);
                };

                db.Aop.OnLogExecuting = (sql, pars) =>
                {
#if DEBUG
                    var mysql = UtilMethods.GetSqlString(db.CurrentConnectionConfig.DbType, sql, pars);
#endif
                };
                db.Aop.OnLogExecuted = (sql, pars) => //SQL执行完
                {
#if     DEBUG
                    if (sql.StartsWith("SELECT"))
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                    }
                    if (sql.StartsWith("UPDATE") || sql.StartsWith("INSERT"))
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                    }
                    if (sql.StartsWith("DELETE"))
                    {
                        Console.ForegroundColor = ConsoleColor.Blue;
                    }
                    Console.WriteLine($"耗时:{db.Ado.SqlExecutionTime.TotalMilliseconds} ms {Environment.NewLine}Sql:{UtilMethods.GetSqlString(db.CurrentConnectionConfig.DbType, sql, pars)}");
#endif

                    //#if !DEBUG
                    //                    if (db.Ado.SqlExecutionTime.TotalMilliseconds > 200)
                    //                    {
                    //                      //LogHelper.Warn($"耗时超过200ms:{db.Ado.SqlExecutionTime.TotalMilliseconds} ms {Environment.NewLine}Sql:{UtilMethods.GetSqlString(db.CurrentConnectionConfig.DbType, sql, pars)}");
                    //                      Console.WriteLine($"耗时超过200ms:{db.Ado.SqlExecutionTime.TotalMilliseconds} ms {Environment.NewLine}Sql:{UtilMethods.GetSqlString(db.CurrentConnectionConfig.DbType, sql, pars)}");
                    //                    }
                    //#endif
                };

                //db.Aop.OnDiffLogEvent = it =>
                //{
                //    //操作前记录  包含： 字段描述 列名 值 表名 表描述
                //    var editBeforeData = it.BeforeData;//插入Before为null，之前还没进库
                //    //操作后记录   包含： 字段描述 列名 值  表名 表描述
                //    var editAfterData = it.AfterData;
                //    var sql = it.Sql;

                //    var mysql = UtilMethods.GetSqlString(db.CurrentConnectionConfig.DbType, it.Sql, it.Parameters);

                //    var parameter = it.Parameters;
                //    var data = it.BusinessData;//这边会显示你传进来的对象
                //    var time = it.Time;
                //    var diffType = it.DiffType;//enum insert 、update and delete

                //    //Write logic
                //};

                return db;
            });

            App.Container.Resolve<ISqlSugarClient>().IniDb();
            Console.ReadLine();
        }
    }
}