﻿namespace WeShellServer
{
    public class GroupResponseWithKey
    {
        [SqlSugar.SugarColumn(IsPrimaryKey = true)]
        public string Id { get; set; }

        public string ParentId { get; set; }

        public string Name { get; set; }

        public string Icon { get; set; }

        public List<GroupResponseWithKey> Childs { get; set; }
    }
}
