﻿using SqlSugar;
using System.Reflection;
using WeShellCore;

namespace WeShellServer
{
    public static class SqlsugarHelper
    {
        /// <summary>
        /// 初始化数据库表结构
        /// </summary>
        /// <param name="config">数据库配置</param>
        public static void IniDb(this ISqlSugarClient db)
        {
            //db.DbMaintenance.CreateDatabase();//创建数据库

            var entityTypes = Assembly.GetExecutingAssembly().GetTypes().Where(u => !u.IsInterface && !u.IsAbstract && u.IsClass && u.IsDefined(typeof(SugarTable), false));

            if (!entityTypes.Any()) return;
            foreach (var entityType in entityTypes)
            {
                db.CodeFirst.InitTables(entityType);
            }

            //检查是否有超级管理员,如果没有则新增一个超级管理员
            var superAdmin = db.Queryable<SysUser>().Any(x => x.Account == "superAdmin");
            if (!superAdmin)
            {
                SysUser user = new SysUser
                {
                    Account = "superAdmin",
                    Id = SnowFlakeSingle.instance.NextId().ToString(),
                    Name = "超级管理",
                    Status = "Y",
                    Remark = "系统自动创建",
                    SuperAdmin = "Y",
                    Token = Guid.NewGuid().ToString(),
                };
                user.Password = PwdUtils.AesEncryptor_Base64("123456", user.Id);
                db.Insertable(user).ExecuteCommand();
            }
        }

        public static async Task<SysUser> GetSysUser(this ISqlSugarClient db, BaseInput baseInput)
        {
            return await db.Queryable<SysUser>().SingleAsync(x => x.Token == baseInput.Token);
        }

        public static async Task<SysUser> ThrowIFNoSysUser(this ISqlSugarClient db, BaseInput baseInput)
        {
            return await db.Queryable<SysUser>().SingleAsync(x => x.Token == baseInput.Token);
        }

    }
}