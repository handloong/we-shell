﻿namespace WeShellCore
{
    public class BaseInput
    {
        /// <summary>
        /// Winform Token
        /// </summary>
        public string Token { get; set; }

        public string Id { get; set; }
    }
}