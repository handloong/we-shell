﻿namespace WeShellCore
{
    public class LoginInput
    {
        public string Account { get; set; }

        public string Password { get; set; }
    }
}