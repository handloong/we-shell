﻿using System.Collections.Generic;

namespace WeShellCore
{
    public class ServerInput : BaseInput
    {
        public string IP { get; set; }
        public string Name { get; set; }
        public int Port { get; set; }
        public string DutyUser { get; set; }
        public string GroupId { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public string Remark { get; set; }
        public string Icon { get; set; }

        public List<ServerAuth> ServerAuths { get; set; }
    }

    public class ServerAuth
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool Allow { get; set; }
    }

    public class ServerQueryInput : BaseInput
    {
        public string GroupId { get; set; }
    }
}