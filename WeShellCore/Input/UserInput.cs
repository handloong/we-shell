﻿using System.Collections.Generic;

namespace WeShellCore
{
    public class UserInput : BaseInput
    {
        public string Account { get; set; }

        public string Name { get; set; }

        public string Status { get; set; }

        public string SuperAdmin { get; set; }

        public List<string> GroupIds { get; set; }
    }

    public class UserQueryInput : PageResponse
    {
        public string Account { get; set; }

        public string Name { get; set; }
    }

    public class UserChangePassword : BaseInput
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}
