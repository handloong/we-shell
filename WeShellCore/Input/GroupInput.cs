﻿namespace WeShellCore
{
    public class GroupInput : BaseInput
    {
        public string Name { get; set; }
        public string Icon { get; set; }
        public string Remark { get; set; }
        public string ParentId { get; set; }
    }

    public class GroupQueryInput : BaseInput
    {
        public string ParentId { get; set; }
    }
}