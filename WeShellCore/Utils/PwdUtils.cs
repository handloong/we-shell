﻿using System;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace WeShellCore
{
    public class PwdUtils
    {
        private static int _size = 32;

        /// <summary>
        /// AES 算法加密(ECB模式) 将明文加密，加密后进行base64编码，返回密文
        /// </summary>
        /// <param name="EncryptStr">明文</param>
        /// <param name="Key">密钥</param>
        /// <returns>加密后base64编码的密文</returns>
        public static string AesEncryptor_Base64(string EncryptStr, string Key)
        {
            try
            {
                Key = ProcessKey(Key, _size);

                byte[] keyArray = Convert.FromBase64String(Key);
                byte[] toEncryptArray = Encoding.UTF8.GetBytes(EncryptStr);

                RijndaelManaged rDel = new RijndaelManaged();
                rDel.Key = keyArray;
                rDel.Mode = CipherMode.ECB;
                rDel.Padding = PaddingMode.PKCS7;

                ICryptoTransform cTransform = rDel.CreateEncryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

                return Convert.ToBase64String(resultArray, 0, resultArray.Length);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static string ProcessKey(string key, int len)
        {
            key = Regex.Replace(key, "[^a-zA-Z0-9]", "");
            key += $"WESHEELCORE";

            if (string.IsNullOrEmpty(key) || len <= 0)
            {
                return string.Empty;
            }

            int keyLength = key.Length;
            int repetitions = len / keyLength; // 计算需要重复的次数
            int remainder = len % keyLength; // 计算剩余的字符数

            string repeatedKey = string.Concat(Enumerable.Repeat(key, repetitions)); // 重复字符串
            string finalKey = repeatedKey + key.Substring(0, remainder); // 拼接剩余的字符

            return finalKey;
        }

        /// <summary>
        /// AES 算法解密(ECB模式) 将密文base64解码进行解密，返回明文
        /// </summary>
        /// <param name="DecryptStr">密文</param>
        /// <param name="Key">密钥</param>
        /// <returns>明文</returns>
        public static string AesDecryptor_Base64(string DecryptStr, string Key)
        {
            try
            {
                Key = ProcessKey(Key, _size);

                byte[] keyArray = Convert.FromBase64String(Key);
                byte[] toEncryptArray = Convert.FromBase64String(DecryptStr);

                RijndaelManaged rDel = new RijndaelManaged();
                rDel.Key = keyArray;
                rDel.Mode = CipherMode.ECB;
                rDel.Padding = PaddingMode.PKCS7;

                ICryptoTransform cTransform = rDel.CreateDecryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);

                return Encoding.UTF8.GetString(resultArray);
            }
            catch (Exception)
            {
                return null;
            }
        }


        /// <summary>
        /// 密码相似度
        /// </summary>
        /// <param name="oldPassword"></param>
        /// <param name="newPassword"></param>
        /// <returns></returns>
        public static double Similarity(string oldPassword, string newPassword)
        {
            int editDistance = LevenshteinDistance(oldPassword, newPassword);
            double similarity = 1.0 - ((double)editDistance / (double)Math.Max(oldPassword.Length, newPassword.Length));
            return similarity * 100;
        }

        /// <summary>
        /// 计算莱文斯坦距离算法
        /// </summary>
        /// <param name="s1"></param>
        /// <param name="s2"></param>
        /// <returns></returns>
        public static int LevenshteinDistance(string s1, string s2)
        {
            int[,] distance = new int[s1.Length + 1, s2.Length + 1];

            for (int i = 0; i <= s1.Length; i++)
                distance[i, 0] = i;

            for (int j = 0; j <= s2.Length; j++)
                distance[0, j] = j;

            for (int i = 1; i <= s1.Length; i++)
            {
                for (int j = 1; j <= s2.Length; j++)
                {
                    int cost = (s1[i - 1] == s2[j - 1]) ? 0 : 1;

                    distance[i, j] = Math.Min(
                        Math.Min(distance[i - 1, j] + 1, distance[i, j - 1] + 1),
                        distance[i - 1, j - 1] + cost);
                }
            }
            return distance[s1.Length, s2.Length];
        }
    }
}