﻿namespace WeShellCore
{
    public static class ObjectExtension
    {
        public static T As<T>(this object data)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(data.ToString());
        }
    }
}