﻿using TouchSocket.Core;
using TouchSocket.JsonRpc;
using TouchSocket.Sockets;

namespace WeShellCore
{
    public class App
    {
        public static Container Container { get; set; }

        public static WebSocketJsonRpcClient WebSocketJsonRpcClient { get; set; }

        public static void InitializeWsJsonRpc(string ws)
        {
            WebSocketJsonRpcClient = new WebSocketJsonRpcClient();
            WebSocketJsonRpcClient.Setup(new TouchSocketConfig().SetRemoteIPHost(ws));
        }
    }
}