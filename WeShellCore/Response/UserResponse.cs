﻿namespace WeShellCore
{
    public class UserResponse
    {
        public string Id { get; set; }
        public string Account { get; set; }

        public string Name { get; set; }

        public string Password { get; set; }

        public string Status { get; set; }

        public string SuperAdmin { get; set; }
    }
}
