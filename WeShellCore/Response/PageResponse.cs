﻿namespace WeShellCore
{
    public class PageResponse
    {
        public int Total { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int TotalPage { get; set; }


        public string Token { get; set; }
        public string Id { get; set; }
    }
}
