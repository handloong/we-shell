﻿namespace WeShellCore
{
    public class UserLoginResponse
    {
        public string Account { get; set; }
        public string Token { get; set; }

        public string Name { get; set; }

        public bool SuperAdmin { get; set; }

        public bool NeedChangePassword { get; set; }
    }
}