﻿
namespace WeShellCore
{
    public class BaseResponse
    {
        public bool Successful { get; set; }

        public string Message { get; set; }

        public object Data { get; set; }

        public PageResponse Page { get; set; }
    }

    public class Retval
    {
        public static BaseResponse Successful()
        {
            return new BaseResponse() { Successful = true };
        }

        public static BaseResponse Successful(object data, string message)
        {
            return new BaseResponse() { Successful = true, Message = message, Data = data };
        }

        public static BaseResponse Page(object data, int pageIndex, int pageSize, int total)
        {
            return new BaseResponse()
            {
                Data = data,
                Successful = true,
                Page = new PageResponse
                {
                    PageIndex = pageIndex,
                    PageSize = pageSize,
                    Total = total
                }
            };
        }

        public static BaseResponse Error(string message)
        {
            return new BaseResponse() { Successful = false, Message = message };
        }
    }
}