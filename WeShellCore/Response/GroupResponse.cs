﻿using System.Collections.Generic;

namespace WeShellCore
{
    public class GroupResponse
    {
        public string Id { get; set; }

        public string ParentId { get; set; }

        public string Name { get; set; }

        public string Icon { get; set; }

        public List<GroupResponse> Childs { get; set; }
    }
}