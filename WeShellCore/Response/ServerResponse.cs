﻿namespace WeShellCore
{
    public class ServerResponse : BaseInput
    {
        public string IP { get; set; }

        public string Name { get; set; }

        public int Port { get; set; }

        public string DutyUser { get; set; }

        public string GroupId { get; set; }

        //[SugarColumn(ColumnName = "Type", ColumnDescription = "服务器类型:Linux/Windows", Length = 50, IsNullable = false)]
        public string Type { get; set; }

        //[SugarColumn(ColumnDescription = "状态,Y可见,N不可见", Length = 10, IsNullable = false)]
        public string Status { get; set; }

        public string Remark { get; set; }

        public string Icon { get; set; }
    }

    public class ServerAuthResponse
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool Allow { get; set; }
    }
}